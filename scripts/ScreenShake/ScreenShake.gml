/// @description ScreenShake(xrange, yrange, anglerange, intensity, ease)
/// @function ScreenShake
/// @param xrange 
/// @param yrange
/// @param anglerange
/// @param intensity
/// @param ease
//screen shake script
	

with(oCam)
{
    xrange = argument0;
    yrange = argument1;
    anglerange = argument2;
    intensity = argument3;
    ease = argument4;
}

