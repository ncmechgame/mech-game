///@description CreateMenuPage
/// @arg	["Name1", type1, entries1...]
/// @arg	["Name2", type2, entries2...]

var arg, i = 0;

//store all the arguments in an array called arg based on the number of arguments passed in
repeat (argument_count){
	arg[i] = argument[i];
	i++;
	
}

//should access each value and check the length of the array, currently using a method that sets all of them to 5,
//this creates some empty data adn isnt optimized

//returns the id of the grid we create
var ds_grid_id = ds_grid_create(5, argument_count);

for (var c = 0; c < argument_count; c++){
	
	
	var array = arg[c];
	
	//get the length of the arguments that we pass in as arrays
	var array_len = array_length_1d(array);
	
	//loop through our grid and update it with the values from the arguments
	for(var xx = 0; xx < array_len; xx++){
		ds_grid_id[# xx, c] = array[xx];
	}
}


return ds_grid_id;