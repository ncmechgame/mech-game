inputDashUp = keyboard_check_pressed(ord("W"));
inputDashRight = keyboard_check_pressed(ord("D"));
inputDashDown = keyboard_check_pressed(ord("S"));
inputDashLeft = keyboard_check_pressed(ord("A"));

doublePressThreshold = 600;

if (inputDashUp) {
	
	if (current_time - lastPressedUp < doublePressThreshold) 
		timesPressedUp++;
	else
		timesPressedUp = 1;

	lastPressedUp = current_time;
	
	if (timesPressedUp = 2) {
		y-=25;
	}
}

if (inputDashRight) {
	
	if (current_time - lastPressedRight < doublePressThreshold) 
		timesPressedRight++;
	else
		timesPressedRight = 1;

	lastPressedRight = current_time;
	
	if (timesPressedRight = 2) {
		x+=25;
	}
	
}

if (inputDashDown) {
	
	if (current_time - lastPressedDown < doublePressThreshold) 
		timesPressedDown++;
	else
		timesPressedDown = 1;

	lastPressedDown = current_time;
	
	if (timesPressedDown = 2) {
		y+=25;
	}
	
}

if (inputDashLeft) {
	
	if (current_time - lastPressedLeft < doublePressThreshold) 
		timesPressedLeft++;
	else
		timesPressedLeft = 1;

	lastPressedLeft = current_time;
	
	if (timesPressedLeft = 2) {
		x-=25;
	}
	
}
