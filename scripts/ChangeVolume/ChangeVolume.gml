///@description ChangeVolume
/// @arg value

//get the type of the menu object we are changing, master, sfx or music
var type = menu_option[page];

switch(type) {
	case 0: audio_master_gain(argument0); break;
	case 1: audio_group_set_gain(audiogroup_sfx, argument0, 0); break;
	case 2: audio_group_set_gain(audiogroup_music, argument0, 0); break
}



