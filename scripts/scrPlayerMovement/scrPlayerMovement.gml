inputUp = keyboard_check(ord("W")) || keyboard_check(vk_up);
inputRight = keyboard_check(ord("D")) || keyboard_check(vk_right);
inputDown = keyboard_check(ord("S")) || keyboard_check(vk_down);
inputLeft = keyboard_check(ord("A")) || keyboard_check(vk_left);

inputBoost = keyboard_check(vk_shift);

moveSpeed = 5;
boostModifier = 2;

scrPlayerDash();

if (inputBoost)
	moveSpeed += boostModifier;
	
if (vspeed > -moveSpeed) 
	if (inputUp)
		vspeed -= 1;
		
if (vspeed < moveSpeed) 		
	if (inputDown)
		vspeed += 1;

if (hspeed > -moveSpeed)
	if (inputLeft)
		hspeed-= 1;
		
if (hspeed < moveSpeed) 
	if (inputRight)
		hspeed += 1;

if (!inputRight && !inputLeft) {
	if (hspeed > 0)
		hspeed -= 1;
	if (hspeed < 0)
		hspeed += 1;
}

if (!inputUp && !inputDown) {
	if (vspeed > 0)
		vspeed -= 1;
	if (vspeed < 0)
		vspeed += 1;
}


