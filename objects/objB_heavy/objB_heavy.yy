{
    "id": "d70865d3-6ef4-47f8-bf11-b629d10213b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_heavy",
    "eventList": [
        {
            "id": "e68f6616-5b09-499d-b232-8f19025f99c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d70865d3-6ef4-47f8-bf11-b629d10213b4"
        },
        {
            "id": "babfc619-fe11-4134-92c1-d426effb04d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d70865d3-6ef4-47f8-bf11-b629d10213b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "559c04e4-976d-4a37-9007-3600f96f16f6",
    "visible": true
}