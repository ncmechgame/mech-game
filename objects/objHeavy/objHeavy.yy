{
    "id": "c39d393a-e983-4307-9c33-2c2073d73127",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHeavy",
    "eventList": [
        {
            "id": "4b196ac9-8cf0-40a2-beb2-f8c2cf5c9536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c39d393a-e983-4307-9c33-2c2073d73127"
        },
        {
            "id": "315f9625-46f2-4abe-ac77-dcc4d4935a74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "c39d393a-e983-4307-9c33-2c2073d73127"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "054cfb2f-7b20-4021-addf-f4db220755a4",
    "visible": true
}