{
    "id": "20ac9023-effe-47ef-9e66-7fde6f849e55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFeet",
    "eventList": [
        {
            "id": "27b27655-3ea4-4553-86c2-27c5ebb0f4fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20ac9023-effe-47ef-9e66-7fde6f849e55"
        },
        {
            "id": "ed761c4d-be76-4055-aa2e-521e2a77b4ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20ac9023-effe-47ef-9e66-7fde6f849e55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
    "visible": true
}