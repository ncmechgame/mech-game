inputUp = keyboard_check(ord("W")) || keyboard_check(vk_up);
inputRight = keyboard_check(ord("D")) || keyboard_check(vk_right);
inputDown = keyboard_check(ord("S")) || keyboard_check(vk_down);
inputLeft = keyboard_check(ord("A")) || keyboard_check(vk_left);

maxSpeed = 6;

if (inputUp) && (speed < maxSpeed)
    vspeed -= 1;
    
if (inputDown) && (speed < maxSpeed)
    vspeed += 1;

if (inputRight) && (speed < maxSpeed)
    hspeed += 1;
    
if (inputLeft) && (speed < maxSpeed)
    hspeed -= 1;


speed = speed * 0.9;

if (speed < 0.1 && speed > -0.1) {
    speed = 0;
	image_speed = 0;
}
else 
	image_speed = 0.5;

image_angle = direction;