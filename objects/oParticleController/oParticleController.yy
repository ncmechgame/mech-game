{
    "id": "3eba0e91-ee4f-47a5-8882-7792830b715d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oParticleController",
    "eventList": [
        {
            "id": "ac57c274-00b4-4f4b-b169-9f5ffb5c0b9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3eba0e91-ee4f-47a5-8882-7792830b715d"
        },
        {
            "id": "b63de36c-7ea1-4f79-93a7-3a6c2b342c7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "3eba0e91-ee4f-47a5-8882-7792830b715d"
        },
        {
            "id": "d3e94271-53d3-438d-a414-59a92a0d73b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3eba0e91-ee4f-47a5-8882-7792830b715d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}