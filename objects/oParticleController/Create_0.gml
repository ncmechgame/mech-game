/// @description Initilaize Particle Sytstems
    
	// Very Front
    global.pSystemForeground = part_system_create(); //foreground
    part_system_depth(global.pSystemForeground,-1000000)
    
    global.pSystemMiddleForeground = part_system_create(); //middle foreground
    part_system_depth(global.pSystemMiddleForeground,-10000)
    
    global.pSystemMiddleBackground = part_system_create(); //middle background
    part_system_depth(global.pSystemMiddleBackground,10000)
    
    //Very back
    global.pSystemBackground = part_system_create(); //background
    part_system_depth(global.pSystemBackground,1000000);

