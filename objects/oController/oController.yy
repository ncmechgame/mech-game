{
    "id": "7f341dd4-f38d-49e2-8e39-c0fb5465f364",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oController",
    "eventList": [
        {
            "id": "9bfadcfd-8ca1-4e43-9384-cba400f7c8dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f341dd4-f38d-49e2-8e39-c0fb5465f364"
        },
        {
            "id": "aff33e46-d956-416d-8857-d086b94e5318",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f341dd4-f38d-49e2-8e39-c0fb5465f364"
        },
        {
            "id": "247db9a7-c8e8-4cbb-abd8-77d08ecb5091",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "7f341dd4-f38d-49e2-8e39-c0fb5465f364"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}