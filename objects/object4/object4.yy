{
    "id": "0b5b64ae-e18b-46bd-8f45-2192b4e11462",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object4",
    "eventList": [
        {
            "id": "bd5f7318-8bd1-493b-a905-98795254afaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "0b5b64ae-e18b-46bd-8f45-2192b4e11462"
        },
        {
            "id": "a77eaf89-88f9-4ee2-80e1-668f86e04570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b5b64ae-e18b-46bd-8f45-2192b4e11462"
        },
        {
            "id": "9158c98c-87f8-4bd4-b763-d769f160003f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "0b5b64ae-e18b-46bd-8f45-2192b4e11462"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1424299-66db-4076-ae19-b75a2bd4de0e",
    "visible": true
}