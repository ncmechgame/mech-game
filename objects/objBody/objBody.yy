{
    "id": "fb76c9fb-0ce7-4528-b965-32a254ac3d6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBody",
    "eventList": [
        {
            "id": "1ff5dd63-63a2-461b-bcff-e0d6218c25ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb76c9fb-0ce7-4528-b965-32a254ac3d6c"
        },
        {
            "id": "fb91a8d6-2725-4fdc-b748-269037b7e542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb76c9fb-0ce7-4528-b965-32a254ac3d6c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7eb556a8-f274-497d-90a5-c1e740e2f14b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c84f5647-ee97-42d7-92ba-aeeb3bfe0b78",
    "visible": true
}