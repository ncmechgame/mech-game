lastPressedUp = 0;
lastPressedRight = 0;
lastPressedDown = 0;
lastPressedLeft = 0;
timesPressedUp = 0;
timesPressedRight = 0;
timesPressedDown = 0;
timesPressedLeft = 0;


weapons = [objHeavy, objHeavy];

weaponPositions = array_create(2);

for (i = 0; i < array_length_1d(weapons); i++) {
	with (instance_create_depth(0, 0, objBody.depth + 1, weapons[i])) {
		position = objBody.i;
	}
}