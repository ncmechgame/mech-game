{
    "id": "245613bb-015b-4132-a09e-753662acd6bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Lazor2",
    "eventList": [
        {
            "id": "a1fe71a9-bbd0-4505-a443-8eece593017c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "245613bb-015b-4132-a09e-753662acd6bc"
        },
        {
            "id": "f6065124-ed8b-41f0-813e-8f2ef13a9a10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "245613bb-015b-4132-a09e-753662acd6bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
    "visible": true
}