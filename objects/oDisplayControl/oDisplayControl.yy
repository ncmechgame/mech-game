{
    "id": "c47e961c-637e-41fb-ac13-af852f5275b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDisplayControl",
    "eventList": [
        {
            "id": "925e4272-df5d-4d27-bc8d-37111d3b1b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        },
        {
            "id": "acfa6503-ce79-4fa8-a421-f62d6ea82ae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        },
        {
            "id": "2f05996b-cf33-4bb8-b2be-e041303450b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        },
        {
            "id": "1c20ab69-83cc-469d-ade7-11833d46baa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        },
        {
            "id": "e7334780-67c1-491c-a6d1-e38773ed8b35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        },
        {
            "id": "d80753a7-f11e-4895-b992-7b842c64a119",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "c47e961c-637e-41fb-ac13-af852f5275b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}