{
    "id": "ba12c3bb-9677-4189-91ec-f96fd28259ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Play",
    "eventList": [
        {
            "id": "4079ee2d-bbeb-46f5-85a5-b88210a2c974",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ba12c3bb-9677-4189-91ec-f96fd28259ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "515825c4-1c98-41c1-a70b-5ae1283f3d44",
    "visible": true
}