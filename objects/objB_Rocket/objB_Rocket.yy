{
    "id": "6438bf45-c7d1-4e96-be34-781843957f1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Rocket",
    "eventList": [
        {
            "id": "3120dad7-3794-402a-a465-9f3e0600347b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6438bf45-c7d1-4e96-be34-781843957f1e"
        },
        {
            "id": "25cbe0e7-4812-4cd2-ab72-38fee0c98ab2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "6438bf45-c7d1-4e96-be34-781843957f1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
    "visible": true
}