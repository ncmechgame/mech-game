{
    "id": "155d99fd-dce3-4116-bf86-9a7e5ff592d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBeam",
    "eventList": [
        {
            "id": "3e7a5185-705b-4380-a2f7-70e5153b18dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "155d99fd-dce3-4116-bf86-9a7e5ff592d8"
        },
        {
            "id": "b976eb68-a1b7-4211-847b-43e8ebd7b400",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "155d99fd-dce3-4116-bf86-9a7e5ff592d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26d857e7-b4e1-40e2-a7f0-ba101d9bf8d1",
    "visible": true
}