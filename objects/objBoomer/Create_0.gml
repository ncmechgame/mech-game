
//set player object
player = objBody;

//state
state = "alive";


delay = 50;

//acid explosion particle
particle1 = part_type_create();
part_type_shape(particle1,pt_shape_flare);
part_type_size(particle1,0.16,0.96,0,0);
part_type_scale(particle1,1,1);
part_type_color3(particle1,65280,16384,0);
part_type_alpha3(particle1,0.95,0.80,0.07);
part_type_speed(particle1,3,5,-0.40,0.50);
part_type_direction(particle1,0,359,1,0);
part_type_orientation(particle1,0,0,0,0,0);
part_type_blend(particle1,1);
part_type_life(particle1,20,40);
Sname = part_system_create();
emitter1 = part_emitter_create(Sname);


