{
    "id": "8dccc7d2-a56e-4813-aa1f-93c83e1b47a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHangerBack",
    "eventList": [
        {
            "id": "be2064c3-5d53-4cda-94cb-f55917e56976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8dccc7d2-a56e-4813-aa1f-93c83e1b47a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4160bcf-e1d3-4e43-86bc-8a726c910ded",
    "visible": true
}