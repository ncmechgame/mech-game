/// @description Insert description here
// You can write your code in this editor

global.Pause = true;
global.ViewWidth = camera_get_view_width(view_camera[0]);
global.ViewHeight = camera_get_view_height(view_camera[0]);


//not sure wtf this is
global.KeyRevert = ord("X");


global.KeyEnter = vk_enter;
global.KeyLeft	= vk_left;
global.KeyRight = vk_right;
global.KeyUp	= vk_up;
global.KeyDown	= vk_down;
global.KeySpace	= vk_space;

display_set_gui_size(global.ViewWidth, global.ViewWidth);


enum MenuPage {
	main,
	settings,
	audio,
	graphics,
	controls,
	length
}

enum MenuElementType{
	script_runner,
	page_transfer,
	slider,
	shift,
	toggle,
	input,
	length
}


//CREATE MENU PAGES
//Each item in the menu has a different purpose, either change a page or run a script, etc...

//MAIN MENU
ds_menu_main = CreateMenuPage(
	["RESUME",		MenuElementType.script_runner,	ResumeGame],
	["SETTINGS",	MenuElementType.page_transfer,	MenuPage.settings],
	["EXIT",		MenuElementType.script_runner,	ExitGame]
);

//SETTINGS
ds_settings = CreateMenuPage(
	["AUDIO",		MenuElementType.page_transfer,	MenuPage.audio],
	["GRAPHICS",	MenuElementType.page_transfer,	MenuPage.graphics],
	["CONTROLS",	MenuElementType.page_transfer,	MenuPage.controls],
	["BACK",		MenuElementType.page_transfer,	MenuPage.main]
);

//AUDIO
ds_menu_audio = CreateMenuPage(
	["MASTER",	MenuElementType.slider,			ChangeVolume,		1,	[0,1]],
	["SOUNDS",	MenuElementType.slider,			ChangeVolume,		1,	[0,1]],
	["MUSIC",	MenuElementType.slider,			ChangeVolume,		1,	[0,1]],
	["BACK",	MenuElementType.page_transfer,	MenuPage.settings]
);


//GRAPHICS
ds_menu_graphics = CreateMenuPage(
	["RESOLUTION",	MenuElementType.shift,			ChangeResolution,	0,	["384 x 216", "768 x 432", "1152 x 648", "1536 x 874", "1920 x 1080"]],
	["WINDOW MODE",	MenuElementType.toggle,			ChangeWindowMode,	1,	["FULLSCREEN", "WINDOWED"]],
	["BACK",		MenuElementType.page_transfer,	MenuPage.settings]
);

//CONTROLS
ds_menu_controls = CreateMenuPage(
	["UP",		MenuElementType.input,			"KeyUp",		vk_up],
	["LEFT",	MenuElementType.input,			"KeyLeft",		vk_left],
	["RIGHT",	MenuElementType.input,			"KeyRight",		vk_right],
	["DOWN",	MenuElementType.input,			"KeyDown",		vk_down],
	["SPACE",	MenuElementType.input,			"KeySpace",		vk_space],
	["BACK",	MenuElementType.page_transfer,	MenuPage.settings]
);

//need a way to chnage which menu page we are on
page = 0;

//store all of our data structures (grids) in an array of menu pages
menu_pages = [ds_menu_main, ds_settings, ds_menu_audio, ds_menu_graphics, ds_menu_controls];

//save which element of the array you are on
var array_len = array_length_1d(menu_pages);

for (var i = 0; i < array_len; i++){
	menu_option[i] = 0; 
}

inputting  = false;



//This code should be in a persistent controller objects create event
audio_group_load(audiogroup_music);
audio_group_load(audiogroup_sfx);



