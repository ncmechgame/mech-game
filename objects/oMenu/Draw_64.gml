/// @description 



if (!global.Pause) exit;

//store the width and height
var gw = global.ViewWidth, gh = global.ViewHeight;

//only drawing one pagte at a time so only need to access to one
//menu pages array stores all the ids of the grids

var ds_grid = menu_pages[page];
var ds_height = ds_grid_height(ds_grid); //number of elements in teh grid, main menu has 3 entries, settings 5

//how far the elements are away from each other and the dividing line
var ybuff = 32, xbuff = 16;

//spacing for drawing
var start_y = (gh/2) - ((((ds_height-1)/2) * ybuff));
var	start_x = gw/2;
var ltx = start_x - xbuff;
var lty, xo;



//****************************************************************
//Draw Pause Menu "Back"
//*****************************************************************
var c = c_black;
//draw_rectangle_color(0,0,gw,gh,c,c,c,c,false);


//*****************************************************************
//Draw Elements on Left Side
//*****************************************************************

//set text to be right justified and centered
draw_set_text(c_white, fntXolonium, fa_right, fa_middle);

for (var yy = 0; yy < ds_height; yy++){
	lty = start_y + (yy * ybuff);
	
	xo = 0;
	c = c_white;
	
	if (yy == menu_option[page]){
		c = c_orange;
		xo = -(xbuff/2);  
	}
	
	draw_text_color(ltx + xo, lty, ds_grid[# 0, yy], c, c, c, c, 1);
}

//*******************************************************************
//Draw Dividing Line
//********************************************************************
draw_line(start_x, start_y, start_x, lty);


//**********************************************************************
//Draw Elements on Right Side
//********************************************************************
draw_set_halign(fa_left);

var rtx = start_x + xbuff
var rty;

for (var yy = 0; yy < ds_height; yy++) {
	
	rty = start_y + (yy * ybuff);
	
	switch(ds_grid[# 1, yy]) {
		//we dont use a shift type
		
		case MenuElementType.shift: 
			var current_val = ds_grid[# 3, yy];
			var current_array = ds_grid[# 4, yy];
			
			var left_shift = "<<";
			var right_shift = ">>";
			c = c_white;
			
			
			//only draw the arrows if you can actually shift in that direction
			if (current_val == 0) left_shift = "";
			if (current_val == array_length_1d(ds_grid[# 4, yy])-1) right_shift = "";
			
			if (inputting and yy == menu_option[page]) 
				{c = c_yellow;}
			
			draw_text_color(rtx, rty, left_shift+current_array[current_val] + right_shift, c, c, c, c, 1);
			
		break;
		
		
		case MenuElementType.slider:
			var len = 64;
			var current_val = ds_grid[# 3, yy];
			var current_array = ds_grid[# 4, yy];
			var circle_pos	= ((current_val - current_array[0]) / (current_array[1] - current_array[0]));
			c = c_white;
			
			draw_line_width(rtx, rty, rtx + len, rty, 2);
			
			if (inputting and yy == menu_option[page]) 
				{c = c_yellow;}
			draw_circle_color(rtx + (circle_pos*len), rty, 6, c, c, false);
			draw_text_color(rtx + (len * 1.2), rty, string(floor(circle_pos*100)) + "%", c, c, c, c, 1);
			
			
		break;
		
		case MenuElementType.toggle:
		
			var current_val = ds_grid[# 3, yy];
			var c1, c2;
			c = c_white;
			
			if (inputting and yy == menu_option[page]) 
				{c = c_yellow;}
			
			//change color based on whether on or off is selected
			if (current_val == 0) {
				c1 = c;
				c2 = c_dkgray;
			}
			else {
				c1 = c_dkgray;
				c2 = c;
			}
			
			draw_text_color(rtx, rty, "ON", c1, c1, c1, c1, 1);
			draw_text_color(rtx + 32, rty, "OFF", c2, c2, c2, c2, 1);
		
		break;
		
		case MenuElementType.input:
			var current_val = ds_grid[# 3, yy];
			var string_val;
			
			
			switch(current_val) {
				case vk_up:		string_val = "UP KEY"; break;
				case vk_down:	string_val = "DOWN KEY"; break;
				case vk_left:	string_val = "LEFT KEY"; break;
				case vk_right:	string_val = "RIGHT KEY"; break;
				case vk_space:	string_val = "SPACE KEY"; break;
				default:		string_val = chr(current_val); break;
			}
			
			c = c_white;
			if (inputting and yy == menu_option[page]) 
				{c = c_yellow;}
			draw_text_color(rtx, rty, string_val, c, c, c, c, 1);
			
		break;
	}
}


draw_set_valign(fa_top);



















