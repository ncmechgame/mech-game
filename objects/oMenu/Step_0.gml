/// @description

if (!global.Pause) exit;

input_pressed_up	= keyboard_check_pressed(global.KeyUp);
input_pressed_down	= keyboard_check_pressed(global.KeyDown);
input_pressed_enter	= keyboard_check_pressed(global.KeyEnter);

var ds_grid		= menu_pages[page];
var ds_height	= ds_grid_height(ds_grid);

//check to see if we are changing values of the menu
if inputting {
	
	//we dont use shift
	switch(ds_grid[# 1, menu_option[page]]) {
		
		case MenuElementType.shift:
			var hinput = keyboard_check_pressed(global.KeyRight) - keyboard_check(global.KeyLeft);
			if(hinput != 0){
				//audio effect here
				ds_grid[# 3, menu_option[page]] += hinput;
				ds_grid[# 3, menu_option[page]] = clamp(ds_grid[# 3, menu_option[page]], 0, array_length_1d(ds_grid[# 4, menu_option[page]])-1);
			}
		break;
		
		case MenuElementType.slider:
		
			switch(menu_option[page]) {
				case 0: if (!audio_is_playing(sndTestLaser_master)) { audio_play_sound(sndTestLaser_master, 1, false);	} break;
				case 1: if (!audio_is_playing(sndTestLaser))		{ audio_play_sound(sndTestLaser, 1, false);			} break;
				case 2: if (!audio_is_playing(sndTestLaser_music))	{ audio_play_sound(sndTestLaser_music, 1, false);	} break;
			}
		
			var hinput = keyboard_check(global.KeyRight) - keyboard_check(global.KeyLeft);
			var current_array = ds_grid[# 4, menu_option[page]];
			if(hinput != 0){
				//audio effect here
				ds_grid[# 3, menu_option[page]] += hinput*0.01;
				ds_grid[# 3, menu_option[page]] = clamp(ds_grid[# 3, menu_option[page]], current_array[0], current_array[1]);
				
				//change volume while sliding to get isntant feedback
				script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]);
			}
			
		break;
		
		case MenuElementType.toggle:
		
			var hinput = keyboard_check_pressed(global.KeyRight) - keyboard_check(global.KeyLeft);
			if(hinput != 0){
				//audio effect here
				ds_grid[# 3, menu_option[page]] += hinput;
				ds_grid[# 3, menu_option[page]] = clamp(ds_grid[# 3, menu_option[page]], 0, 1);
			}
			
		break;
		
		case MenuElementType.input: 
		
			var kk = keyboard_lastkey;
			if (kk != vk_enter) {
				//audio in here
				//if (kk != ds_grid[# 3, menu_option[page]]) 
				
				ds_grid[# 3, menu_option[page]] = kk;
				variable_global_set(ds_grid[# 2, menu_option[page]], kk);
			}
		
		break;
	} 
	
	
}
else {
	var oChange		= input_pressed_down - input_pressed_up;
	if (oChange != 0) {
		menu_option[page] += oChange;
		if (menu_option[page] > ds_height-1) 
			{menu_option[page] = 0;}
		if menu_option[page] < 0 
			{menu_option[page] = ds_height -1;}
		//audio_play_sound()
	}
}

if (input_pressed_enter) {
	switch(ds_grid[# 1, menu_option[page]]) {
		case MenuElementType.script_runner: script_execute(ds_grid[# 2, menu_option[page]]); break;
		case MenuElementType.page_transfer: page = ds_grid[# 2, menu_option[page]]; break;
		
		//dont need a break until the end for these ones so we can update the inputting option
		case MenuElementType.shift: if (inputting) {script_execute(ds_grid[# 2, menu_option[page]]);} 
		case MenuElementType.slider: //if (inputting) {script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]);} break;
		case MenuElementType.toggle: if (inputting) {script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]);}
		case MenuElementType.input: //if (inputting) {script_execute(ds_grid[# 2, menu_option[page]]);} break;
			inputting = !inputting;
			break;
	} 
	
	//audio
}
	

if (keyboard_check_pressed(vk_escape) && (!inputting)){
	switch (page){
		case 0: instance_destroy(); break;
		case 1: page = 0; break;
		case 2: page = 1; break;
		case 3: page = 1; break;
		case 4: page = 1; break;
	}
	
}












