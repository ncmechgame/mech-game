{
    "id": "8dba7e63-fba6-4558-b45e-59b9d9de9ebf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_back",
    "eventList": [
        {
            "id": "84f47763-039d-44d1-a11a-67eed828f0f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 9,
            "m_owner": "8dba7e63-fba6-4558-b45e-59b9d9de9ebf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2d8fbbc-2925-47e2-82f1-dec03093fc7e",
    "visible": true
}