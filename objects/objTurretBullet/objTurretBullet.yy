{
    "id": "d33f03e5-34dc-4546-93c9-40c410f14c30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTurretBullet",
    "eventList": [
        {
            "id": "39458b15-4557-4e4e-8e4c-e5f8c7a9cb37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d33f03e5-34dc-4546-93c9-40c410f14c30"
        },
        {
            "id": "2cd24adf-6daa-4e10-a044-4c9d571a6875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d33f03e5-34dc-4546-93c9-40c410f14c30"
        },
        {
            "id": "2395fc7b-7b9b-4273-88c1-6ba0b2eb953c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d33f03e5-34dc-4546-93c9-40c410f14c30"
        },
        {
            "id": "e9efdbb7-f26d-4a96-a3f1-15db02ad8d6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d33f03e5-34dc-4546-93c9-40c410f14c30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0223a30e-1df5-4098-8e95-c322c4537e49",
    "visible": true
}