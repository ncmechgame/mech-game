{
    "id": "c7741d31-3428-4baa-9ee7-bdd4f96d3193",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_medium",
    "eventList": [
        {
            "id": "50a98724-113b-44f5-a9af-8190a4b8369c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7741d31-3428-4baa-9ee7-bdd4f96d3193"
        },
        {
            "id": "acc84f89-caa6-4931-9c93-b1ec13466ebb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "c7741d31-3428-4baa-9ee7-bdd4f96d3193"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acb53a44-d572-4212-aa4c-8d9073e33b26",
    "visible": true
}