{
    "id": "05d34371-3c0c-486b-ba51-6ba927bd6a73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_light",
    "eventList": [
        {
            "id": "6b165737-91d2-4996-9b88-b016255314dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05d34371-3c0c-486b-ba51-6ba927bd6a73"
        },
        {
            "id": "1c45c30d-589c-4e57-a820-7c3eabdcf48d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "05d34371-3c0c-486b-ba51-6ba927bd6a73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bed91c98-a9ed-43e8-805b-1436a4a370cb",
    "visible": true
}