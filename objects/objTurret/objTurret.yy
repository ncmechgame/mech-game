{
    "id": "d9295637-15ce-47a6-9241-e6ed670614fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTurret",
    "eventList": [
        {
            "id": "f82bc00b-645d-4313-92ef-1eec75a434e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9295637-15ce-47a6-9241-e6ed670614fe"
        },
        {
            "id": "53aa7e2f-f325-467b-bdfa-e3fe47eabc5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9295637-15ce-47a6-9241-e6ed670614fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3a148fab-9e45-418d-a76a-a67c4d346b61",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
    "visible": true
}