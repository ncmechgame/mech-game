/// @description Insert description here
// You can write your code in this editor



// if player in range, shoot
if (distance_to_object(player) < range) and state = "alive" {
	state = "firing"
	
}

switch(state){
	case "alive":
		if image_index > 5
			image_index = 0;
			
		break;
	case "firing":
		shotDelay -= 1;
		if shotDelay = 18 {
			sprite_index = sprTurretShoot 
			image_index = 0
		}
		if shotDelay <= 0 {
			bullet = instance_create_depth(self.x -4, self.y-3, 2, objTurretBullet);
			bullet.direction = point_direction(x, y, player.x, player.y) + irandom_range(-5, 5);
			bullet.speed = 5;
			bullet.depth = -10;
			sprite_index = sprTurret
			state = "alive";
			shotDelay = shotDelayMax;
		}
		
		break;
}
