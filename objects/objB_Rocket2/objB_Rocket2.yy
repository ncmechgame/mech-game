{
    "id": "cbb17937-c674-4724-ad8a-53a99d2e2d5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Rocket2",
    "eventList": [
        {
            "id": "98248111-07f4-4392-bff9-3cabd52c049b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbb17937-c674-4724-ad8a-53a99d2e2d5a"
        },
        {
            "id": "54c4f20c-7b1b-42a6-b5bb-62dbf9029676",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cbb17937-c674-4724-ad8a-53a99d2e2d5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
    "visible": true
}