{
    "id": "4e5d3c38-7ca2-4a1f-b8b3-5e0ec0d2e4c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuBack",
    "eventList": [
        {
            "id": "7b16d4f1-606f-40f7-9f31-242230b05cad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e5d3c38-7ca2-4a1f-b8b3-5e0ec0d2e4c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a716c514-4e99-48ab-8b44-fdd2dacc339c",
    "visible": true
}