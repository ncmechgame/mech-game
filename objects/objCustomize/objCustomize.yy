{
    "id": "08be37f7-547f-48ab-938f-beda68051729",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCustomize",
    "eventList": [
        {
            "id": "43b90390-0c32-43ae-8d64-9bf290ee5ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "08be37f7-547f-48ab-938f-beda68051729"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8bbd2ef-112c-4c9d-b238-c7b5b0ec0dfa",
    "visible": true
}