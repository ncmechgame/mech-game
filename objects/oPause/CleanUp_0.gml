/// @description

if surface_exists(screen) {surface_free(screen);}
instance_activate_all();


part_system_automatic_update(global.pSystemForeground, true);
part_system_automatic_update(global.pSystemMiddleForeground, true);
part_system_automatic_update(global.pSystemMiddleBackground, true);
part_system_automatic_update(global.pSystemBackground, true);

