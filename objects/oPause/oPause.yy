{
    "id": "73abec67-5d8e-448e-b0e6-4d0e9d560b90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPause",
    "eventList": [
        {
            "id": "6bc24cea-fd2c-4fa2-b921-d47a83125cc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73abec67-5d8e-448e-b0e6-4d0e9d560b90"
        },
        {
            "id": "974cf679-e52f-4e0a-951c-0a2206c90c13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "73abec67-5d8e-448e-b0e6-4d0e9d560b90"
        },
        {
            "id": "6f089d25-79fa-48dc-9c54-09f5d37c5188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "73abec67-5d8e-448e-b0e6-4d0e9d560b90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}