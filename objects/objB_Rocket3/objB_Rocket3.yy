{
    "id": "4e48f8ba-a4ac-4567-b013-fc8584c3c9f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Rocket3",
    "eventList": [
        {
            "id": "d8ba0177-eaba-40ac-a097-fdae52fe0fa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e48f8ba-a4ac-4567-b013-fc8584c3c9f1"
        },
        {
            "id": "70735827-9d6a-4920-b80b-8a379cb8d222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "4e48f8ba-a4ac-4567-b013-fc8584c3c9f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
    "visible": true
}