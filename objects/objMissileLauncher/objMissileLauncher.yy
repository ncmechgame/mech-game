{
    "id": "e8912a07-2e08-43b6-a8fb-ebe290b7fefa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMissileLauncher",
    "eventList": [
        {
            "id": "1bdf8fe4-e118-4d69-a5e6-f72e5d0a7680",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8912a07-2e08-43b6-a8fb-ebe290b7fefa"
        },
        {
            "id": "f420128c-8b10-4515-b35d-689d7a3cbbad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8912a07-2e08-43b6-a8fb-ebe290b7fefa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e73f5602-d702-4c5d-96db-2855c3d81aa4",
    "visible": true
}