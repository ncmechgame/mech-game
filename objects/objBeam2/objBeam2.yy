{
    "id": "a3132a2b-6444-4ffe-9147-27f55091589e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBeam2",
    "eventList": [
        {
            "id": "a6f52906-e847-4d24-90b9-9942e8bcd4fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3132a2b-6444-4ffe-9147-27f55091589e"
        },
        {
            "id": "9bc1951d-076d-4dff-9413-3472072c230a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3132a2b-6444-4ffe-9147-27f55091589e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1b54cc9-2ecf-43ef-baab-6b58b03b4abd",
    "visible": true
}