{
    "id": "659a7b8e-3378-4ad1-b9aa-521171d51f56",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objB_Lazor",
    "eventList": [
        {
            "id": "dbf74972-9ac6-4d53-b6f3-97e12615f740",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "659a7b8e-3378-4ad1-b9aa-521171d51f56"
        },
        {
            "id": "1e58c7a7-ede1-43f0-a2f5-9c200ba2da07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "659a7b8e-3378-4ad1-b9aa-521171d51f56"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
    "visible": true
}