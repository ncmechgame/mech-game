{
    "id": "7bcb9303-e5b1-4b10-8b82-4f8b8f9aa606",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCam",
    "eventList": [
        {
            "id": "6c535b6a-6930-4e1b-b0c8-e19a9ad8c97b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7bcb9303-e5b1-4b10-8b82-4f8b8f9aa606"
        },
        {
            "id": "335bba2f-6ac5-4851-9192-93ab40045ba9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7bcb9303-e5b1-4b10-8b82-4f8b8f9aa606"
        },
        {
            "id": "d6a19c8b-eca0-40c9-8ea7-3dc6d28ea22a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7bcb9303-e5b1-4b10-8b82-4f8b8f9aa606"
        },
        {
            "id": "e152548d-e1e6-4c57-9e0e-2980f3f41e69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7bcb9303-e5b1-4b10-8b82-4f8b8f9aa606"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}