/// @description

if instance_exists(oPlayer)
{
    var xTo, yTo;
    
    //Move camera towards cursor direction
    move_towards_point(mouse_x, mouse_y, 0);
    var aimDistance = distance_to_point(mouse_x, mouse_y);
    var aimDirx = oPlayer.x + lengthdir_x(min(128, aimDistance), direction);
    var aimDiry = oPlayer.y + lengthdir_y(min(128, aimDistance), direction);

    xTo = aimDirx;
    yTo = aimDiry;

    
    //Commit to camera movement
    x += round((xTo-x)/10);
    y += round((yTo-y)/10);
	
    
}


///Screen Shake
if (intensity != 0)
{
    intensity += ((0 - intensity) / ease);
	//camera_set_view_angle
    camera_set_view_angle(view_camera[0], random_range(-anglerange, anglerange) * intensity );
        
    x += random_range(-xrange, xrange) * intensity;
    y += random_range(-yrange, yrange) * intensity    ;
}

