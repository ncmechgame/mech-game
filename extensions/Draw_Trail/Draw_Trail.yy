{
    "id": "bcaca247-5495-4269-bbb1-72b70b326475",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Draw_Trail",
    "IncludedResources": [
        "Sprites\\spr_zigzag",
        "Backgrounds\\bck_grid",
        "Scripts\\draw_trail",
        "Scripts\\trail_init",
        "Scripts\\draw_trail_ext",
        "Scripts\\trail_calculate",
        "Scripts\\trail_destroy",
        "Objects\\obj_trail1",
        "Objects\\obj_trail_ext1",
        "Objects\\obj_trail2",
        "Objects\\obj_trail_ext2",
        "Objects\\obj_spawner",
        "Rooms\\room_example",
        "Included Files\\Instructions.pdf"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 123146089894126,
    "date": "2018-04-14 02:06:51",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.killer.drawtrail",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "version": "1.0.6"
}