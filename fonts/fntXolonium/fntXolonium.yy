{
    "id": "dfd9df37-cafb-43d2-a7ed-6462509accab",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntXolonium",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Xolonium",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1937dab2-19bd-463b-bb34-97cb2248bba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 98
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6ab91ee8-a7ed-4183-96ba-8c6990b44ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 180,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7362ee65-7397-42e3-be14-a1007ffec270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6076b581-2192-482a-a2a4-d51655ed1d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "73a4b6b9-a235-4dcf-82d7-b2e1463acc10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 197,
                "y": 26
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e3824326-9c75-47cc-af12-ac324ad7b6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bf3c91e7-6f3e-4091-88ca-e378a3bc095c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "488e6ba6-2cd1-4deb-82e5-929f632110e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 201,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "22060dc2-925b-4809-85b9-e090c603ccc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c3ad553a-89c6-412b-a5b3-276bd88a99e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3897d3d2-bcf1-44e8-8c6f-1b345d6854ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 184,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d403f4a0-f124-4043-97fa-37e976e6201b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "795c47b3-2e0d-4029-b2cf-37bf6995ddc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 161,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "688a3ee9-81c0-4b47-b43b-57f1ff90b7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "60cb6881-f0db-47a6-bbab-ac6a635c6433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 168,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d5d186e1-9858-41be-8ed6-5f6b4492c13c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cc7dbb34-784b-4a08-b02a-dfabe967a42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "baa54973-06d9-487c-a1ad-89c56b3e979c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 2,
                "shift": 13,
                "w": 7,
                "x": 65,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5664f9fb-8dc8-45cd-9cfb-e075b8a9cdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3d6124bf-0689-4507-8c03-e01b57ede786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "39ad67e7-b4d4-4c54-934f-464d7b279d51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "26bec004-dd1e-4171-b093-a4421017cb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d75bf064-8bb0-411e-95eb-35b337b4b499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "902185ec-c566-4294-ba1b-0b6a77cb035d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5f789fec-bbc8-4340-a9b8-bc12773b5ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5435a0ca-250a-408f-946a-2c8fc6918c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7df0f822-4d23-4db0-b37f-fee12cf36e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 174,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "942cf886-7d50-4c25-a212-a63c189ea82f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 154,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2a3cd3a8-3e6d-483a-a776-978f7af8f206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fd86c54d-00de-4c48-87fc-2cb93b683155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eab3546f-ae99-40ef-9068-687d8841bec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 132,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "25b63957-77b6-4f9e-863b-8e4cfb5172d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c683b72f-14c1-4a19-81fc-e0e993fb1dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5f1a2ef3-baad-4176-9470-bf63eedf0f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "480de69f-e653-444f-8651-25fbf325c666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 26
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fd84ec80-cf93-4cf8-bbd1-70984f7552b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bc55e0a5-c1f9-451e-a277-689012b71970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 26
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dd24be1e-f02c-4106-ae9a-2f3e52b68c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cf840663-48f6-4a47-8871-db840b7523ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0c054c8c-6a57-4149-89cb-2f3dc8a8e47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 26
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b7d3d12a-1c92-4b03-86fc-4b8017304dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 26
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9eb49a8a-4b29-4d69-a962-56e9c824bd9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 206,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "df88cb46-387b-47eb-b92a-7a509642c8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1030728b-b6cd-46c7-974b-b9841ae7ea70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 32,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e8ec7a80-7788-40f3-bf70-3690b9566989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 26
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d6a69008-b5fb-4135-ad4e-67a4e3fde1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "936b934b-392f-4335-b278-175bd074921d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 167,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0c5a902b-5478-49c7-8132-6c58e178705e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4c4ea9f8-8a50-4e37-ae35-b74a70d79d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 17,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "228670e4-e0e4-4829-8270-c55ec8cf6f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5d0ee79f-37c6-4288-8436-3e494824b358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 152,
                "y": 26
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c551c210-a2e9-4418-af92-7db2087ab297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "718ccbb9-bdcc-43dd-a71a-90673f64042c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "74f50725-f75e-4fcc-82ee-a099b022d244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 26
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2a41103a-a560-49d2-9c47-a0ddfaef8fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fde5f357-5018-43ab-ac08-ca963dde0bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c551f69d-28c8-452e-8ebc-972409963a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "02ae605b-63a8-4903-944c-a220e1770f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "744be2d1-d343-4cf0-b6c7-95ed6197a1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 77,
                "y": 26
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2f4431c1-bf3d-40aa-bbfd-cc7be289db56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8b5381f5-e888-4f24-9170-050e551e1beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2d3d01fc-1b05-4329-9701-34a7a6d5c802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 138,
                "y": 98
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1e6ee1cf-3386-4ebd-9fbc-df8b6ed896fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0308fc56-1cb1-4eb8-9389-32da83f9d589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 184,
                "y": 50
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3a5843d2-91a9-486a-8fa5-1204a4f7ca68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 130,
                "y": 98
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e0d5dd92-7494-44d0-9971-eff7e75bc60b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3c90ca18-76ec-4144-900d-b643d78c8064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8fab2008-a9c6-4dc8-909a-069efa901a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 208,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "74f93611-8358-45f8-a878-f0a4d256a89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8c5dd09b-021b-44bb-8826-9328762cf064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "64e728ad-fe79-4682-adf6-f6298ad296b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 122,
                "y": 98
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "580f3c2d-6ac0-44f5-a319-e9ffb3d7f84a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 239,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cb24f133-65ee-447c-803d-dc4993160edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a6696e6e-43ee-4770-a4fe-6b7af99fd465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 196,
                "y": 98
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4a2aca26-3df7-4137-a478-f7d1a3ae8a1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 114,
                "y": 98
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "14a66148-67a7-4b7c-8f3b-ff0482007c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "84edb658-2df1-4060-9665-7056c30d82ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 191,
                "y": 98
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b60d2fcb-233c-4ce2-adca-49ca5757b7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1b35e414-e2e6-44c4-8326-46698179a449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "664a50ec-80f3-4399-9ae1-43680561b7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8dd151e8-e0a6-4a58-bc3f-08c74283e71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2edc645a-6a0b-4e2d-9c1b-15c2d20541ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ab6a6513-88dd-4a0f-b596-031b0b7c8d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 98
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "34d42f52-487b-4b14-9baf-d50155974bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 98
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f599bc97-c11a-41a5-a416-4186a50c44b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 98
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "53b55374-dfda-46a3-bb3a-9cc8839bd922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c047d6aa-ad93-4eb5-9626-e40766a9a4b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "08faad65-3c61-4ecd-8d01-e245bce5b776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7b3fc519-6b91-46dd-889c-2f8379c32703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 212,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "475808dd-5588-4d3f-b40c-5a761985f1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b344e6aa-360f-479c-90d3-5f85921fa965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 226,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b6ce3d06-e726-4afa-8a21-415811c28652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 244,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "583c9265-1cbf-44f4-ad42-48d6b822db68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 186,
                "y": 98
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6bf4343b-abdf-48ad-9a2d-72add3cf15ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 98
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b7eecb19-8d8f-4b2f-9786-9a7b50f0ed82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 119,
                "y": 74
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b19e1cad-0cdd-4638-b1fa-3313c065a723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "cb3546a9-ab6a-46e2-b04f-d975aa2a191c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 74
        },
        {
            "id": "b20f4221-a5ce-434a-b011-d8b83df8aede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "4ec5e859-429c-4fb5-9ade-b08704b7171b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "44d4f753-502c-4b48-b7a3-9e4e023c675b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "0b9a277d-913f-48f0-b8f4-6ad2792b9909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "13ce4d3b-be91-4e46-b532-21c5104eb95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "b50f4179-7b2f-4d70-940a-04b04dcfde15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "76d28882-1db1-44ef-a7cd-a9d537cb5691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 198
        },
        {
            "id": "1171f8e7-dcd1-41b8-9953-574fdf5f827b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "b74b3db8-0fcb-49ac-aa16-6d190cace1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "2e30b649-325e-4608-8ded-e0bc2fdb5787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "e864c83c-f57c-42ff-87af-50780e339a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 308
        },
        {
            "id": "cec9e3d4-e9e9-48ef-b57c-5cf3c4eafa05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 506
        },
        {
            "id": "17f75f35-ac54-4863-9396-0f73317993fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 508
        },
        {
            "id": "1a7945bb-30e0-4774-8211-25b685b11157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 913
        },
        {
            "id": "232356a0-88f0-4397-87ff-e47e9ddab714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "d1529266-0623-4e7a-9000-b1fe87fb25ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 923
        },
        {
            "id": "dd28ab4b-8ec1-41aa-a614-3e5b30124083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1032
        },
        {
            "id": "4c57a2f4-3325-4c6d-8384-558b9d1c15c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1033
        },
        {
            "id": "62cf5804-fdcf-45e2-8716-14f16603bc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1040
        },
        {
            "id": "051fe347-41e9-405a-928b-63bd721c3119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1044
        },
        {
            "id": "4eaef42c-aaa4-4944-ac7a-79d9549de40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1051
        },
        {
            "id": "abd44795-9028-496f-8d8d-51c667cb55d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1076
        },
        {
            "id": "8b3c174f-eb30-4327-a5d5-ec90c3a3a2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1232
        },
        {
            "id": "c587b878-c188-4b26-995d-12dd0693e2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1234
        },
        {
            "id": "5bb40cc6-bc8b-457d-9bea-02837787dc65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1236
        },
        {
            "id": "30d27765-c9e3-445a-87d1-6fc6f12a5bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "b7232241-5deb-4785-9acd-611ec919ad5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 74
        },
        {
            "id": "61169b07-f9c4-4d0c-b2a4-f586d551c22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "6165b2d3-76cd-4d47-86d7-2409b0c9ce8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "3ccd1082-d6f8-49f5-9dda-974011d562a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "0534cab4-c2a4-4977-a142-0c8b96164e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "d0badd00-4ff5-4c85-b7cb-49a54719bd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "b1c24953-35be-4f86-b83a-5b02df0fe163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "27e4da0f-7e80-4013-be63-3573d8fc9f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "a1efd7ed-05b9-4450-8351-567387c5ade8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "11e3a501-8bfd-421d-9947-562ab00e15da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 258
        },
        {
            "id": "b9c4cbe2-b5fe-4830-ad70-88183bddd11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 260
        },
        {
            "id": "a391ad10-bdfb-4c38-8fdc-127429aafc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 308
        },
        {
            "id": "73ad9dd2-df5b-477f-9e8d-daca8998e215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 506
        },
        {
            "id": "fcbc7e37-4732-4509-8f18-95ec0beaa3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 508
        },
        {
            "id": "1d8dba94-1bdf-4a2d-a864-cc0bc36da3e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 913
        },
        {
            "id": "9c68e51d-08b0-421f-a73a-28a71646031c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "df558dd4-61ef-4ef6-be50-ab00e670358c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 923
        },
        {
            "id": "2ab0e16a-264e-4211-85b0-1f7051db9862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1032
        },
        {
            "id": "b45f0cb8-be0e-43d8-a09b-4666157fe7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1033
        },
        {
            "id": "bf0e2258-93e2-4729-9ed4-090c8b83b748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1040
        },
        {
            "id": "021b8d06-65a8-4f6a-bc3b-edfb531a5adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1044
        },
        {
            "id": "e0d73056-2b53-4fd0-9217-0641a56f2422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1051
        },
        {
            "id": "949a6898-c517-457d-94e0-8afc44199bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1076
        },
        {
            "id": "18b8bef8-8403-41b3-b9cb-c80329ca167d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1232
        },
        {
            "id": "54b048ac-9b84-4a1c-bdd1-c25b0613f8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1234
        },
        {
            "id": "5abdc093-b7df-4c6f-82d8-30af24ef0767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1236
        },
        {
            "id": "801008f0-9c32-4e26-8f2c-15cd8c55b42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 65
        },
        {
            "id": "b48c4e5d-5d01-4c67-b86d-5507ff91a0c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 99
        },
        {
            "id": "93418581-40d2-4c47-abe9-3380e2be3b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "82462f40-3e4e-4a53-8b51-d05f22d828a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 101
        },
        {
            "id": "7dccd628-2d8a-4cea-be37-f977187a3a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "cacc1cc3-a90a-4d42-8e82-3f93affa3e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 106
        },
        {
            "id": "ea56c2cd-89cc-4fe4-9d46-ea76c2e35043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 111
        },
        {
            "id": "fd357bb4-2702-4e70-977e-f3194ec7bb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 113
        },
        {
            "id": "3c266338-1eda-4dc5-ab83-443e5415b83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 192
        },
        {
            "id": "7b634fb9-2117-4a8d-8c5f-87f4ae3073b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 193
        },
        {
            "id": "c5191000-020c-44aa-94d2-a09d14c7dbd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 194
        },
        {
            "id": "2b4c0e44-defb-498b-b7ab-3410adae1d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 195
        },
        {
            "id": "3126519d-c9f2-4b1e-b600-4647886a6302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 196
        },
        {
            "id": "be2dec91-c501-4280-a477-0b4fc6aa5a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 197
        },
        {
            "id": "f4b01936-7ac9-4ef1-9534-f4ad7310d32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 198
        },
        {
            "id": "0c98a613-7028-4fd9-85b8-e08c757bf6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 231
        },
        {
            "id": "ef9cabe2-9fca-455a-b207-ebc0a78b932c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 232
        },
        {
            "id": "939110fc-ec87-47bc-82b4-d9b08797d9e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 233
        },
        {
            "id": "819132c4-a71f-4632-b10e-5327101e260a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 234
        },
        {
            "id": "193cc1cf-ee80-47d4-b0ed-25efdfd9c5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 235
        },
        {
            "id": "befab5f4-d7ba-483f-8a52-cc788aa5e1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 242
        },
        {
            "id": "c5f11689-41f2-42ea-b11d-0dc4d5b3a327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 243
        },
        {
            "id": "9ee0e0c4-5859-4369-b014-15f82d4b7e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 244
        },
        {
            "id": "8caf55d2-8363-4103-9aea-71ca39633a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 245
        },
        {
            "id": "ec1dc09f-d9e0-46bc-9daf-7ff4c61b746a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 246
        },
        {
            "id": "f168316d-29df-4799-a14c-da1b8cebca34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 248
        },
        {
            "id": "22e9ae8c-560a-47c2-8a3a-1d60b59247bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 256
        },
        {
            "id": "33c4c809-b750-44d3-a7f4-cc08d982bfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 258
        },
        {
            "id": "deee09a2-8bd2-4d6c-a854-656e024126c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 260
        },
        {
            "id": "c41e48c5-5438-4020-91c0-13eadf9f9124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 263
        },
        {
            "id": "05268eb8-0618-4a72-9136-b790a0cec1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 265
        },
        {
            "id": "0cb6538a-d77c-4701-a75a-23d8e44b84c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 267
        },
        {
            "id": "c0ffea96-d894-41af-89bc-b452789cfe38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 269
        },
        {
            "id": "2eb1a13e-0435-4952-84ac-f0ef5772c76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 271
        },
        {
            "id": "a96cd589-87aa-4636-8fda-e5e65b92c5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 273
        },
        {
            "id": "3674a09a-4fbf-4ac6-bb29-29c4f51c8459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 275
        },
        {
            "id": "83dcb568-97d6-414e-8fa7-d32acfcc415c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 277
        },
        {
            "id": "9fe2f9a4-d012-4f0b-89e5-3b8d6855e1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 279
        },
        {
            "id": "21813d6b-0f12-4a2e-b7ae-5a510bb6a92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 281
        },
        {
            "id": "3b11ca2b-a402-4eeb-891d-4b67cbb9431c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 283
        },
        {
            "id": "1c4a0e41-a775-4675-9049-e9a4b8017497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 285
        },
        {
            "id": "6de0a73c-78d3-4e13-a15e-51066b6f9687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 287
        },
        {
            "id": "3533947e-fd3f-44c5-ad0b-b640ec719b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 289
        },
        {
            "id": "79608b5c-19c9-4ee1-9af3-e99bcc6cba52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 333
        },
        {
            "id": "b92dc769-27e6-436a-a46e-07470a9e84a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 335
        },
        {
            "id": "d461789a-6bc2-4b81-a601-2f73aa611520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 337
        },
        {
            "id": "bd13940c-52f6-4665-a5af-c8c4e1ed1a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 339
        },
        {
            "id": "35ca3c40-4836-4533-9b2f-e4c4ac692a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 506
        },
        {
            "id": "f7fdab87-086e-4eec-a209-a8fb00741ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 508
        },
        {
            "id": "40bf8a37-7d98-47fb-aa3a-59677aefeb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 511
        },
        {
            "id": "c84808a0-ab2d-46c1-a114-c113905b4e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 913
        },
        {
            "id": "159a4678-70cb-42d1-a855-35cb0e29b5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 916
        },
        {
            "id": "dd978608-76f4-4e27-acc2-6f8cc0446aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 923
        },
        {
            "id": "1cf843ef-3f63-400f-ab5f-487acd814642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 940
        },
        {
            "id": "11067012-2254-4c02-ae2f-86991a8e6ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 941
        },
        {
            "id": "b0d94894-1abc-4ec0-a77b-4f52a28366c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 945
        },
        {
            "id": "577b6b49-43e1-420c-9fd6-c0cac0589649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 949
        },
        {
            "id": "51f1c489-2d6c-4ef6-b73b-806aae036337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 959
        },
        {
            "id": "eda27691-ac27-4c54-bac5-5b33ff4adf4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 962
        },
        {
            "id": "b05db302-ae39-4498-a78a-3d6fa2395582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 963
        },
        {
            "id": "4cc4826e-641f-4670-8c70-01c7308da80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 972
        },
        {
            "id": "2fae6b69-ca51-4204-9af9-314431d35c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1040
        },
        {
            "id": "465178ed-249a-4c68-9ce9-70f13201fa29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1077
        },
        {
            "id": "110dc2e1-b137-4950-b823-d10d3f423e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1086
        },
        {
            "id": "01acc13f-2849-412a-a717-202b0bb23da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1089
        },
        {
            "id": "8793e6a5-d427-403a-9b9c-8bb9f96bb114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1092
        },
        {
            "id": "d9a6bba8-5ead-472c-9588-d88dd16ad41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1095
        },
        {
            "id": "83a49fbb-8e1e-4d19-ac49-7b5ba0bf1fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1104
        },
        {
            "id": "c0dc19c0-7a5e-4871-8c7e-a21724cc86a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1105
        },
        {
            "id": "2016b315-3d02-412e-862b-d90a802c5d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1108
        },
        {
            "id": "26497fb1-c653-43c6-a650-376df73ae5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 1112
        },
        {
            "id": "c184ed42-c9f1-437a-84ef-e579a354ec3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1139
        },
        {
            "id": "3f809f30-59b4-4e4e-a236-e6a23aed3fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1193
        },
        {
            "id": "1f5e57af-9877-46c9-9bc0-a60b20b27ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1195
        },
        {
            "id": "92ed2c37-828a-4b73-993f-84f5facd75c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1207
        },
        {
            "id": "f382cd03-9784-4e66-b929-b57f29c5034f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1209
        },
        {
            "id": "43656526-e5ec-4e3c-8f6e-14b8bb5fe901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1228
        },
        {
            "id": "f9edfed4-44bf-4a74-86de-ff6def017c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1232
        },
        {
            "id": "acc48c44-17aa-4e8d-8077-907d137911ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1234
        },
        {
            "id": "6c28c36f-4d0d-42af-aa8b-d19e24f3d976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1236
        },
        {
            "id": "c52df5be-3a61-442b-a46e-8f7485496e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1239
        },
        {
            "id": "4c3770d7-5f5b-47c7-97da-77019e511b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1241
        },
        {
            "id": "971ebfca-a0a6-413c-8331-313f0cce2027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1255
        },
        {
            "id": "2cf9923d-f37e-449d-a028-3eaa0381b5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1257
        },
        {
            "id": "dd465231-a113-4cbf-82f5-7c8264aa61d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 1269
        },
        {
            "id": "0b7f7b7a-6e0b-40b3-b606-1242403ed50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 7691
        },
        {
            "id": "d89141d1-a670-455a-b3af-d69e5a10e853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 65
        },
        {
            "id": "0a631076-4d4b-4d41-8149-31367e642160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 74
        },
        {
            "id": "5257733f-b2e9-4f4c-b241-0725f89f1c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 192
        },
        {
            "id": "bda7c730-c48c-428e-ae3a-c2be0b067d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 193
        },
        {
            "id": "5f866ceb-2553-455e-a3a1-58f883c60d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 194
        },
        {
            "id": "c6722a5c-c02e-4603-9f97-4064d287f31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 195
        },
        {
            "id": "cf611f60-fbdf-44d8-b1a8-de6c8303de0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 196
        },
        {
            "id": "de75e25b-1a69-451d-a250-cdbd0561eec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 197
        },
        {
            "id": "73c9bb9f-056d-4b94-8c8a-e7c7301a2381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 198
        },
        {
            "id": "6d5330e5-05bc-4b91-a203-0f1f5c7c0aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 256
        },
        {
            "id": "0fffb92f-7d9a-4a6a-b697-48fbf6acc35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 258
        },
        {
            "id": "261cffef-a9a1-4c6b-9a49-c9bb38e50688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 260
        },
        {
            "id": "4c4c8ef3-a7aa-4868-8785-2ddcabbe9149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 308
        },
        {
            "id": "e84acd19-a411-4557-9d96-41f2c1b76432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 506
        },
        {
            "id": "d6c55dc9-a24c-49c5-bf48-d68d2a09a0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 508
        },
        {
            "id": "dc5f2cd0-cfd2-4ad9-9a0c-3f92a7f20ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 913
        },
        {
            "id": "aaff0b5f-8972-4256-be4a-81090521a695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 916
        },
        {
            "id": "34e24113-293c-4091-bf2d-7040ff6ece01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 923
        },
        {
            "id": "28d75387-6793-4f5d-b19e-91b977484704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1032
        },
        {
            "id": "4e84acec-7ecd-4c3e-8c6f-80b9ce183744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1033
        },
        {
            "id": "48b3a84e-5ba5-461b-9454-9496c55573df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1040
        },
        {
            "id": "b4890e89-8969-4e23-89f9-6c1802af7513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1044
        },
        {
            "id": "991e1f11-1e4c-4ef5-9e1e-c1a22638b465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1051
        },
        {
            "id": "dae9427b-c192-4b33-bc31-c935d44f1541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1076
        },
        {
            "id": "3cedea56-c57e-4e10-bbd8-84d06608e66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1232
        },
        {
            "id": "ba67d183-1927-408b-8bbb-544f3c3a3824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1234
        },
        {
            "id": "3181ba78-7ad4-4b9e-87b3-b2ba2ad0cff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 1236
        },
        {
            "id": "49890137-1507-411e-b567-87e27f56c2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 63
        },
        {
            "id": "3e2ab621-2fdf-4de3-a37e-030cb61e9771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 84
        },
        {
            "id": "d9874e42-d78c-45e9-888e-8c238e829394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "5a85bfa2-8061-4780-b70e-7100018cfec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "bf31512f-5f75-41ea-a8c8-b9807e8622f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "ce1f306c-cf5a-4188-9468-56a391e2d328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 221
        },
        {
            "id": "b3d36cc7-1010-46f9-b369-bf55c5e4270b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 354
        },
        {
            "id": "73e25392-01be-4a09-a255-882a12feaf7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 356
        },
        {
            "id": "e1978c46-0216-4738-b707-3d42c5cbb442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 358
        },
        {
            "id": "e27baf3c-9967-4ff7-8f1f-e4e22b83da5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 372
        },
        {
            "id": "33e45a29-b413-4695-bdfe-a7d5ee15db9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 374
        },
        {
            "id": "c4faddd8-edd2-4572-ac03-d8ef29497a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 376
        },
        {
            "id": "e4e0c256-aa1c-4812-9c40-16bfad1aa6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 538
        },
        {
            "id": "6cc7af41-13aa-4d96-aedc-1de6203ebdf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 932
        },
        {
            "id": "28822473-189c-40c3-9bdb-58a0a28c9877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 933
        },
        {
            "id": "31b0407a-4f8e-42c1-995f-9ca884e85d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 939
        },
        {
            "id": "981a7793-3758-42e5-bb04-5bc094e8b325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1026
        },
        {
            "id": "9209bad1-b04a-499d-a6a8-a78b93b9aa6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1035
        },
        {
            "id": "426b9514-74ac-44c0-8acc-9db1e5ffcef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1038
        },
        {
            "id": "46509a61-ef2f-413a-a250-49288a0849e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1058
        },
        {
            "id": "2392866f-bb58-42d6-8ba2-1a64b515e1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1059
        },
        {
            "id": "806b023c-eb0b-4fbe-ae1b-c3d25d8466f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1063
        },
        {
            "id": "b3e3ce62-9d3d-4ecb-8099-592d2ea73123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1066
        },
        {
            "id": "7883c53c-1145-4b28-883f-ed522293ad52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1184
        },
        {
            "id": "23fb4780-ad68-4331-9965-cf4b66829a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1196
        },
        {
            "id": "7f27f29d-a874-4ead-995c-2d5f5f2e7ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1198
        },
        {
            "id": "6b069c91-a561-4fc1-a960-e9a9491034c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1200
        },
        {
            "id": "4d638ef7-3bfe-4d1b-a0c4-49d0bd130995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1204
        },
        {
            "id": "69195d5c-cc48-4024-a21b-cd3f9512ac3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1206
        },
        {
            "id": "889ef4fa-d0c1-4281-a7f4-f7bfbfa43cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1208
        },
        {
            "id": "666b3d62-49fe-456c-928e-f87dc234d920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1227
        },
        {
            "id": "32dce579-55e3-4d1f-9b54-04d8d7fc9092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1262
        },
        {
            "id": "df159197-1a18-47a3-b724-0d502f48b0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1264
        },
        {
            "id": "7d68dd4b-ca73-4572-83a6-f812666483ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1266
        },
        {
            "id": "3291854e-34df-40e1-9553-28d283b459f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1268
        },
        {
            "id": "ea268372-a5aa-4578-acb0-cfd6b94418af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7786
        },
        {
            "id": "85215455-9eb2-41a9-a1f1-b23711e14df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7808
        },
        {
            "id": "11eb6f78-5372-4568-8866-dc3f09262c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7810
        },
        {
            "id": "189ed29d-90ad-40ff-a1e7-6ead6ef42752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7812
        },
        {
            "id": "d5533cb4-8e26-468a-85ee-dcd702f6bfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7922
        },
        {
            "id": "f4c6e152-c5ee-477e-b271-0bfa42c78944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "c4ddd1da-e469-471e-9793-d40dcee53e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 86
        },
        {
            "id": "c12f575b-943d-4c03-a1d9-e0c7998a6028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "77000fcb-71e9-4e72-8a66-0b68b82c7727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 221
        },
        {
            "id": "bf9d6fbd-a8c3-477c-b5df-ad3bb6f84a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "81f7186f-bb8d-4489-8043-0b9f73de915e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "f25f9fef-3d0b-4bcf-bfa9-a0c9fce8511c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 358
        },
        {
            "id": "525db42a-3d7a-48aa-aea9-30ba9305d6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 374
        },
        {
            "id": "44cc5a34-fbd5-4b73-aa38-e48edc7f0fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 376
        },
        {
            "id": "b1a07cf2-af2d-48c5-a762-db112ef31e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "f5c2aa75-5b18-4c6b-b314-d66f5bf9910f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 932
        },
        {
            "id": "b53587fe-f38e-4fdb-bf8e-5a217f257b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 933
        },
        {
            "id": "d0842f25-a88e-4ec4-9461-f6caf82acabb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 939
        },
        {
            "id": "e55976cd-3352-4f48-8e0f-2f773e9597d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1026
        },
        {
            "id": "24cd1bf9-7a0b-4a89-b751-9e9e3aaa8269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1035
        },
        {
            "id": "78a60f6f-00d8-41bb-8a52-96ffa27ce9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1058
        },
        {
            "id": "bb9c79e2-93f4-429c-93e3-2a84c195e524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1066
        },
        {
            "id": "c0fe3313-cf1f-417c-a225-52bb612cb17e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1184
        },
        {
            "id": "408f4e34-912d-4ed2-b3a5-b65b83f0fa41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1196
        },
        {
            "id": "20bb013c-3b69-47db-988b-1b1bf71932fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1198
        },
        {
            "id": "a38139d7-81bc-4976-9896-0a4f18ff5ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1200
        },
        {
            "id": "672edfac-ae95-4d4d-bd88-c2e9068f786c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1204
        },
        {
            "id": "f82dc440-c445-41be-9d53-b3e3ba4b81be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7786
        },
        {
            "id": "cc36204c-207d-4010-aa89-4a1f93739bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7922
        },
        {
            "id": "4a7ec2bb-3c4c-46fd-8826-f50d45546ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 63
        },
        {
            "id": "ef36b451-0354-4ac1-ab5d-0edb38e24ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "7838d310-d2ba-4714-bc41-347953e4937a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "78f462d6-a9ab-4e2b-aa5d-c6d820e25bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "8547a6a3-c26e-4934-8ba1-3f1355e599dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "86dcf7d7-77c2-4035-a148-49fa30685c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "4b488f41-ace8-4c2a-8f69-a2e159130198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "3b6d5b61-874b-4a27-8433-6fdbd256a637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "0060c83d-8404-43c7-b01e-01ba3ac3741e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 358
        },
        {
            "id": "7a0e7795-e26a-48ae-bee1-f0658098ef04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 372
        },
        {
            "id": "6c0d975e-a360-46aa-b995-1967e60be7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "1a217a3c-8c76-49af-97e8-9ea30042a122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "6268c49c-5d21-445d-84e0-7de05f71aabf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "40d78c11-9f9e-45c4-a83d-c8b550389410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 932
        },
        {
            "id": "d4fb3123-e7bf-4368-9272-3847899f09fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 933
        },
        {
            "id": "174dbaf3-7681-4d16-83be-fe41fe590bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 939
        },
        {
            "id": "42212fd4-e68a-4fe9-b6bd-48f250b9d6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1026
        },
        {
            "id": "19b8160c-f91e-4fb6-9f7b-9aabe35b46f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1035
        },
        {
            "id": "36f35e8a-d78e-4711-abf3-122d8388fcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1038
        },
        {
            "id": "034955cf-e74e-4bac-9ba0-802abdb005e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1058
        },
        {
            "id": "1c3656bf-a440-482b-b4c4-f563be0ebeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1059
        },
        {
            "id": "88d795e3-9cf5-4f4a-802c-79ace052ee76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1063
        },
        {
            "id": "412cdc19-92aa-4e45-b916-e7eb7933dbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1066
        },
        {
            "id": "456a1dd4-bbad-4924-8d68-2ba65a504d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1184
        },
        {
            "id": "e6a98b0e-524b-4b3d-9195-d9552eb97404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1196
        },
        {
            "id": "001e4f84-7dee-4e1d-b2a1-12d8c3387331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1198
        },
        {
            "id": "34c80aa6-6891-4721-83a2-68af8636ba2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1200
        },
        {
            "id": "2042987d-79a7-4de5-b056-5b1e3d3758f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1204
        },
        {
            "id": "c899d5ab-ccd6-463e-8c72-fabd197f76ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1206
        },
        {
            "id": "150d63ce-c3be-4ff0-b18f-e30ffac125b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1208
        },
        {
            "id": "29fcae41-be77-4ad8-a6c9-a89cfc042b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1227
        },
        {
            "id": "c1450fa9-fc58-4f8e-b01f-4d6205dc25ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1262
        },
        {
            "id": "f90646e1-5e1f-4808-9294-a107d8ee0497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1264
        },
        {
            "id": "47551fb8-c1f4-44fd-9524-012d8b2078d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1266
        },
        {
            "id": "b57f82c7-2d15-4028-bec8-ce0dc266d71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1268
        },
        {
            "id": "fae5f82f-2c94-4651-a401-1c93ff194387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7786
        },
        {
            "id": "d093589a-92c3-464a-8317-d38e446f7805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7808
        },
        {
            "id": "a2781f8a-e547-4c7a-81b0-87327b096a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7810
        },
        {
            "id": "36cf310b-fe66-4414-9e81-6de38afe3d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7812
        },
        {
            "id": "10cb150f-6070-4695-b01b-1c74d8999a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "9bee9ed1-3ed1-4747-8701-91f5517f6911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 47
        },
        {
            "id": "7303b524-9d36-4cf0-b38b-d886aa0fe2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 65
        },
        {
            "id": "0412e806-04b0-4adb-89c5-2a73f97e424b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "b44d6144-66c5-4657-aab6-cc915645761b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 192
        },
        {
            "id": "cc3eaaa7-08e2-4002-bae7-804100f7ffa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 193
        },
        {
            "id": "48062619-5a79-4be8-95a3-438102a84fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 194
        },
        {
            "id": "ceeb2708-1c0a-4c7c-bcb8-b425dce6a067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 195
        },
        {
            "id": "73b3b641-ce97-4402-bf97-af084144958f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 196
        },
        {
            "id": "c53b9b48-df9b-4763-b8f7-66bcfaf36b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 197
        },
        {
            "id": "6aed6891-2ac6-432b-bf7f-32be6a57fb45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 198
        },
        {
            "id": "188e4ad5-beba-4176-95c2-57adad5934d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 256
        },
        {
            "id": "292e6d44-09d7-48c7-aa8b-fffa43ea7068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 258
        },
        {
            "id": "d9d20c0f-2f10-4dea-9c96-8bcad027b4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 260
        },
        {
            "id": "b7b6c465-c9c5-4b24-9813-70967b655641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 308
        },
        {
            "id": "0dceb53b-f429-44dc-bc3c-c99f6ceb5b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 506
        },
        {
            "id": "089085f3-015c-47e3-971d-6eceeb39fe23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 508
        },
        {
            "id": "fbb63b6c-7289-42c6-88e0-26b927f28c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 913
        },
        {
            "id": "52fa7d69-c6a2-49da-a7de-394c78767369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 916
        },
        {
            "id": "568eca06-f25a-46a9-b030-62c257dc4367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 923
        },
        {
            "id": "8049899e-36cf-4693-9339-02139f805d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1032
        },
        {
            "id": "38930e57-d49f-4b05-ad59-b5c84c5410b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1033
        },
        {
            "id": "5f26de8d-69b1-4f78-9d0e-eb1033c35316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1040
        },
        {
            "id": "d94098e7-6702-4ab0-84ae-54ac870eab06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1044
        },
        {
            "id": "4082c3a9-baeb-406f-9f13-c57d5213a1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1051
        },
        {
            "id": "4e4e5228-c94d-46e1-95f9-e6fbf50df791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1076
        },
        {
            "id": "406dafe3-ede3-41d2-a4a7-11eaf6ff9c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1083
        },
        {
            "id": "1a39b115-618d-46cd-876a-966a3eb6b70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1113
        },
        {
            "id": "c064a3c8-daa1-4eb1-8c4a-8a93d508c3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1232
        },
        {
            "id": "ca0fbf0d-9fea-4218-b3be-ff25e6c53ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1234
        },
        {
            "id": "e4f98276-028b-433f-9684-21626c8ed7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1236
        },
        {
            "id": "21712f44-2789-4d13-b467-fe969fff84c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "2c10deb8-cba3-41d1-bd88-aa0450cbec9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 86
        },
        {
            "id": "6014e029-9d03-4ec1-86c8-3ce187e62b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "1819e1b9-e3fe-45e0-9efb-d340a03cefb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 221
        },
        {
            "id": "bd7cd043-b2e7-4e63-b2b5-2e30c0888481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 354
        },
        {
            "id": "1bab3cf5-a94f-4303-8577-bcd4cf3b6eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 356
        },
        {
            "id": "021e40f4-affd-48ba-b33f-a17e4760caf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 358
        },
        {
            "id": "87bbb5dd-6fcd-4470-ae7e-9e01c7d8e41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 374
        },
        {
            "id": "0ea8408f-91da-45be-a370-6b8bfc0680f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 376
        },
        {
            "id": "54bcad2c-ac80-4433-a1e5-1dde4540e026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 538
        },
        {
            "id": "83bf1fe5-9531-4c10-9bd9-7d399ceeea47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 932
        },
        {
            "id": "a4d27ee5-0060-4e1e-b17c-3d02ec7b18e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 933
        },
        {
            "id": "0b2a79b8-0cdc-4430-add2-af9959b8da2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 939
        },
        {
            "id": "f5600c06-1d20-48e4-b372-0553ddd2d710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1026
        },
        {
            "id": "04787f70-b1c5-4f4b-b4d0-429b11d00dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1035
        },
        {
            "id": "1c5ab8be-2856-4fac-b909-be8fb9f1d8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1058
        },
        {
            "id": "e4f4ec6f-e13d-4907-be0c-73231ffa33c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1066
        },
        {
            "id": "ac3fe4a1-5e72-4e17-9167-dfabc2e9f8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1184
        },
        {
            "id": "eb3d40e0-bcb3-43a9-801c-0f8a97bc0f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1196
        },
        {
            "id": "d75f6b99-afde-466f-b90c-ae6c136cd29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1198
        },
        {
            "id": "2fc8bfd0-d607-4db9-b488-b83bf65fcd95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1200
        },
        {
            "id": "0a10cba3-630c-4796-9a84-4d654991d886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 1204
        },
        {
            "id": "a7eb3ac6-1285-423f-804e-31cacbe14868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7786
        },
        {
            "id": "de0a720b-6e1b-4ccb-857d-8f9ef90b5680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 7922
        },
        {
            "id": "acee69b9-f57f-43d7-8604-4ff0b117327e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "dae22dc5-1008-4f3f-ac0b-3f911ad3bff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 86
        },
        {
            "id": "8cb828cd-600a-4950-9374-f4e8ae80f126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "0874a078-068a-4752-962a-7c7a98e77d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 221
        },
        {
            "id": "5cf6007b-e6ce-4f77-8cbc-3c5b349300c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 354
        },
        {
            "id": "0416522f-8302-4948-b93e-8a03a15b52ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 356
        },
        {
            "id": "7213f11f-2712-405f-a29f-8f630bfb8880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 358
        },
        {
            "id": "bd022f2a-b54d-459e-a24d-edc62a1b6e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 374
        },
        {
            "id": "b3ddc277-50af-41ec-aba0-b09a3b8cd5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 376
        },
        {
            "id": "1c88ea0c-8675-4385-99eb-2187a16dd4bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 538
        },
        {
            "id": "fc43f0f5-b037-4865-be5e-2ab28725cb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 932
        },
        {
            "id": "7bb39f0a-ef39-47cc-bb0b-67868a8b6482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 933
        },
        {
            "id": "bf0520aa-c0b6-447c-9da3-ff2b0c8dd656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 939
        },
        {
            "id": "8f5a7f13-e124-40de-9b3e-6a5df1eca21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1026
        },
        {
            "id": "35f73334-a269-40e2-81b3-11892db4f984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1035
        },
        {
            "id": "72e1898a-1c9e-4022-b3a4-bfb854ac5e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1058
        },
        {
            "id": "8de5665f-1ccd-457f-9b71-c02f481a2431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1066
        },
        {
            "id": "d95cdad8-e356-4dd8-81fc-0a9e781d55e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1184
        },
        {
            "id": "047bd2f7-701b-45e7-abd6-dedc683d39f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1196
        },
        {
            "id": "5307dca4-f51f-45dc-96b4-e448374e9777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1198
        },
        {
            "id": "d131f9a5-6cf1-442a-9715-73e4dd91dd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1200
        },
        {
            "id": "de98c906-4f73-4ddb-ad40-12e76cafbdcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 1204
        },
        {
            "id": "72b4debf-6119-4d7d-88cb-ec6e7fd4a9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7786
        },
        {
            "id": "a1ba81ff-4df5-4a2c-a18a-175d8644ba72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7922
        },
        {
            "id": "02fd6221-fc2e-4519-9fd5-69c892ae0f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "fe744fed-e62b-4756-92ba-ee60dad9a1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "7befe09e-3eed-483e-a3e4-da515adb5f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 41
        },
        {
            "id": "3f1a26d8-866e-4ad1-a561-a327fee26462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 42
        },
        {
            "id": "227892d3-1c76-4b48-a1ed-2432cb1e10f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "bf46679d-ce74-4e95-9c14-8424589b094f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "18588186-ab64-4b4b-ae82-1626821a4542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "d54aa306-1203-4088-b9ca-f15565d45063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "fec1db26-3170-4282-90fc-5f38789bdd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "ebe9a42b-5baf-453e-9bc3-1f06efb8206b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "bf6d2cb0-90fd-4a80-afe5-f5d6ae298d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 93
        },
        {
            "id": "91223f54-509f-4930-8e41-1bc09d6ed97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 94
        },
        {
            "id": "352a8343-3a83-422a-a712-19b48a43696c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "cee935ce-2c14-440a-9095-23b96e4ee23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "ab26fbf2-1e65-4e39-8607-dd453d62e41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "66417eae-b2b4-484a-bd1b-434e79148d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 125
        },
        {
            "id": "f6adaa2d-a077-46e3-b113-28f53289ba07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 176
        },
        {
            "id": "8c0e2d29-b71f-4685-a068-3b9b94b95b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "5afccd5b-6862-4c08-bc4b-c6852d44fc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "f1e682c9-8788-4268-8e87-8a47c4bc97db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "d77dad95-8a53-40c6-89e1-6dd627a83731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 354
        },
        {
            "id": "5202492a-2cfd-4c2a-b8de-e5e907f42e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "c0993c50-ce7c-418e-8249-735b1fa1139c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 358
        },
        {
            "id": "31ebbbe5-79b1-42c1-a51f-c13daba339d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "af69ff8b-2a90-4bec-923c-e0d1b412cb7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 373
        },
        {
            "id": "07b71fe3-77c9-4618-a6b4-bed30917f877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "d1d19ba7-cba7-43d7-b4ef-ab12fa9d376a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "17fa304d-f2af-474c-b72a-8793c0179693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "b61ec1e0-41ba-4178-b7ad-29b9817d1fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 538
        },
        {
            "id": "340fb5c0-2462-468f-a0f2-f2f596473c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7786
        },
        {
            "id": "5d342b20-da9d-4de9-9a13-87a707ec9943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "ba024da4-7c13-43a4-b1f4-f7e63be2f9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7809
        },
        {
            "id": "96885bb4-f4b2-4909-84b1-74d7df6b55ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "ebd6e8b5-3cee-422b-8a61-5bfde830ae19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7811
        },
        {
            "id": "59768a4f-f26e-4763-b096-540e26f6ff33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "98be13f2-3127-4a99-a531-6705b7f6f5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7813
        },
        {
            "id": "43e82314-e951-471e-b93e-798abb87a315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "a4c7e080-aaaf-4bcf-99da-20c51f735ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "406b766a-de08-4c61-a8d1-83ee3e5f359e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "c43c5484-9c17-4675-92ea-5fa09ea8a70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f0fdab55-f7e4-4730-93ab-7629dd824957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "a4e4176c-5134-4901-88b3-4e89a6444022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "23f50639-caf1-48e8-8fa4-f73618b6c13c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 10097
        },
        {
            "id": "747be5bd-7215-428b-846e-07b6079c55e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 10099
        },
        {
            "id": "e299b93a-c5a7-4a9a-a3cd-47622861ca29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "f5c16652-5e98-445c-8bc6-3b837bdc9180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "0cb13ccc-6f47-4860-bdab-0627863f5dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "8de1c99a-d77f-4339-a232-eb98fdeede7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "04904e35-a27d-4eb6-8029-0295e49b6c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "e8c1105c-6bdc-4b11-92a1-1658c10b042b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "36ebb801-8e53-45b1-bf84-4354632affb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c3804de5-3c82-4395-9036-870d551ad205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "aae3e1b3-761d-482d-8992-d659c26cb3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "9c4b01b1-c042-4b6f-a446-d581adcc3a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "a1cfbb48-2da5-4cf6-b48b-67cf28fdb314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "766118f1-e7fb-42aa-9643-174c8515e2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "15669809-42c8-42e6-881d-09df54972b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "ab77c839-e685-4ba7-9688-cdb3700d62b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "c2b8bb6a-defc-4cb9-b28f-39134795c57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "61fac5ec-599d-4a1a-b2f3-9564a9f71417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "9453c8d6-9e90-4e80-b7b5-d1797f9b1ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "5244fd1c-1023-4c21-9232-fa433fe0f9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "5d0c8529-664d-470e-b813-6a90f8b0a332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "0a4fdaa0-c806-4c67-8167-ff8226d6b76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "b00988ab-27e4-41cd-bee9-57575f638bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "5f00ccca-ac79-49cc-966a-cc5544c2041d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "e1d4bbee-d0c7-48ac-95ff-97519fa303c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 301
        },
        {
            "id": "4861f91f-21b5-47d1-b282-c23b77336a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "95dfe988-dcc4-4cbf-a057-0e2789ca5479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "75b0671f-2127-46b1-a856-62f38466dfe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "e9492734-c373-4c79-9b26-8f4b2beb04a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "207cdfb8-b5d7-4ebe-8b00-672735644fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "97c3aa60-9478-49f9-9729-2cd6342d9b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "2184a31b-b1b2-4b27-9e7a-5cc4356055d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 221
        },
        {
            "id": "bd24754e-f508-4a31-bdb9-a05ad0fecc79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 374
        },
        {
            "id": "3f38abd8-8285-46f9-bdda-624ae980d5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 376
        },
        {
            "id": "985b2cb0-619f-4036-beba-0631f706ccf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7922
        },
        {
            "id": "cb6f5482-1629-489e-b54a-004812d40a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "ff5c9900-410e-4ae3-af6d-89c9a13cc572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "45aa3802-f733-4f0f-b25b-a19618db1160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "3f5368eb-12ca-4e8b-8ffc-48b157a04164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "c22602de-07fc-4e72-b335-57de1a542eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 322
        },
        {
            "id": "60705d82-a9eb-4e4b-ba98-bf96410fc3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "cb20703e-ebe2-439e-ab48-6baf70cd11eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "902c9195-05a9-45ce-9c2d-97d6be108bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "1f633a71-1050-4e66-906c-96e5fc0f14d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "7f5bdbaa-aa47-49fb-9d4e-796b5fcec276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 42
        },
        {
            "id": "e0dbdb71-d609-4f83-aecd-98cb468925a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "f74504b7-2250-41ab-b2a5-e6d56b9d465d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "772318ef-01ae-4ad8-bec4-665a1c936eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "2fcb8171-da6c-4f88-8410-d95b69fcd9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "0598c86b-0e94-4847-bad5-db9630eb65f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "3d4f02ce-4245-4666-a334-fb89c94067bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 92
        },
        {
            "id": "0eaf4918-cbf2-44db-a7b1-31c6936e46a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 94
        },
        {
            "id": "1bf1b5a2-e160-42ef-aaa7-1b1bb03189d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "afdea999-5a7b-4922-b9ce-7b916cc1ee9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "d8e055bf-8976-439c-8e02-61dd123a8d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 176
        },
        {
            "id": "bf4f5e35-c0ef-4eb0-8f9b-6673be2cefe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "07b40b3c-f617-46fa-ba78-51e17a516108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "c172dcc4-3b6a-4bb2-98b4-1f3a335f4e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "e3198958-d271-4480-a734-aef955189980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 322
        },
        {
            "id": "de7539e1-dfe1-403d-a925-7fc73903664b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 354
        },
        {
            "id": "ebca3b15-3e42-4261-aa7a-8d562509d85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "25e54e3c-91b4-49bf-86a2-589a2798580d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 358
        },
        {
            "id": "0695ea22-fa2a-4430-b9ee-3715f2846b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "def8d9e0-5f40-4efa-8a37-8f14d25e02f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "2faa90c5-e57f-413c-b8a8-3610ba5ce5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "4f0bc6b7-b9d3-4681-873b-57fa838454fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "fb99e7ed-29d8-4454-81f9-733032396285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "0980cbc6-4e43-4c8f-9b1f-85e8902154a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7786
        },
        {
            "id": "f725bb23-a9d7-4083-ab06-5333723c878f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "3d0be0f7-9dbf-4b24-b701-ce8cd916f083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "2398904b-3c3c-4073-a86a-03a4b6a97fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "6aa94407-1ee5-4377-bacc-844a4ce680a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "5666e6a2-53a9-4a9a-b384-9fc076211567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "6280d4c6-b2a6-46df-9559-ff73f310f818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "6f88190c-e221-43e4-87e0-53aa1a7c6379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "b3f7d552-6b69-4922-81ee-704930988b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "55818a5a-08f3-410d-9224-24c90951888f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "8ec487ad-3391-4148-8e14-703c75b28287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "363aa7f5-61af-4ea1-b98b-510a821ea157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "97a478d5-b9b4-446f-a922-8873bac0d274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "ca2939f4-9f13-41df-86d4-4442255df1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "207b33a9-5d3c-417f-9c5d-301d0fbc3062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "2806979a-8c79-40ef-821f-7a554089dcd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "6b59e02a-1095-4ea2-8ea8-88b2cfdc26b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "aab5b1f2-272f-49ed-bd9c-77036fe45a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "6d0402af-ea5b-4baa-bd37-29b30d2f3d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "0d5cbd2b-d3cc-4508-98e8-de51d586b4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "51df9c37-5189-4c93-a987-cd482046d764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "9d555483-b43d-474b-9053-d6247c8fd626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "c3e822f0-7c6e-44ca-8a17-7616e9453572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "c7ae2bb8-e2da-4049-9f9a-2481545d3fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "3c9765da-56f6-4cf4-94ca-034d3cf2f5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "5b4fc2f4-eb0c-4891-8e49-98d3991ecd05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 508
        },
        {
            "id": "b7570cab-51f2-4094-8e85-1711eabbdedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8218
        },
        {
            "id": "5189fc84-bd4f-457b-8d42-02ff19ead3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8222
        },
        {
            "id": "1bf47b49-c5d4-4e4c-aa62-125b72e1be5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8230
        },
        {
            "id": "1a505dec-7c1e-4c76-8a99-984906a339f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "86eafd13-9d62-4a2e-bd5d-87916ba42fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 221
        },
        {
            "id": "5d9e5fe3-a5e7-49df-8d48-38eceb9c808d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 374
        },
        {
            "id": "5e945ec3-ee0c-48f8-ba35-e0ee4d3d8abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 376
        },
        {
            "id": "d683f591-cf5b-40de-926d-258c7c6de324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7922
        },
        {
            "id": "c33bba8e-e422-468c-b758-4d86db36865d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ac157fb4-1bc6-4f43-aef2-60f9961ee1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "8881f763-6fe4-448d-afb1-c58bde203ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "08655645-e790-4908-a25e-14b7e27b5a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 47
        },
        {
            "id": "86d3d089-91c7-4626-908d-ae0184d4834c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "97d01007-0500-4323-a285-db441540f667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "0dd7518c-c576-4c74-9798-20a17b914ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "440a93f2-edb9-4b8e-8e7b-3070c9a33956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "cd258826-966a-4d29-b9d0-830e1dd27b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "fb236d87-cddd-43c6-b742-bc0d0dffd057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "a0362aee-f92a-4c02-ad0c-9f8cffa151b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "ea197453-fb7a-4221-9d5d-5f9ff6146ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "0ba6e74a-03e0-4fbb-a6e3-e4f9340e7c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "a4e83bb0-bf53-4b93-bfb8-4aa6d351fee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "80a7e2a5-1a17-4690-8a3b-0a6049ffb517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "af7f4c65-4c5b-4036-aa4f-eb2abec47b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "395c8318-86ea-490e-a640-8278ff4de21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "2574fd9e-d14b-4747-b14d-17c5cb482473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "88ea19d9-28d8-4efd-824c-61f94331fab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "ef199614-79c9-40e5-a814-d9ab92f5b00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "7dfa1704-9e36-4302-8830-9c34e5f4ece8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b68b29a5-0e62-4bd3-857b-71dbfe0d7dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "d21e8da4-eb5c-42b7-a01b-86bc0f423ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "517e2dda-c3be-4bcc-b9f3-14a873fe11e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "d633da11-206f-47bc-a95f-be92981e2dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "ba214ac0-f490-403a-acce-dd47e383fce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "3821002b-eea6-43ce-96c4-6c29d424baf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "e59da4c6-2b08-44ba-af23-8ab9fc6d8f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 192
        },
        {
            "id": "a27522f8-1a5d-4a09-8585-510d9b3e88fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "ed624a54-f7f0-47a9-a6e6-b5628df9798f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "074285ca-aa95-416a-b15c-c9becc49ccbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "6381c5cc-b675-4459-94fc-36ab727e1e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "a052d9ca-c4cf-45c4-b48c-e37c104ba6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "ed676584-7760-4794-8d85-2ec677771c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 198
        },
        {
            "id": "fcfc70e8-76e7-4f05-96d1-3ff50f656ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "595a874b-d549-4964-a895-dd1506729fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "7ee4a396-b93b-4114-a9ca-6113209adc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "4eb133ea-2a00-41f4-8d83-ab9f1abd3ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "9645790b-7b19-44f8-b9ea-01831bb11340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "8919d290-12d1-4e19-8a6c-a0b0a3e3f7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "fb96f8c0-0768-4604-bb78-55505b4d0fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "61e35be8-23df-4192-8f75-6bd48214c7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "c254e17e-0057-4021-9b5e-c0098c6f77b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "bb10ee2d-aead-493b-8b61-bb44296af3aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "796d9dc6-efd9-457d-ab40-7f55fd9af8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "7f2e8482-2f61-4ae7-8f94-d5b411b64fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "a8dcb205-b387-492a-b995-6c37634e42b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "36821a4b-6cfc-486f-9d1c-0096cd336605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 240
        },
        {
            "id": "c6de7d8b-f08c-4133-8aa4-f0cb72a2fdc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "8a6a3adc-546e-4236-8c50-1ba161da5ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "b90ebc8a-e091-4737-9a20-6be480783c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "7c6602b8-1de1-4941-ada9-7814d8c0bce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "459b5d9a-1eae-4de5-8cec-5a6fa397d268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "164be85e-6246-47a6-8d0a-abe12ef487d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "4068ea9e-817a-4e3d-a73a-6cebefcf2c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "b861c32d-6b42-4704-ac5c-6158826787f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "642f2edf-ba1b-4bca-aeb1-1d2eb7bd70ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "9020d98f-66ab-42e6-8f04-f3a8dc975a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "3f08e940-f182-4d8c-9bb8-b26d516021dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "8dfc9932-0b2a-41e3-9b1f-2f35ac7744b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "31310ec6-37bf-45da-b504-26b0db157306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "e5c8bde6-aa30-417b-8ec8-07e7110068c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 256
        },
        {
            "id": "69031e89-a952-409e-ba1e-de302b1b186b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "d5cd3370-d40c-419b-a89e-f6fad1488e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 258
        },
        {
            "id": "9e3c25aa-1058-4dcc-99e5-505e49aad0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "0cfcd660-630d-43d9-af1b-0ab3753b0125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 260
        },
        {
            "id": "3a659944-a7f8-478a-a7be-f864b017632a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "1ad166c3-b36b-4979-980c-f33fd1ecbb5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "a948015c-7bb0-4f72-9ab2-ebb9c8d70bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "1f9e24e3-1d30-4584-9287-3a865583a2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "e1f94d57-02b3-42f8-844a-28e6e00d8839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "ee54753c-92b1-4364-bfd6-7b88879e27b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 271
        },
        {
            "id": "4f708a1f-4ae9-4e1e-8995-6cac8958099b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 273
        },
        {
            "id": "d2db49ee-6ee4-49bb-b8bb-272e24a1917f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "9d8561a4-5450-4d09-ac4f-91b766b46143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "1792689f-b7d3-47eb-bdc4-28be3be60d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "50f41056-ec88-4a30-bda5-9f6fe191ca92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "418d19b1-32a6-4970-8695-be7000775b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "bddfd3bd-aba8-4210-8d9b-12fea2971aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "8d5fdb47-180d-42a6-b1ff-b9b97b3857de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "019617a0-a06e-4576-9c8e-be576b780cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "348beda5-121d-4edc-a3e1-61d1afbfa315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "6e23d991-df97-4a62-8e23-66318893c979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "e71a890f-ed1e-4381-8e2c-8011063dbae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "055356b0-05a2-4a58-94ab-2b9d5edb2609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "df419ac7-b006-4284-99f1-e54975775621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "e0c5b936-a7e3-455d-8994-df123a05b176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "6686ada7-1995-4a29-b2a2-6f6caa042cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 322
        },
        {
            "id": "0735c59a-9c6e-41ab-af2d-2bc20be9d103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "46b578ae-d411-4c77-9e1a-3c7dc790fd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "7c582d08-1b9a-45b3-8d9a-6c838636c38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "f79456d9-d466-45dd-9e24-500f36aaeae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "2eff1f00-a155-40e0-bffc-c32b135dc82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "fb889d9e-d655-49c6-be0b-8ab681a175bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "c186f398-989e-4868-9dcf-6be9355fa277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "a8ae5eea-22b4-4555-af1a-504d295359b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "93b129a3-6dc6-4d73-997e-c81c5b163a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "44f5dfc7-22f8-4be7-a63b-e0bed7aee554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "1c24a2e4-92e2-494a-9b7a-960b38c63433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "a45e409b-d2a6-4140-ad6a-67edae315917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 349
        },
        {
            "id": "6cb40a3d-0a9a-474c-a57d-c4723c38ac9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "866c2fd9-4ace-4b3b-b69e-7600adf5f407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 353
        },
        {
            "id": "eeefe647-5c9e-4977-bb20-9736744a0afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "47f95229-4ec3-4d2c-a5a2-c4fda1113384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "2bbd9174-8539-47e7-b813-a2c8501148c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "bf65753f-3481-4900-b8c0-f7f37b3ecbf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "a91ffcfc-783c-41e5-8168-60120f5e16a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "5dc92379-28c0-4b50-9664-9e3f9bcc11c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "6fe7d71c-b903-4421-ac87-974937b51c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "a4650a2b-23e0-49a9-98fa-c259f6de666d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "c1e62ce0-9a10-401f-9e07-591568706579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "22ae20d8-d93c-4e44-ad2a-3618b59360b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "4b08e4db-574e-4cc5-a2ca-91c4657a8d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "2263dc4d-50b6-4cd4-a1ed-81825bd9aa3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 506
        },
        {
            "id": "c18e9974-b8ce-49ab-909d-a63e719b505b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "b04f752c-7434-4998-a1b7-56338d74765c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 508
        },
        {
            "id": "66e92d21-7327-4f6a-9f2a-55df594e4e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "08a7f6c1-e8d1-47e8-a180-a27b83550c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "df43bdc3-de4e-4307-8642-fef784784a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "7d49f5de-7c05-462d-98fe-08676447a932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7691
        },
        {
            "id": "32fed7a5-96de-4b75-ae3e-fcccbf1074da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7745
        },
        {
            "id": "a3ef836a-2d10-4010-ad9e-7ccd87ff03d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7767
        },
        {
            "id": "fc70335e-a765-420a-acde-71b6e93fc21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7777
        },
        {
            "id": "68a4fadb-60e0-4423-bdf7-12aeb1705d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "54ac8d04-2443-4dec-b15f-f12eadbf1b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "fbc3eb94-d7f8-4b70-9acc-4dcf690f4a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "1c7e8ab1-b1a5-41b5-9853-88f672551477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "9bda8398-ff7d-4462-9b84-c4782250ab78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "e39efe88-ce62-4b49-badd-5b87c75a3d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "524f6512-a8bf-4dfa-8d85-eb2046795715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "a589a7cb-f6c0-4cb3-8e10-e376785c0f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "3a500616-ff7a-4f99-9d3e-58d027b64f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "8696929f-ce94-4487-bf4a-1cc0e6eda2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "289dd603-cb88-43de-b475-3c75b4550a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "4e3a4a7c-917b-42fe-84f5-d42327c44974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "549bd7df-f06b-4f66-8943-8baf76e23bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "dd88dcad-97ac-4f3e-9595-dbc776d64645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "5ef90ba9-1f13-4ac4-887c-1e6d9354361a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "45cad783-2e7b-408f-8a88-a093b461c191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "66fe9684-7a5f-4e40-b8e9-0ace85e5fb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "2478cb01-7436-4c3d-ac32-2d2a1d36a45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "160b0e6a-0e50-4300-a35d-12579054416b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "1a8281a2-3acf-4a3f-b6b5-2ce722a0bb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "45072c3a-0965-4abb-b626-b82346904ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "def6f54c-bd11-43b1-991e-af068ab4a389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "a2e3e383-921d-4818-a2c4-ec0ba25549de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "5129d10c-2362-4029-8ede-1a7d3d7dc0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "46361d9d-4dcb-4d7e-a50d-516fc7ffd7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "fba610d2-5f76-4af5-b659-575c4dd80702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "183569ff-56a4-4911-8cf2-e3baf4ef79fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "1e679d5b-1228-4e23-929f-1f8e2f2bdb54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "ad22e997-5ea6-4882-9919-205a92af93ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "17500898-8c5c-4440-9800-b44d6728999b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7c98f7b6-e23d-4a19-8541-38662bc80b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "15e2a81b-4246-4d92-b0ff-d5bf4cff9e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "2397a5a6-5ef8-4104-959d-f1a681a89f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 118
        },
        {
            "id": "5606fcc6-3a92-49ed-b6f4-13389d16672a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "6b25cd18-f680-4367-a1a1-d3f9e6ae3aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "38e54602-49c9-4fcc-91d7-3181b2435c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "6433f6d1-5ca8-47d0-9f4e-1e12c0796baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "c6e6097c-1664-4d20-ae1c-bb23cd141d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 187
        },
        {
            "id": "7781d622-ddd7-4634-97f1-3247ffacbf53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "d9b4d04c-4900-44fc-8c55-166102727874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "cbb5cc46-0846-486b-a55b-93382543a0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "5073e6d5-9ad1-44a0-a296-4c88aea02211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "0119db38-707f-445e-92c5-45234f6b2fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "2138ecb1-dab1-4891-91b1-961053140bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "f98eeef0-8692-443a-952e-96825603ebe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 198
        },
        {
            "id": "88fbc764-b46a-43f4-a81d-855319f67f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "134b7d57-cb9b-46f3-b9a5-2cf202468c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "66aca1d5-cdb9-425d-bc1e-31858a8cc8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "ce189202-3b65-440b-9fb1-9e01d4c3b3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "cee376aa-725a-4851-9b7f-1564c56c6c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "9b0f12ca-7921-4c04-96c0-19e70b685ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "76c50b23-33f9-420e-9820-45231d38c641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "fb149536-5451-4bd1-b9f8-541d9b25d5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "3cf0eb7c-7973-444a-9a56-d7ec8003e558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "784f129a-aafa-48a4-9c31-6eeab300f5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "f2cb28f0-0d5c-49b7-bc4f-40b0114bd9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "d951cd29-4b7a-494c-be44-4d47b9b2567f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "d1dfd585-55f4-444e-952f-01dfb4685d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 239
        },
        {
            "id": "ec6c6d2b-2e45-4445-876d-63858585c335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 240
        },
        {
            "id": "572cfbcf-0d25-4722-8a32-252f1b9f5011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "990593e6-01e0-4708-a3cf-71715d5d135f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "2a1e69f0-14fc-4efe-9414-1a9fc8fcf86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "f447f6e6-c7ad-43eb-ad57-55ca7eee89a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "5e4b345a-7a3a-408a-bac6-d633461b7159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "bce0a47f-727b-4aab-9beb-610fd8a34108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "ca8088a2-c032-4695-b478-85989e614934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "58e254af-92ed-4ca5-90b7-f9c2a96907aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "1ccbb3ec-f5b0-47b0-810e-37d041354cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "c8b68116-0877-4cb1-9499-61fbc4650b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "7b30c32c-595b-4d71-a464-f94dabed061f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "8161ceaa-3643-4993-8a8b-ca1ba6ade20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 253
        },
        {
            "id": "f162a0ad-d152-40c9-ba26-6a335a94b9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 255
        },
        {
            "id": "1813a36e-bb63-4258-90d2-0daa36a6d333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "769c1f82-9078-4ec3-b29d-af08c10cd25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "05e3ca05-aaa2-4caf-abb2-3dd8ca97f83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "c7caf311-b9de-4867-816b-1b35c657cc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "6d04ccd9-484d-4fc6-81c8-6c5f6b4b53b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "48050871-55ca-4cd5-a808-7bc08b6bdae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "b62a4cd8-7140-405c-a3f6-8533291103a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "61437984-bd4a-4d2b-bce6-e059360cfc4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "1df1a6c5-840d-425d-8691-d0c101a078e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "b75dab13-3d7c-46dd-b840-5617ea928b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "c29fba12-423e-425b-999d-eba5496a7cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "0e5c6c98-1b66-485f-b6d1-3f14650799be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "38defa7a-b82c-486e-bfec-6fdb4639a559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "a927b577-d64b-4dbe-8e80-47a438a0f5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "55bbcf98-e9fd-4b11-a4eb-71d913f5e8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "75806c60-d22d-455d-bed1-3e9a84f0226f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "45753e7c-c807-48a5-b5c6-ee5ba92e1134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "880af017-b47b-4ee5-a6a4-4c614c935a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "76a1af78-b944-4719-b4c1-59d99b0d8036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "230fbf73-a917-4d80-bc66-7935038090ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "aabef8db-39ca-4c25-9f33-d2b78d70122a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "95e288d2-7bfc-45d0-9954-6432f01788b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "4482d41b-c26a-418b-bb2c-c043e83cac4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "98697656-12a3-4204-872b-28536a721580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 305
        },
        {
            "id": "d024f45e-60d9-48e7-b91f-298df03cf1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "14e02f8b-fc3c-4da1-a75a-522b27d3c616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 312
        },
        {
            "id": "95b12a17-cff3-4714-a858-dd4d0b7c7c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 322
        },
        {
            "id": "5a7c9eed-c092-4a60-be34-bd8030c81e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "611bdef2-9ef0-4024-8891-7af2694ade48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "f5eb5f31-ce4b-4921-b20e-beff0575a710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "578190ec-ea7b-42ca-8ecd-d0d8131bee11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "69668c39-74ad-455a-a3a3-ea03de752434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "c9ffa2f3-50fd-4d6c-8a9c-3a97ebafeca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "67dc9df7-f4e0-443e-8c96-f30f19a8f09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "ed2b94cc-3ad3-4f0b-90cb-ff04da924b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "042de066-a7eb-40f7-9eba-7bc2ffbda561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "f5a78eef-bf3a-4f2b-bd53-1e4283d883dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "b14eb639-4521-4ccc-8202-5d3b872f0d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "1aa8f143-cd11-47db-abe8-f570fc5eaeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "7c3c34d9-a27f-4f57-acb1-1792599dee7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "8a012352-8dd6-4197-b75b-cd5269a77173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "28ad2fc9-5492-4772-8380-e3e5f5fde75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "10df4ac2-976e-4ed4-907c-c1632286103a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "f9270f7a-0229-4bb3-93ec-17230e960552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "c3806758-457e-4bd6-8646-29ed25bedf2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "aab63ed8-dc96-4e3a-8a89-648523d5f3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "ec2f74a6-ee06-4e66-b367-9142849bb11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "2585c7fd-df24-4189-a808-873d9da5f47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 373
        },
        {
            "id": "0ef86228-bf25-4b37-ab4a-81dc531c7f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 375
        },
        {
            "id": "a1358bf4-129e-4458-9e0e-a181eea5fdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 378
        },
        {
            "id": "f5092b2d-b100-4a61-a0ab-cb457263197d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 380
        },
        {
            "id": "a6c9a00a-ad18-4a47-beb6-d9e4c99a2af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 382
        },
        {
            "id": "5e6e482e-e51d-403d-ab6f-4a8cf892787c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 506
        },
        {
            "id": "972fb07d-f08e-41bb-9070-db1a8179a9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "d0cf3096-ef01-45db-a387-d64514eefdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 508
        },
        {
            "id": "7199b73d-a502-4241-b8a0-d7cc2ad987ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "a5f636dd-90fd-42ac-93d8-68c8444939ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "761c2c12-47b1-439b-9583-7c03415170e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "5781fc1a-351b-46ca-8d39-7f55800457b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7691
        },
        {
            "id": "bee3dcbc-4d4a-4c14-afd9-e65b78a17771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7745
        },
        {
            "id": "275493e0-71a9-4af8-8495-20a9eb8630af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7767
        },
        {
            "id": "58347920-f0fa-43fd-b986-993b3f5f168f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7777
        },
        {
            "id": "929e1aa2-3f9a-49cd-9ff5-f415fd7618eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7809
        },
        {
            "id": "f45e99b0-c263-41f2-97c0-56822fefc5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7811
        },
        {
            "id": "3703cf03-378a-4772-89a7-99ca81734b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7813
        },
        {
            "id": "e54b8c20-940f-4483-b1d6-c98fd1dd9f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7923
        },
        {
            "id": "c6c90396-7135-400b-9e4c-b1361e64030b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "e5c272a6-ba0d-4158-b3d6-368aefc531bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "f6401e22-0c84-4f8b-bc1f-bf1edf8a20d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "9f94ebbf-46d8-47a3-98e7-0a1f3525fcc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "7e14f984-3a99-497e-a5b9-283056ccb78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "417b3b4e-719e-4ef6-9c85-01c868f3ed7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "c31b2489-aed8-4b24-ae63-6fbcb90e535b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8250
        },
        {
            "id": "701de3dd-d626-40f0-bb00-7e70aefb41f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "d72bf22e-e299-4be8-bc2e-ef2941c20491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "18ca24d7-4534-4dd5-8a52-1117ccd39060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "44dffba7-52b8-4d80-94e7-e9a56fdeb3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "ec5f87ac-ee3a-4d36-baa5-0cbfcee1b56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "f66da6f2-2814-4768-b891-df1b20e4d247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "ff11766c-b66a-4aa8-a995-5e266c0a0f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "216b0a01-2c99-422f-99d8-8d9bfab501a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "a38030e2-2d97-408e-80bc-70911d840777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "0adbd9d1-2c8f-4417-9882-b127d333bf5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "3678f193-bd75-42f4-8a92-4ee443ee3afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "07dce2c1-675c-4bea-b844-f3d461073fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "67d493b7-68c0-4d8e-8993-4b62e72e9513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "3fd188e1-717f-4ce9-af26-8b0a2b223887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "b58987b1-655d-4fbd-a7a9-4975fc52fd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "36ae5d86-d19b-4b16-868b-c621e978669c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "68d3566a-f247-4529-9118-903ae295b4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "443e000e-c9cc-4335-ae71-011382779f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "fb8ff0a5-2f4f-4c6e-b485-bb6d7fd7da13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "488f58fa-db31-4310-a952-383c1408701f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "70b9b5a2-8207-4320-bc18-8a91f0e957d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "a9d5c5c1-f2da-469d-b7b4-ec946a476d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "beec41c0-ce3d-4397-ab68-69e324dfd7f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "6e3083d7-1202-43a3-a2c0-ddeaf25aa26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "1a5d1644-28ae-41a6-9e3f-787b143bd23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "1fe8611c-2709-49f4-a75b-564278953c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "331fc5e7-1157-49cf-ac1b-3e1660b9f427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "8d3aa123-cb38-41e5-80c2-10a8c9d00c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "6abf6d94-cb81-4489-a36c-67960b085e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "84e1cf79-79ac-4c43-82ab-186f74c8e4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "21413aa3-9842-46fb-9480-8507560b3df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "569e9f3f-3ae2-46ee-84f5-c178c54cf287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 239
        },
        {
            "id": "c9da6707-86f4-4ac8-9f39-0213c16ed1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 240
        },
        {
            "id": "2a83a9bb-0cf5-41e0-b880-089be14f1205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "61d882f2-c6b7-4b90-b992-598eb56e020b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "1c4f6ba7-21ee-4dcc-b49c-b451750393c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "8d6c3634-6520-4b71-a662-3f076bdabfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "6692e708-dce5-4d16-8102-895946ef05ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "85609d58-8340-43ac-b641-06429a6e6250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "859747f5-a2e1-4332-93be-a8e447da0384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "88e609a0-b7aa-4d03-89a8-1ed0cc2c7de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "a95700f9-80df-478b-86df-c10e37849d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "bf1fca04-1cdc-4325-a007-ccff3ce44263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "29b2f3c3-59e9-47f3-82c1-711bb43deff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "508cd146-416b-4ae8-9390-56109f634674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "d8ce4cdb-e98f-4e7b-974d-d1a291288bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "d03d5910-8903-4ab0-8b90-97dddc5a8d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "dc2ef192-71c5-4fc4-af7d-482904396b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "2c0a7147-4f4a-4c64-9c2c-97b7b55f5022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "79edf25d-928f-4b86-9be7-dcd358acb879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "18df0153-6627-4c61-9c67-2409e26dfca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "42603de5-67ce-44ee-9457-2f3506d44c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "4604cc69-0042-4470-aed7-a43582ce8193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "5af2b795-cb34-4df7-a0d9-d31d41054ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "15a70c6a-1524-410d-b64e-829cb1916370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "cea48233-0bcc-47fd-8af5-7f92b30c1a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "63f1674f-4032-4a4f-a298-4e0c1956bf34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "1aa2c8f4-c2c0-41cf-8ebd-598f23695953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "a654d90c-96e1-46c9-846f-f6608ed035b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "14d4f5d3-0c2e-466b-a12d-f02a6a27d626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "da1a1b60-7cfb-4ba1-b2a2-309441190a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "a729d916-ed6b-4373-ba9a-7ed9cdcaa518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "cf569b5c-d253-4134-a164-d307b89a51e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 322
        },
        {
            "id": "7b89fab2-ee6b-45ef-8bd8-0ccaead0c521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "ddbabf28-270c-464c-96a6-1a091f2aaae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "070b153c-2108-4675-8e72-5d3bfccc55b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "1503910b-ec21-416a-b014-4abd71837652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "d01c07f8-5423-4618-9e12-b5656502e270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 341
        },
        {
            "id": "d4d2e16c-fe75-4a70-8148-652bbcdc77cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 343
        },
        {
            "id": "0ba8d947-9d04-4148-af50-87a040ce3bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "d6bab015-f39a-48fc-a038-99118786bbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "6fdaf6bf-0c91-4fb3-8306-7910ea9440de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "76430937-9295-48cb-969d-ef51565a327d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "a2a8116b-6721-4299-bbab-9373021572f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "ed04a4d9-bb85-46d3-9a32-735e5c67f007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7691
        },
        {
            "id": "67a653a9-37c3-469e-89da-adb6fc926060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "60b782bc-8aed-4c6d-a384-072e4af91dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "1a27804e-5f98-47a5-a715-39f08110999d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "fc9a2bb6-dba6-4b23-be7c-3be9ab7ff425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "4a2f9c12-8591-4f85-9b7d-9912b1b1331f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "9e1be435-0647-41c4-b170-8836181a7447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 253
        },
        {
            "id": "95a2280f-598c-48c5-9803-cc96e1e2f588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 255
        },
        {
            "id": "53b7f933-22f4-4185-aa2e-14c76b37471b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 322
        },
        {
            "id": "a153611b-27f6-4dcc-804b-ab9753204263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 375
        },
        {
            "id": "050fe5f6-8248-4471-ab81-c0b2766b91b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7923
        },
        {
            "id": "3e2f0ae3-7c08-44f9-8054-35a5850d28d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "f86b687b-e8d2-4262-812c-707fb38ef9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "7697c5e7-e105-48c5-a54f-c9d6539eed77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "caae8e42-7ced-4761-a26e-f24b75685396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "ee98ab5d-ec61-4cb4-8e75-cb865383f6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "644a61fa-152a-4941-aeb3-c041d1db0619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "cec0ee1b-253a-439f-a261-3757fa243950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "0092b901-6ed1-44b7-9e26-7e614c22d5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "256615b1-ee2f-404f-b338-de3e59327759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "53cb459d-7342-419d-ae0b-2c13c299e538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "193b3ec9-0a82-4574-a6da-2c9d205d3898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "911a63be-1230-4a9c-a048-47e5089b810d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b1e880a7-378e-4d10-ad1c-37fd230c0ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "bfebbfed-5f99-4dd3-b4c8-b43e912793df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "20d949c3-3a83-4d41-b010-f3b7be6176b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "5f1fa5f2-b7a6-43d9-88a0-aafc4860fbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "eabb8589-12f0-440d-a65f-bc55d20e1fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "55a4d041-a61d-4405-ab47-39fd0f7659c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "713660d7-1145-4646-878c-ec1bb644eb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "24fb462c-d8b6-47a0-a876-ac0ab3733381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "4bf167c2-dd23-4e62-a450-2e85067f3f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "91b8073d-fb13-4522-90f5-832bfe0837ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "12b7c20c-7d8a-4583-ba92-3b0eacdf06bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "e25440da-5a4f-46e2-9889-fc468708533e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "b506dccd-57c1-403d-8216-4fa40b81da2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "e9bf67e6-0adc-46bf-ab29-feb913736f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "e78f030b-a0f7-4dc7-9597-ac9def0da930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 187
        },
        {
            "id": "763da436-9c18-41ea-89bd-6e515eb83b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "f8e457dd-04cc-4247-a2c0-f7e3e19c526c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "402a2d57-6df4-42cc-a221-db88f37e3a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "017b348e-9dcd-47ec-85f0-06fd08edca39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "517a24af-38d5-42da-959b-82581bb06957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "2ed49c5e-1b16-417e-bc34-f97633ea28a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "550fbbea-4723-49fd-af44-98942f303a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "459b4f1c-440f-4db4-8eac-6ce3a1916deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "3ea09b2b-c709-429a-a62f-38f48d8caccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "15f64494-4921-4ed4-a20e-d134d2a387c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "27458054-bc09-4f9e-ad3b-f44d69000c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "6bcc31d8-3464-469b-8983-5ae986d713dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "489c4cf4-5b82-4d23-af0a-cd9653ebf160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "a2fbd742-f434-4db7-9bc3-00a62d34bc1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "638b0e99-ae25-4320-9148-fe91600db557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "eb3908ec-f6d1-4be6-aa1a-f61ddf9c66ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "3de2d672-5631-4112-81ee-002284fb3728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "9b91a017-5953-4b77-8ed3-cc26da90fc43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "f7f080a5-05d2-44f5-9040-599bcf1d50c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "dc96d227-d860-4b43-afc8-efe1ae7d3acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 237
        },
        {
            "id": "2899b3b5-74c9-4729-b32c-e7730e7ccfdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 239
        },
        {
            "id": "76c49063-8427-43b8-ac54-f92f6be4f039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 240
        },
        {
            "id": "57ef6336-9bfe-43f4-a5e6-c4bbbfc66324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "78efb6bf-e006-4ee9-8efd-cf46db264b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "20c02881-431b-4419-b032-8d56fb666559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "b8383c49-cfce-4573-ba4b-1c8c959a398e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "739a906c-c675-42ce-a113-a90905655425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "ba500069-0907-4484-924d-bb215578b393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "50b4fd91-7da9-4535-a4e8-7b060571be6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "932e7ab2-2f6b-438e-887e-c667a8cfc3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "1a11248f-1b25-4a06-92d5-585b869d418c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "49cc0957-32be-4d05-9aec-02730e011d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "1c96c1df-76e2-445f-8673-39538a9c6d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "71cd7e42-938e-4f13-a46a-a62a3783f64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 253
        },
        {
            "id": "27977f4e-92ec-4409-8f44-5914dc3a95af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 255
        },
        {
            "id": "6d591294-6c56-4781-93af-fe88ea41a908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "849ec742-c1fa-42e4-9ff5-b5435fb8687c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "cd2b7e66-e59d-4a59-8bbe-ee83194bf2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "cafc675f-16b3-4000-b7e5-9a3be8e1bfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "e0d2ab26-ec3f-434b-ad96-9bc19bbae005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "3322e4f7-fa24-4671-9c25-b246421889c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "35ad1b26-9120-4711-a0ae-983a55fc7a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "7fbd3a67-1cce-4883-a39d-9227e62c2f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "0665cbe4-eab6-480a-840a-3cb9dafa1e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "fa2aa06d-c3fe-40d2-a4f2-21809728b2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "fd359991-81a8-4a80-8388-ab21fd761036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "694bb9a8-3166-463d-9c9f-a391eca6fd7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "9e5214e8-ed0f-44c6-95a2-523d40992390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "89ce3e2c-95a2-4d8f-906d-d72439782401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "7521091a-3be6-4fda-93d6-858cd8526772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "81e51880-dc94-49df-841f-03145ae6532d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "0e976ca2-559f-4baf-ac67-a00146e6a0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "8636a18e-1763-4a55-b8b6-ca58539912f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "c447122b-b8ae-46e7-a33f-d1d52fb76daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "e3749250-b54e-4d00-8ff0-74295dd44a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "bf173413-69c9-45b7-859f-c029928d95df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "6e9c2897-2dc4-44a4-8efa-1b9ad894898b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "b35b907b-abab-4ec4-839a-f289f8fa0542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "596e1e7f-d713-4012-a844-fafb7b189e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 305
        },
        {
            "id": "c1780c8d-f188-4f90-bb84-85d3bdec9acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "877f2980-4d84-4f16-947b-eff4e965658c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 312
        },
        {
            "id": "379c7b1e-af37-4064-841b-a4e6217e4a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 322
        },
        {
            "id": "b202207a-73a0-458c-9193-acbed2009bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "ff639ed3-d014-47e8-90a2-e985e5c3a507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "572eb9f5-bc44-4a82-9a09-83261de0feb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 328
        },
        {
            "id": "b28a265e-da8b-4aa6-a3e3-def0363fa903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "e9354887-8b17-4f06-9a35-e5b0ce117ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "21e25db6-6b0f-4796-a8c3-8b67cbe13f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "67e22d31-6857-4a1b-8755-b326a9e01a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "35871e14-299a-4b46-84e9-97732b8d5a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "ead36388-32fd-4438-9451-092ee33acfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "9f262c0d-2863-4ab2-ac4e-5e5d9f6e756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "8dfea52c-e63b-4498-a41e-aac8e8447b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "c73198a2-499b-4c2d-bed1-da14e4948767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "896cd48f-e7ac-4f7a-8d06-5c83f34b7638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "976050ae-cf69-461d-b52a-ff9d4c1683dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 353
        },
        {
            "id": "d3fd14df-c1ab-4daa-9b55-fd01833fa67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "1c42a399-3a59-448b-94ad-fb5274b07b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "3c4fda12-2bc7-4356-a48d-993b5c1fe852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "b885a414-4f67-45c9-86e8-9439c58b8b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "b58578a1-52d8-43fa-8786-772732039f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "f62c7346-91a6-4f99-b28f-fa001e521556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "b1a4f97c-6c93-415d-8249-9970dbc144e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 373
        },
        {
            "id": "a6d242bc-22de-44e4-8de3-0f7ac75d3507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 375
        },
        {
            "id": "f79cbe0f-ac48-47a3-b297-aff94bfdff86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "8e75b05d-e24b-4ad7-8ef7-62ac2746117c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "626d4a5c-515c-406e-99cc-4cedb15ac955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "e237f645-424b-46bc-a01d-3d299a5b148e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "28c18dc6-e1f9-4daf-841d-76b173af6af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "b33ebac6-a57a-42a9-9d0d-d2364a663bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "1c827aac-d791-4231-85fa-cb3772dc1b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "78a9a755-7916-4187-b4ae-c0608d3ee5ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "eb74c15b-0286-45b0-ba07-f796715e4a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "38e764f6-febd-4c91-9b57-9b88a1e8fcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7691
        },
        {
            "id": "6d2b7dd9-0412-4f57-bc18-41656e9851da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7745
        },
        {
            "id": "f4362381-6bf9-42b6-9172-f8c05e465fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7767
        },
        {
            "id": "9b901a8b-c501-424b-be00-7594252922be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7777
        },
        {
            "id": "a5ebf405-d80b-4ba5-8358-1ad1be2ac8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7809
        },
        {
            "id": "9b20f7ab-fd82-4bac-a67f-fc5dab0e0be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7811
        },
        {
            "id": "4ad5622e-b939-4f88-8b47-a5ba69662517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7813
        },
        {
            "id": "aab10c47-3cb6-491a-aab2-c91d5701044c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7923
        },
        {
            "id": "c8a59c43-6bbd-4549-87c3-f300b850557a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "ee2a4825-5705-4907-af77-798086c32633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "df6cb40d-3787-40f8-86ec-2d1cabe60a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "87931fcd-2a4b-4fbd-8cd0-530f4cd3e876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "ddb3d76c-d8ba-41d0-bd2d-ffc194a7c0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8230
        },
        {
            "id": "fd57f2db-2526-48f4-9cee-16cfbc5e25ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "190ffdff-3d0a-4c03-9af7-4d276ea1974a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8250
        },
        {
            "id": "e37230f8-619e-4216-bb9c-7627c4bd72b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 322
        },
        {
            "id": "ff0518e7-bb1a-4628-92b2-bb2827ca2466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 65
        },
        {
            "id": "32e49251-6003-4e4b-b23a-b99ca3f78fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 99
        },
        {
            "id": "3b3e1476-31c6-4a9a-aad6-f9d5a76171cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 100
        },
        {
            "id": "2848c13d-281a-42d4-a840-1267ea26e0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 101
        },
        {
            "id": "58cd3a38-00a5-4c31-8ad3-80447438c957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 103
        },
        {
            "id": "83050e94-3bf1-4749-8da2-b35769e52a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 106
        },
        {
            "id": "913fd2d4-2a06-4c89-bd07-260d54e568fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 111
        },
        {
            "id": "d4f4a577-adfc-485a-a495-0ca6f3f38ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 113
        },
        {
            "id": "90f7f309-a41b-4d1f-aeab-06ad9158d0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 192
        },
        {
            "id": "31e0ca73-1fc9-4518-897d-01ede539b656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 193
        },
        {
            "id": "ede09dfb-62f7-4a74-b345-2fc8e0697ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 194
        },
        {
            "id": "4e8b22ff-2027-4ab3-865e-350e05257cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 195
        },
        {
            "id": "961d996e-f662-4ab6-9742-3e7daeacfc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 196
        },
        {
            "id": "a09c0e43-292a-4621-a7df-acfdeda1ea01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 197
        },
        {
            "id": "34f21b0e-e92b-41e6-bc1b-f899b9f3cad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 198
        },
        {
            "id": "3a481e68-faa3-447e-b9d5-f8094ddcb462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 231
        },
        {
            "id": "7a0574fa-ece3-4656-9536-21271331c3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 232
        },
        {
            "id": "b5a19ed3-83c0-441c-8a3c-69019c8baedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 233
        },
        {
            "id": "a8b87bbe-74cd-443f-8e93-61df29d6a050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 234
        },
        {
            "id": "7e576720-90f0-4d96-804c-bdc719fd5f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 235
        },
        {
            "id": "1e8e27eb-b070-408f-ae59-e95077e8da4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 242
        },
        {
            "id": "d27a026f-7cac-460e-b10f-873a42ba497b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 243
        },
        {
            "id": "312f88ce-554c-4be3-9abd-bb16df7664c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 244
        },
        {
            "id": "40335571-5d39-4a7a-aacc-e251a613f83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 245
        },
        {
            "id": "05e76d9d-456f-4e7b-b9c0-b510951d1c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 246
        },
        {
            "id": "5b0f56e8-23a7-4347-8aae-6e7e5289b526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 248
        },
        {
            "id": "71cf6c69-330a-4c1c-b122-693a48e3ac45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 256
        },
        {
            "id": "63c5a626-8a7f-4b30-92de-66582ccd83f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 258
        },
        {
            "id": "76e990ff-f534-48e4-a007-aab3d0bcce0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 260
        },
        {
            "id": "e89d4635-ffab-4b86-bca1-7489ab8d3aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 263
        },
        {
            "id": "4b1d4b14-db71-4d6b-9a8a-eda1f010fb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 265
        },
        {
            "id": "e3311399-74ec-42b1-9c41-c5cab4cb12ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 267
        },
        {
            "id": "5448d2c7-94f2-46b0-b2e2-c640e343762a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 269
        },
        {
            "id": "795818f3-fea1-4474-a887-dac7c87a58e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 271
        },
        {
            "id": "43866a89-c77c-463e-997b-36927fc7cb47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 273
        },
        {
            "id": "f7831bd0-ab61-4065-9db0-18e199a2daff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 275
        },
        {
            "id": "50809d8e-85e9-4149-b82d-715936fe8b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 277
        },
        {
            "id": "904f23e5-9f30-406c-a45b-12c2bfe13dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 279
        },
        {
            "id": "0687b7d5-627e-4734-9b58-671538ff5bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 281
        },
        {
            "id": "1c8d7474-06a1-4dd3-b742-25f040d1395c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 283
        },
        {
            "id": "17930cb2-3010-49af-b526-501025780ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 285
        },
        {
            "id": "fa6eaceb-520b-4d73-af9b-a79bee5b9653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 287
        },
        {
            "id": "9e98aabc-9ffb-41c3-9b44-328f78da5f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 289
        },
        {
            "id": "3373ecf4-879f-480b-b34d-fdf6a3de4014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 333
        },
        {
            "id": "c0ecb62a-b3bf-44a0-8d51-da7ea5db89c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 335
        },
        {
            "id": "72980f5f-548e-4867-98b2-486d7b05f7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 337
        },
        {
            "id": "cc4b1df4-1f33-40c7-9beb-055030883d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 339
        },
        {
            "id": "dc2c6de0-f93b-4cb0-ab40-ada0c6840a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 506
        },
        {
            "id": "85a43387-8f82-48c1-8792-1a5d257ce083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 508
        },
        {
            "id": "5be0c6f3-f93a-4f61-8db4-87d5494d2ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 511
        },
        {
            "id": "207460c0-c1a5-439e-9a53-d4db42aee8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 913
        },
        {
            "id": "69992182-0980-4639-9aaa-5293727d25b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 916
        },
        {
            "id": "e6cdc6ad-91ae-4eb1-88b0-cf1d1b575e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 923
        },
        {
            "id": "690febaa-6ca7-4c39-9d3d-6f14a8fab684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 940
        },
        {
            "id": "9e2d8ef4-85ca-4659-95af-9b989fae46d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 941
        },
        {
            "id": "ebc2ab81-682f-4686-80d2-6c218cd72e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 945
        },
        {
            "id": "92625bc6-9bc2-4e1d-995c-4511c5ef7b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 949
        },
        {
            "id": "6dcb8541-6637-46aa-a4cd-4f071d44d70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 959
        },
        {
            "id": "8e69fa39-bd70-4d5c-a71c-4f7d664f178f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 962
        },
        {
            "id": "0faf6664-a79c-455f-9e10-27812882899c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 963
        },
        {
            "id": "72786663-5324-4a1f-88fd-cb67c2624be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 972
        },
        {
            "id": "4f0f52ee-8e81-4be0-915b-84288dae0924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1040
        },
        {
            "id": "ea9604cb-340b-4105-b830-265ad878ce62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1077
        },
        {
            "id": "eaeea218-7acb-4715-a045-c57e51840cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1086
        },
        {
            "id": "c3e1af88-b570-4125-9611-4b3efdbb7b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1089
        },
        {
            "id": "e5bce4de-1c56-4a65-a44d-b6a84b4da691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1092
        },
        {
            "id": "6e52692c-d9ff-44a4-a87f-1386f401ed23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1095
        },
        {
            "id": "e67b05a3-1c1e-4ba1-a173-e132e8a70461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1104
        },
        {
            "id": "0666bb16-bccc-4982-a2c2-332b5a15c67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1105
        },
        {
            "id": "09a082ef-b69b-43fc-8e8b-6535342e1db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1108
        },
        {
            "id": "47fd6579-0064-4967-80d6-0aba670b8a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 1112
        },
        {
            "id": "c0aa249b-e7e6-4fcf-9daa-5f08762685e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1139
        },
        {
            "id": "094a9922-225d-43a6-a9cb-248a61b9fe30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1193
        },
        {
            "id": "64dc1347-a723-4882-b9f6-8646e275da39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1195
        },
        {
            "id": "4a54770e-0aad-424c-8238-99725371ea0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1207
        },
        {
            "id": "6113214c-f3cd-445c-a1c9-b148f569693c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1209
        },
        {
            "id": "e2829878-ae28-4b59-a328-1d8b191973e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1228
        },
        {
            "id": "f44ac266-dfc8-4979-b77c-8b1b28d3137f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1232
        },
        {
            "id": "e168a1e6-088c-4099-b79f-9454427165f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1234
        },
        {
            "id": "3195380c-9d9c-416b-90b3-37ae9262a508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1236
        },
        {
            "id": "339a4429-c7c4-4c80-a2ea-52b3c2efe2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1239
        },
        {
            "id": "9a3521f4-0593-4b0d-83bf-7b47b634f2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1241
        },
        {
            "id": "e5f8a0c6-7a27-4a62-bbe7-ca02240032ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1255
        },
        {
            "id": "1212542f-af1b-4a0e-8174-d52bb2e34512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1257
        },
        {
            "id": "aa5102b9-2a15-4b0b-b8e5-f9f36bb39b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 1269
        },
        {
            "id": "31bbe54f-8677-40b8-9b34-a4e45d8f3532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 91,
            "second": 7691
        },
        {
            "id": "15d24689-aa7e-48ea-b892-cfc10b2837cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 84
        },
        {
            "id": "546126b7-c089-40dc-9a9a-9b290bbd4c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 86
        },
        {
            "id": "2d92c940-3834-4b5a-90ea-25e173a158d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "caff3a4f-2631-4e26-80db-2488b4ca9ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "cdb58482-1a0b-447d-963e-a84c15aaab5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "edb1babc-2b3f-4e45-b42a-079661d6c60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 221
        },
        {
            "id": "e5aed433-f693-4e64-885c-709cd7013547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 354
        },
        {
            "id": "51cc8704-746a-4b44-92e2-a22e32e225af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 356
        },
        {
            "id": "e44dbfa9-ba62-4752-bbe9-6b1f517fbb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 358
        },
        {
            "id": "4fea4ddb-8291-4672-8b29-14db2307069d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 372
        },
        {
            "id": "f7d65415-bd95-4b93-a867-0c75461995a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 374
        },
        {
            "id": "4904d78d-0b25-431a-8af7-0a2928c75e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 376
        },
        {
            "id": "155416cd-85a5-412c-9965-cd09f3e40556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 538
        },
        {
            "id": "2ff64c2d-331f-4e98-bb33-700bc4e14f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 932
        },
        {
            "id": "e7a8f112-59d6-4574-8082-8be9618bb220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 933
        },
        {
            "id": "9d89573f-6556-4aa6-8307-0d35c1ea23eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 939
        },
        {
            "id": "82af90d9-86f2-4b24-baf0-7076d531eaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1026
        },
        {
            "id": "2982f8f8-d441-4ce7-adf9-46e315d7023f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1035
        },
        {
            "id": "4c4a4f29-e751-484a-a388-11beec21ce17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1038
        },
        {
            "id": "4b72b7e3-57f6-4dd5-baba-1c580d26dfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1058
        },
        {
            "id": "100928f8-ec4b-4706-8254-3426640ccd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1059
        },
        {
            "id": "c9eedfe1-946c-4d00-a449-c0632c3fb587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1063
        },
        {
            "id": "22739741-7a8f-49e5-9b44-0337e6ab6b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1066
        },
        {
            "id": "813c36c3-61fa-4785-a151-6df3e46d9480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1184
        },
        {
            "id": "9a986511-40a9-40ef-b65c-057266c9b393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1196
        },
        {
            "id": "7f122014-db0c-4435-9bf8-40ded7e62696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1198
        },
        {
            "id": "43c706bb-d5d6-4a0f-a003-c467e2c903b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1200
        },
        {
            "id": "013032f2-1c22-4451-8d30-16e80b4d1371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1204
        },
        {
            "id": "89f06d00-801c-4faa-97bc-6cd1212d155f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1206
        },
        {
            "id": "08c9f417-1314-4a67-893b-329ef298e46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1208
        },
        {
            "id": "c1062a74-7ea8-40de-b448-2011a37146b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1227
        },
        {
            "id": "a675715d-a496-4621-a893-02fea52fb05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1262
        },
        {
            "id": "c71b4556-9437-4cf0-a94b-bb3c72610479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1264
        },
        {
            "id": "bf2546aa-8406-4c7b-85a0-f50fe420a54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1266
        },
        {
            "id": "c95b0b13-6b43-414c-b555-3528de614ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 1268
        },
        {
            "id": "d2ab501e-0110-4baf-9571-c2542e6a4ba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 7786
        },
        {
            "id": "555e3265-83db-43a0-886f-b78a93b0a953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 7808
        },
        {
            "id": "8619c925-6f36-40e5-af94-e33f590f5fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 7810
        },
        {
            "id": "7ec0a465-efc6-4e31-b4db-1a8da0452baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 7812
        },
        {
            "id": "124b5df5-03b9-4348-817e-06e4259c494a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 7922
        },
        {
            "id": "91542a7b-69fc-4109-bcae-96a1bd5664e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 65
        },
        {
            "id": "2ff5a5a4-9875-430b-b1fb-093edb137188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 74
        },
        {
            "id": "a3c54d56-a177-437a-a058-92f1677b5909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 192
        },
        {
            "id": "a6cd994f-0b34-401f-8875-f8720e57edb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 193
        },
        {
            "id": "db2e2c60-6b03-4330-8d06-6733343a1fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 194
        },
        {
            "id": "92306bf0-a544-4633-ae98-0a2e67909eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 195
        },
        {
            "id": "964b1638-f2da-4b0a-8929-cebcc324e294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 196
        },
        {
            "id": "1e9b56c6-21a6-4dbf-95cc-47b88ecaf7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 197
        },
        {
            "id": "a7d44600-9441-49ae-9541-5910bcb3fef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 198
        },
        {
            "id": "94ce01d5-e853-400e-9cf7-d5a8cd8688e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 256
        },
        {
            "id": "6b1d904a-7e5e-4904-bc57-f2aadc5af281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 258
        },
        {
            "id": "08d0d887-a05c-4bb0-91c9-e1c6c0379c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 260
        },
        {
            "id": "cceae36c-26d8-4278-84ae-e0682a7d9872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 308
        },
        {
            "id": "5f105f54-1762-4afb-a9bc-c6e4254eedd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 506
        },
        {
            "id": "56a0febc-9727-46d8-8822-81b8a3f16267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 508
        },
        {
            "id": "b9d8eaff-6624-4a01-ad2d-c3ee37b8b328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 913
        },
        {
            "id": "ea07d098-8822-442a-889c-2b8db686e48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 916
        },
        {
            "id": "d8904f6e-ac2f-4bf2-a15e-46b998b137f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 923
        },
        {
            "id": "162ffa61-027a-4a6e-a333-d209a7c7e74a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1032
        },
        {
            "id": "28e24f74-a4db-4298-8ea9-079053f48171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1033
        },
        {
            "id": "ed7afedf-c115-4b2a-aded-d7a53894cd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1040
        },
        {
            "id": "b7d70333-0b10-4dae-a0fd-4fdc98249b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1044
        },
        {
            "id": "c53b732b-42ed-4060-967d-2bc6e4f8baba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1051
        },
        {
            "id": "41df9f75-1028-40a8-bf31-43df283feebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1076
        },
        {
            "id": "194ea408-1f9a-43a4-ad38-133e1c27c4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1232
        },
        {
            "id": "0786c2d8-9333-46ef-86cc-340c81556fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1234
        },
        {
            "id": "8cf16a50-6cc4-42e8-97ef-6f95894a3a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 94,
            "second": 1236
        },
        {
            "id": "180c4ab1-c724-47a8-8ae3-b9bf245917f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "ad79b767-8d8c-45c5-8837-646d8fbe2dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 86
        },
        {
            "id": "7682b445-49a9-4a06-8800-9c49f9f2fd26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 87
        },
        {
            "id": "d59d3eca-9ee7-4aae-b126-cd2680b4dfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 89
        },
        {
            "id": "acf3f79d-8b46-4e09-af37-fac4bfd0150a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 221
        },
        {
            "id": "ee2ae7dd-8abd-441c-b7c0-e7b4950ab484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 354
        },
        {
            "id": "2495e538-dc1c-4de2-81fb-7778631be2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 356
        },
        {
            "id": "e6902a9c-93a7-43c7-acd8-5c307cd12bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 358
        },
        {
            "id": "3046eaea-e92b-4a81-9f90-b05cbdca204c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 372
        },
        {
            "id": "d1176e7d-711b-4567-b630-4b1cc4749099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 374
        },
        {
            "id": "52e7f392-1913-4227-81d7-1dc1561a311c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 376
        },
        {
            "id": "f9f203f9-ec73-47af-be34-4e760f49d31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 538
        },
        {
            "id": "91e9bb36-cb32-4f1d-8083-1818cd0e787b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7786
        },
        {
            "id": "32ee4269-ab21-4565-9091-d16172fd0193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7808
        },
        {
            "id": "ac9eba67-93f7-4893-8130-c547b870698b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7810
        },
        {
            "id": "9a55ccc2-b25f-492b-aa04-2f751d9efbf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7812
        },
        {
            "id": "85faf881-2a00-4a05-94ee-2412bfde1dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 7922
        },
        {
            "id": "87b64fc9-3d8b-47f3-b561-76ad77a65ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "8ef7f806-8e4d-46c3-a06b-c2aed26a2c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "a329f405-6f5f-40ba-8714-31e14022b152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 86
        },
        {
            "id": "fb35fbdb-277a-4fed-8e6d-1bb9ddccefa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 87
        },
        {
            "id": "9ce57798-b66b-4368-9c9b-bcad41fbca23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 89
        },
        {
            "id": "0ad1effa-7869-49c6-a0b5-43aee26f3976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "42a78fcd-2dd7-41de-979a-0a428483de45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 125
        },
        {
            "id": "54272da1-72fc-49a4-84b0-6841b583bbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 221
        },
        {
            "id": "1a6e1ece-54d7-4dad-b6d1-2bc4a9040847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 354
        },
        {
            "id": "e0a6c6a7-0bae-4e8d-a3cf-c20cb4e5bf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 356
        },
        {
            "id": "729cc60f-1fa5-4ea2-82c3-51a8b97c7d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 358
        },
        {
            "id": "4ed7659a-e785-4f79-a359-b18bd8c414fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 372
        },
        {
            "id": "d3be6b65-4357-4f01-b05c-004bc8faad7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 374
        },
        {
            "id": "6a73816d-c857-4cef-ad81-548a31da5369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 376
        },
        {
            "id": "894c2aef-8182-45ce-9136-33e6cb2efc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 538
        },
        {
            "id": "501577e6-1f29-4842-89f4-81a2ca4738b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7786
        },
        {
            "id": "897a32bf-5538-41a1-b913-a79da2e5884d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7808
        },
        {
            "id": "d593e8c0-9c47-47d4-b14d-fe53db47e9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7810
        },
        {
            "id": "62d72fe0-a831-41c1-a4c1-483d9bae93b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7812
        },
        {
            "id": "38b5304e-f9da-4d1d-983a-7232afc8c809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7922
        },
        {
            "id": "6076cfbc-9ff5-442d-bca3-61c081a58cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 10097
        },
        {
            "id": "d21a56ad-bb3c-4da4-b117-16501ee1da9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 10099
        },
        {
            "id": "fcd38e46-4430-404a-9b62-555ba4638e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "af4c84bf-3d88-4791-b347-814b96414dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "8ca5d700-7088-4fc6-8aa0-fca0e41907f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 86
        },
        {
            "id": "cca0c98b-d25e-43c7-84e2-a1be21d8c255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 87
        },
        {
            "id": "5652a6c7-3e1a-49cb-a892-9f3eb9897c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 89
        },
        {
            "id": "3d309e04-bc8f-4bad-843c-d97c271288e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "a1b1967f-cbe2-42d1-b3ca-b38432b488ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 125
        },
        {
            "id": "7e6b22cd-ebbe-4da6-b5d2-f703913e2ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 221
        },
        {
            "id": "0ed5cc0a-b97a-4f08-8bea-0c83e45ea3db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 354
        },
        {
            "id": "db51920f-9368-4ea5-a1f2-bf9274298ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 356
        },
        {
            "id": "55615db0-3426-4b30-aa94-40369cbf1d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 358
        },
        {
            "id": "cb18c943-7bc4-4f5b-b1fe-615c8199c7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 372
        },
        {
            "id": "d38cd9e6-f9e4-48f3-a7fb-d7b2d803ab02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 374
        },
        {
            "id": "127043ed-9339-44f1-b264-e4fb88b09675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 376
        },
        {
            "id": "d72be861-8e5b-4c7f-8574-fd3b69c5f391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 538
        },
        {
            "id": "8fd0d1ba-0efe-4577-966c-24e664d7e716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7786
        },
        {
            "id": "718b3d52-4bc6-45f1-868e-324c2d6c5100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7808
        },
        {
            "id": "52d561c0-e0cb-4bc4-adfa-5f99be47d8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7810
        },
        {
            "id": "374500e3-2f87-4e5a-ab5b-1edaf33aa58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7812
        },
        {
            "id": "41fae5a1-cfe0-4d8a-bd2a-cbbb707c0ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7922
        },
        {
            "id": "2551c653-2113-4308-b4f3-31402d88f9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 10097
        },
        {
            "id": "f1cc3b9e-6a06-4a56-8070-a81d56ffbbf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 10099
        },
        {
            "id": "f28af7bf-cc50-45c3-a968-4c39990c8af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "b4ac4b69-2e82-4f77-9d5a-fbae182ac814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 192
        },
        {
            "id": "f3b47ad6-a8ad-4af5-8e32-5fdaea085d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 193
        },
        {
            "id": "c8bfdfcb-cdc7-4e2b-b41c-1f5113074369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 194
        },
        {
            "id": "4b9130b2-61b3-4ac1-88f5-14b161be491b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 195
        },
        {
            "id": "6a737dce-1123-46fa-8f86-b8fb0df2cd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 196
        },
        {
            "id": "3ff70eea-0b08-472b-a66a-fcb9cd239d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 197
        },
        {
            "id": "2e20e1cb-0728-4b7b-b4a4-7bba112a745e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "afd80258-cd23-4733-82d2-f1d30d2de623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 239
        },
        {
            "id": "bb054aac-73ad-4f95-b87e-7b67d4a05f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 256
        },
        {
            "id": "3361260e-58c8-4add-bb86-f1d65a9e18a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 258
        },
        {
            "id": "4b3ba3e1-a2cd-4c46-bbc8-bad41e1ed7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 260
        },
        {
            "id": "2c80db3d-eb79-46a6-bb03-c1a7aef72fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 297
        },
        {
            "id": "d7da54ef-03eb-459e-88b6-679947ba149d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 299
        },
        {
            "id": "9ac03469-63a8-4567-9f47-a463d618e61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 301
        },
        {
            "id": "c4ed0ed0-6dee-4add-90ff-34422e0633b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 506
        },
        {
            "id": "b3b27d8a-0b68-4042-bd75-cfa743ab780a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 508
        },
        {
            "id": "96d403c3-8974-4b42-940c-a3f18ae25994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 84
        },
        {
            "id": "f15fd51b-4351-47f5-889f-48022635b05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 86
        },
        {
            "id": "0dd1d63b-79dc-49e6-a297-aadec66d217b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 89
        },
        {
            "id": "6ef57327-ba48-4cc1-aaa0-ff2897d89abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 221
        },
        {
            "id": "24155f2b-476a-4f47-b83c-bce6f71cb058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 354
        },
        {
            "id": "4b7284d6-920a-4037-bfeb-434e76325f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 356
        },
        {
            "id": "27634cbe-89f0-463e-84e9-6137b9fe2391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 358
        },
        {
            "id": "6eae5a04-d891-4c58-b2da-e27b35bf3629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 374
        },
        {
            "id": "9d822171-b165-4330-a0fe-860606c0e0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 376
        },
        {
            "id": "e7495ea7-6837-428f-bb83-7d655dde2de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 538
        },
        {
            "id": "c56e09ea-9bf0-454e-aac6-cf64889a3a78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 7786
        },
        {
            "id": "3385cc4b-de5d-4478-8db8-6ea6673cb5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 7922
        },
        {
            "id": "de65b87b-6648-4c0b-94cf-168a0bee6b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 84
        },
        {
            "id": "52b8a6cf-6b2d-483e-8956-9ead317edf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 86
        },
        {
            "id": "08cda4bd-a67b-4652-b122-d10c0b4e5c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 87
        },
        {
            "id": "95f9bfb5-8d06-4cda-a5e7-bb81b0be992d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 89
        },
        {
            "id": "6524b75a-68bd-44c2-ad6a-94aa78e9e9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 221
        },
        {
            "id": "d23b930e-4bfd-4bcc-b030-500e47d1fcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 354
        },
        {
            "id": "23c918d5-f940-431a-8989-e09ce849f2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 356
        },
        {
            "id": "8b7a53f6-8142-4be5-b4d8-a1369bae02d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 358
        },
        {
            "id": "31dc9f15-bc42-43ba-9007-87fc8b4d9b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 372
        },
        {
            "id": "56d1e412-65d5-4b05-a45d-4a99b0334a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 374
        },
        {
            "id": "bc02b5f3-1c57-4f5c-b964-a989cf12afc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 376
        },
        {
            "id": "73a99432-924f-4e5e-8e34-0ae6af445eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 538
        },
        {
            "id": "2f86d93d-6d91-43dd-b239-29227c108b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 7786
        },
        {
            "id": "eb9a7a33-62c5-4a3f-8811-98618c9ba672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 7808
        },
        {
            "id": "fcf2c38d-08e3-4744-87ef-5a6b2c72fe92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 7810
        },
        {
            "id": "6d268e4d-53be-401c-9d3d-49e31d1c465a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 7812
        },
        {
            "id": "25571935-65af-4434-9588-aca30929700e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 7922
        },
        {
            "id": "c6c2714b-6223-49b1-9e08-5bbff29c0c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 84
        },
        {
            "id": "412ca377-80cb-43e3-8e3a-4213c1b1b404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 86
        },
        {
            "id": "f5304a6e-0868-4683-a9e9-e590237e7b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 87
        },
        {
            "id": "4859e3a7-f47e-4d85-8f35-23df2f447459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 89
        },
        {
            "id": "723c2f91-fdb7-4a9a-a43d-66701bba6df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 221
        },
        {
            "id": "1db4fcce-9cf3-4c19-a83c-d7c965858f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 354
        },
        {
            "id": "25b3ff04-d736-4a51-a7ca-e7df5539570a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 356
        },
        {
            "id": "00da35de-15ac-4cd0-addb-184a4f8f10f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 358
        },
        {
            "id": "e978becc-cbeb-4dd1-a60b-790254728983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 372
        },
        {
            "id": "e57c8ee4-76ac-4499-8001-e574df1bb8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 374
        },
        {
            "id": "02022b08-01ad-451a-8ea5-8b823da80ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 376
        },
        {
            "id": "f2d318f7-ced3-4b74-96d8-2228b4c4e35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 538
        },
        {
            "id": "c33467f1-97ea-4638-9b05-9506024e1601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 7786
        },
        {
            "id": "bbc69f4d-71af-4e1c-819d-da47f26c3110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 7808
        },
        {
            "id": "197ac5a9-65ec-4656-8326-afbca10fe7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 7810
        },
        {
            "id": "45484595-f04e-416d-99e3-dd1cb0c3b95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 7812
        },
        {
            "id": "ed16505b-d5bf-4720-af1b-180f2208995c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 7922
        },
        {
            "id": "adbc9e50-5c71-4f48-9457-77851d76aef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 84
        },
        {
            "id": "03eb2a28-ff62-4b6c-a016-c0a37edf4c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 86
        },
        {
            "id": "bf672136-b8fc-479d-9cbf-55052f43f6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 87
        },
        {
            "id": "c71eebd9-b875-4301-a118-9ade7e432661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 89
        },
        {
            "id": "7bf63446-a8ed-4856-a03b-acf257c6c046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 221
        },
        {
            "id": "7d27d393-8daf-4d95-84ba-a1d6865d5668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 354
        },
        {
            "id": "077ab481-c1a8-455a-8142-66625f0ced74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 356
        },
        {
            "id": "b870f63b-bfd3-45db-9dc4-56f1864c84ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 358
        },
        {
            "id": "0bda7501-76e1-4f00-ac49-f868a9d76ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 372
        },
        {
            "id": "591e5ab7-c148-402d-86f3-10cd22ce65d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 374
        },
        {
            "id": "8eda97d4-3854-4a00-bb6d-3a5077dc5d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 376
        },
        {
            "id": "1621ed13-558d-4796-a8ab-eff83e0b708f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 538
        },
        {
            "id": "f6f2052a-9639-45d3-a702-efc9b51bc635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 7786
        },
        {
            "id": "e5d8d324-4eb9-4d2d-b979-aa460cd8e0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 7808
        },
        {
            "id": "8e0f90e7-1092-4100-97c9-198ff09b65af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 7810
        },
        {
            "id": "ed03b5ba-09ec-4ec2-b883-32a204bf1eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 7812
        },
        {
            "id": "b8036a9d-8932-4444-acf0-19f68a0c104d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 7922
        },
        {
            "id": "8e757cf8-0488-4475-921d-ddbb098ebd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "67f18fb5-e54b-4a7b-8f3b-def9e095bb8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 84
        },
        {
            "id": "0c172bbf-890b-4de6-b8fe-1ca662924d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 86
        },
        {
            "id": "971c8c44-68be-4871-9c00-dce369c0450d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 87
        },
        {
            "id": "519895d2-fe6a-4494-ba8f-507f63685dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 89
        },
        {
            "id": "05445121-e5d7-4790-9e2e-0a63ac8fd8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 93
        },
        {
            "id": "d02d6dc1-d4a8-4e77-a163-02aa758ac0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 125
        },
        {
            "id": "da34c5d0-d475-42bf-bb90-98249993e9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 221
        },
        {
            "id": "3bd42414-e218-4002-bf9a-1ab8851741bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 354
        },
        {
            "id": "604d8449-4dd4-44b7-89f4-5f747f2019ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 356
        },
        {
            "id": "4f19bc54-6e39-42ef-91c6-4f52ca082c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 358
        },
        {
            "id": "b650b97d-7bda-472a-9d6d-eb9f8b03ce1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 372
        },
        {
            "id": "91520c30-0b69-4af7-ab6d-8a11c1933aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 374
        },
        {
            "id": "92fa5c57-7ea8-42c6-bde7-8a7d9eba5a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 376
        },
        {
            "id": "7f62bb7f-d198-47c7-9abd-d22b0b7f83a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 538
        },
        {
            "id": "6a4a4d2c-65c4-4d0e-97db-7cba7eaf4a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7786
        },
        {
            "id": "b71d5615-b720-417d-812c-8e7c2e0bae3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7808
        },
        {
            "id": "41e22bb4-c1c9-4c90-8bab-5e580e00fd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7810
        },
        {
            "id": "9931296c-3286-4e03-a112-dffdb2928793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7812
        },
        {
            "id": "1ca49125-0eeb-429f-a5d6-a98616bc3070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7922
        },
        {
            "id": "2305d586-98ea-4766-9ddf-38f09736a177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 10097
        },
        {
            "id": "86abc7fd-ed57-411f-96ba-4661ef9ba401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 10099
        },
        {
            "id": "7ddc9952-5707-441a-9402-f06c4ab91f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "35809964-7da5-4cbd-a17b-0c8f580819f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 84
        },
        {
            "id": "17223e0e-fa67-4c1a-ad9f-f03070b29a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 86
        },
        {
            "id": "2a1ef976-6f5d-4144-a4f4-967f658f2cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 87
        },
        {
            "id": "e8c7a614-b89b-4efb-a396-cae2c39959f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 89
        },
        {
            "id": "b15b9a58-318a-483f-a2e5-d1faf9d61425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "d780464d-d8c7-401f-bbe6-0a79a1ef25bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 125
        },
        {
            "id": "0d2b9080-c44a-4079-9568-e4cd32917a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 221
        },
        {
            "id": "72a8a299-6373-4ab3-8e50-7765498d4a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 354
        },
        {
            "id": "46f17f54-ad0c-4501-b1d3-72932a3c614b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 356
        },
        {
            "id": "7f5544b3-bb93-421f-b7f6-efe08be9e34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 358
        },
        {
            "id": "09ca4727-99cf-42e8-b74f-6933a2012652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 372
        },
        {
            "id": "74df6a6c-d311-4f5e-9e0c-368adb51c446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 374
        },
        {
            "id": "606de470-5f79-4df4-81fd-4cba6937cdc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 376
        },
        {
            "id": "2ba4d159-15a4-4960-b4bd-fd6a60bc22ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 538
        },
        {
            "id": "699e7a52-6f6f-448a-b472-e09044136d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7786
        },
        {
            "id": "b3eab7d9-5e24-40a5-9138-92135ec6c053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7808
        },
        {
            "id": "c47aa778-728e-4fcb-92ed-fc21af4ba5f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7810
        },
        {
            "id": "96b2a691-2002-4f9d-b595-a7be55067011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7812
        },
        {
            "id": "66a187b5-473c-4bc5-af2a-603c62c6773c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7922
        },
        {
            "id": "d8e9c222-9041-4484-80cf-4d317c6f7b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 10097
        },
        {
            "id": "87880462-0a8f-4861-a423-92090467e7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 10099
        },
        {
            "id": "cca51f1a-321f-4511-86e9-701172c4b22a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 84
        },
        {
            "id": "4247102a-afd6-451a-a35a-7b1015919b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 86
        },
        {
            "id": "ffb03341-ebf1-4f04-a713-04df3449b698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 89
        },
        {
            "id": "de0cdc9e-adb6-43a5-9d60-3c7bf38e208e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 221
        },
        {
            "id": "a7ec7b6e-ee1d-41f2-8497-3f7f7b315ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 354
        },
        {
            "id": "bac8d770-facb-460d-b4b6-52a473439961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 356
        },
        {
            "id": "9d2833b2-9c65-49ca-a220-575b588eb697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 358
        },
        {
            "id": "779640ba-a72b-492a-9441-277af0afad64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 374
        },
        {
            "id": "24bf5a33-dca6-425b-af8c-ae25c6ac02e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 376
        },
        {
            "id": "2d57c2e1-e6f7-4eca-8715-f4ffadeec7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 538
        },
        {
            "id": "a2723424-1d34-431e-9e0d-b28b8b5e333f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 7786
        },
        {
            "id": "30ee4d8f-6e49-415e-b578-8661334ae8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 7922
        },
        {
            "id": "3a547baa-e201-4d09-8911-498ab3e12355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 65
        },
        {
            "id": "c74028d1-df67-440b-8829-6748b7166290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 192
        },
        {
            "id": "f2fca960-d455-45af-a546-ef57ee6f5d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 193
        },
        {
            "id": "fbef71e0-198d-492b-b960-f499c85fe758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 194
        },
        {
            "id": "c1923635-3a61-46b0-b176-7c333a608ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 195
        },
        {
            "id": "912a69c2-d6f3-4c75-b460-4d80a65bcadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 196
        },
        {
            "id": "dd06a980-edfe-41d6-8eb3-075c41b5b290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 197
        },
        {
            "id": "a9aed9e3-2479-483a-9f8d-b203c2f9ed06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 198
        },
        {
            "id": "3035e585-7d3e-454d-bb63-26db3a52d8af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 256
        },
        {
            "id": "2ce8aa5a-c261-4ceb-b869-3a5e2ca02b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 258
        },
        {
            "id": "2639dd3d-ad72-4fe4-9eb8-58537b6e139f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 260
        },
        {
            "id": "e8263278-d22f-49b4-b478-22618d018cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 506
        },
        {
            "id": "33421fa0-ecd5-481d-97a3-18b75a82d549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 508
        },
        {
            "id": "51417c0d-3e0d-4037-8833-beed0940f96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 84
        },
        {
            "id": "f88fe593-e764-4309-a673-7ef45d291661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 86
        },
        {
            "id": "1f99c74c-eb62-43f0-af5b-0c23a035e873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 87
        },
        {
            "id": "0d74958e-2c81-487b-8418-c9642a8e41aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 89
        },
        {
            "id": "970575b0-4979-4915-a1bb-61cab729c27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 221
        },
        {
            "id": "dae326a9-97ff-4982-87df-bc69874b3821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 354
        },
        {
            "id": "84c08401-8dbc-4717-8487-5ccd4061a51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 356
        },
        {
            "id": "6a3fef8e-90fe-4245-9fa4-962ab13ccd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 358
        },
        {
            "id": "311f50a2-73b5-4519-a025-5a00ef4e5a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 372
        },
        {
            "id": "e8c01d6f-7e90-4f1a-97ed-883401134cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 374
        },
        {
            "id": "9542dce6-5d84-41b2-8360-36e8c1a87d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 376
        },
        {
            "id": "cdb5d03f-5986-4206-9c7e-01461fcf4963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 538
        },
        {
            "id": "af45390b-cce6-412e-979b-0d32428e3582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 7786
        },
        {
            "id": "1a4bb64e-b078-48b0-9070-93863988d85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 7808
        },
        {
            "id": "84e4fca4-2c5c-4131-b095-87f450e61b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 7810
        },
        {
            "id": "8f08c204-d2f0-400a-b8f6-bacffeda760c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 7812
        },
        {
            "id": "232cb486-e624-4960-9edf-f20aa6969ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 7922
        },
        {
            "id": "9d269b0d-b947-41c0-ab58-a8cf6661e1e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 84
        },
        {
            "id": "2a435a9f-aea8-49d1-962b-06eb2ff7644c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 86
        },
        {
            "id": "e60f6feb-af86-4eb4-8c47-85046109ed66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 89
        },
        {
            "id": "338e4ac2-b7bd-4350-9b68-7e08ef29d193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 221
        },
        {
            "id": "4d2d7800-6a01-49e2-9174-114c1ef593b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 354
        },
        {
            "id": "b61ffb69-58e7-4d10-bf74-90ea45415a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 356
        },
        {
            "id": "b4409b53-f7b7-4d49-bea3-20384c5341ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 358
        },
        {
            "id": "09e8ea0a-44b5-49aa-9324-c81d060cc448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 374
        },
        {
            "id": "d845616a-1026-4968-bb57-cb192462434f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 376
        },
        {
            "id": "0088b101-8546-4e4e-81e1-f0c884a24734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 538
        },
        {
            "id": "83dbe0e0-f61a-4abf-b143-2fb3369ee9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 7786
        },
        {
            "id": "52e7ce5e-2ee7-49a0-bd6e-fcfa4ff80926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 7922
        },
        {
            "id": "e3cddcea-0be5-447b-b737-f1c7ea808e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 65
        },
        {
            "id": "b8dcfb9a-6420-426d-87ec-2194507745db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 84
        },
        {
            "id": "bfff7982-960e-44c1-9885-5baed61e4d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 86
        },
        {
            "id": "e91dd9ed-0981-4426-9603-5ca2edbe07ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 88
        },
        {
            "id": "f734e668-a2d6-467f-8600-2dc54aa62718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 89
        },
        {
            "id": "9f2b445f-a994-4c52-964d-8f42f811f8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 192
        },
        {
            "id": "8ced4bdd-a5d5-495a-97dc-027c600e6596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 193
        },
        {
            "id": "200bb61f-9188-4219-bb44-1ddeab2c8025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 194
        },
        {
            "id": "0ad16178-09b2-482a-8374-a8dd52c60db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 195
        },
        {
            "id": "44ef5177-e845-463c-b873-15556cd8028d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 196
        },
        {
            "id": "1c3fdccd-bf83-43f2-bb96-833ebc77ba73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 197
        },
        {
            "id": "fdb17275-e217-4ae5-a302-3200d087d080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 198
        },
        {
            "id": "29b9f7db-498c-4c0f-8da7-3c0b7a1e41db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 221
        },
        {
            "id": "c2f2e64e-8dee-4d2c-ac72-1ce6874d0216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 256
        },
        {
            "id": "d52fa026-5ca4-44c3-b82b-b969af47e0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 258
        },
        {
            "id": "a021b020-c49c-4bf9-a90e-9287a0f138e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 260
        },
        {
            "id": "2f4a48c4-d6b3-49dd-a913-2815d644ba10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 354
        },
        {
            "id": "7837747e-2a09-4f3d-afe5-baa4db3fb980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 356
        },
        {
            "id": "c81c0b34-71fa-47fa-bfab-420e369d6732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 358
        },
        {
            "id": "7a932e69-f93d-4105-8270-cd80d79e44a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 374
        },
        {
            "id": "1cd6b396-d70b-4862-a3b6-37d8c2eec932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 376
        },
        {
            "id": "60284779-f815-460b-b674-cce36e1abd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 506
        },
        {
            "id": "8afd7138-5971-4b27-82aa-c53d29ee2f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 508
        },
        {
            "id": "2eccc083-0bbe-440e-b408-d7876f708135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 538
        },
        {
            "id": "b9c0564e-cb46-4a2f-a74c-1cb2eb44bb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 7786
        },
        {
            "id": "f162115b-fe25-41f5-a42d-2aa94b675464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 7922
        },
        {
            "id": "b10ee636-ddeb-41b9-b43a-fa420203bf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 65
        },
        {
            "id": "cb46bfc1-8dae-41b9-b4db-ff49b8a99b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 84
        },
        {
            "id": "3f4d5994-909c-48b4-b639-715b1468703f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 86
        },
        {
            "id": "1aa05057-9dd3-455c-a232-7d676c9202c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 89
        },
        {
            "id": "3b0ce43a-3708-4ef9-9734-28d056308963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 192
        },
        {
            "id": "b5e1a393-d151-4759-a0af-51387586664b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 193
        },
        {
            "id": "26d83f56-67d6-4ca9-9cf4-acceb19ce1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 194
        },
        {
            "id": "b910d783-ccc9-44e1-a014-618c07c27797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 195
        },
        {
            "id": "2314e4e7-bcca-4e15-b7bc-92d9ad2aa91c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 196
        },
        {
            "id": "e432aaad-15bd-46e3-9053-0a7a3d73d2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 197
        },
        {
            "id": "5727d3c8-75e1-4351-af1a-813c756c195d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 198
        },
        {
            "id": "1379e6c0-9ecf-4741-8024-2a98677b3bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 221
        },
        {
            "id": "ac6b60a3-6a22-4ec4-a660-e6a0bc946770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 256
        },
        {
            "id": "c7fb5ce0-0a03-49af-ad6e-5e7bcdc080c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 258
        },
        {
            "id": "da6bcf21-079c-47c9-9795-ec1b345bfd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 260
        },
        {
            "id": "bc682194-87c5-4a19-be99-2f5afe6d92d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 354
        },
        {
            "id": "f5bc9987-5f27-405f-bc0d-9aad28e4383f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 356
        },
        {
            "id": "ed727879-7687-4560-86ce-6a9279ebbfd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 358
        },
        {
            "id": "88e6db30-dc27-4e1a-93e5-b329fb7e22bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 374
        },
        {
            "id": "838edeef-952a-47cf-950e-0cc1f46c5394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 376
        },
        {
            "id": "403d3b97-016f-4122-b7b1-022a0854396e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 506
        },
        {
            "id": "765aa07f-a5fa-4af4-9db9-751e127c236c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 508
        },
        {
            "id": "12aba8db-c683-4261-800c-e82b4077a143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 538
        },
        {
            "id": "88e6e150-a27f-4457-a0ba-e16a5caae3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 7786
        },
        {
            "id": "5073bdf3-b07c-4050-b154-83cd27c4cdb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 7922
        },
        {
            "id": "f0215e02-6a51-4d66-8426-497132860257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 65
        },
        {
            "id": "000dd0b0-8343-4363-b271-36ab3583ae4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 84
        },
        {
            "id": "21e3a6d0-bd11-4b04-a7bf-fe32c715c7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 86
        },
        {
            "id": "5cc08d8b-c416-4f7d-97d6-916089df2fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 88
        },
        {
            "id": "935c98fe-1252-4e39-8048-92bdc01c1318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 89
        },
        {
            "id": "c2d360e5-0a08-459a-bcf4-d7efe0964564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 192
        },
        {
            "id": "a9728cd8-3122-4e0e-9eff-0a75e9130c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 193
        },
        {
            "id": "5ef1cb6f-6aba-4599-b06a-d7bcd07f0e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 194
        },
        {
            "id": "4c5d6bb9-2293-4caf-b845-f73af408a0a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 195
        },
        {
            "id": "42ae3d7e-0f90-4a40-89b4-346a257c006c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 196
        },
        {
            "id": "da85584f-74b3-4ddd-bd80-c7f8b04d7d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 197
        },
        {
            "id": "566e032c-60d1-4460-84f2-4c52b82f285a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 198
        },
        {
            "id": "709e4e24-ae0e-43e1-84c0-e8c25df65f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 221
        },
        {
            "id": "97772d6a-b313-4331-925d-4e528121a021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 256
        },
        {
            "id": "4f90b327-3272-4466-8f9c-4055d3adc77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 258
        },
        {
            "id": "67c74106-c6e2-40b1-b9d7-512e8e8e008a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 260
        },
        {
            "id": "35cc233e-03c5-4f84-b807-3bf6d523e702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 354
        },
        {
            "id": "9d82ef41-d2a7-45ea-8794-731c8b7c8b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 356
        },
        {
            "id": "50f57230-b619-49ba-b477-e81fc9312193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 358
        },
        {
            "id": "601e5174-eaa8-432e-87f2-068744136778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 374
        },
        {
            "id": "3ee147b6-363a-463a-b73a-a3b50a738a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 376
        },
        {
            "id": "e4e59b3c-9b21-422f-96ad-2b333477be04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 506
        },
        {
            "id": "f0af0728-5eda-4aee-a075-914225d2a4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 508
        },
        {
            "id": "10e4003c-4716-469a-9477-54a097c0f0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 538
        },
        {
            "id": "09214194-872d-4637-8149-ea98aef18b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 7786
        },
        {
            "id": "26cc6363-7443-439e-863b-e1b933f631e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 7922
        },
        {
            "id": "2f7dd9b2-6059-469d-bc73-a14d9a91f331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 84
        },
        {
            "id": "65e121a4-af5d-4935-b930-3393857480e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 86
        },
        {
            "id": "bfa1be67-7e9d-4e12-8bdf-ce2cf58ff5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 89
        },
        {
            "id": "33743346-60cd-4f1d-99cd-594d62002c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 221
        },
        {
            "id": "22be5a6d-b16d-4331-9ebb-7e40f92be396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 354
        },
        {
            "id": "22959be7-615f-4e34-b530-463213183103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 356
        },
        {
            "id": "c7ca89aa-0198-4f99-b050-71c041ef7f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 358
        },
        {
            "id": "8f730c9c-a93c-4fdf-9e91-1e261dd75f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 374
        },
        {
            "id": "7df453aa-70b2-4a7a-b1a7-f414c491955b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 376
        },
        {
            "id": "114ab787-82a4-44dc-9c8d-2cac8ea59b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 538
        },
        {
            "id": "dffe9104-632c-44ce-aa46-00a18ae8c2a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 7786
        },
        {
            "id": "01586b45-3add-4ae2-b1d8-93384d928560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 7922
        },
        {
            "id": "3f9071bf-f997-44d9-ab1e-be9e1de45542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 65
        },
        {
            "id": "4ba6814f-2583-4bc9-8b97-ddd899ac2316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 99
        },
        {
            "id": "781b49ea-7376-45b0-a69f-1a79c22e6525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 100
        },
        {
            "id": "83db5431-92f0-4cea-9870-c92785140626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 101
        },
        {
            "id": "12fec0cd-b2a0-411a-a22b-82bf49907a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 103
        },
        {
            "id": "c7e5f686-b15e-4345-acdb-f75f88360824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 106
        },
        {
            "id": "2d3d7cc7-db2d-47f4-a1f3-b65ff9358f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 111
        },
        {
            "id": "51ddd912-d61b-47f0-bb26-bdf86b58fd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 113
        },
        {
            "id": "a3fd715e-786a-49f1-a857-94e3e179ef40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 192
        },
        {
            "id": "b683bd32-edf0-473b-a023-72e424b16aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 193
        },
        {
            "id": "c3244708-fe05-4ba6-9103-bf76fc088b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 194
        },
        {
            "id": "4718f816-982a-4644-baec-66ab47ce93bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 195
        },
        {
            "id": "6f63989b-2535-48c5-8871-9ece5874d486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 196
        },
        {
            "id": "91f49ae6-acb3-42fb-ae48-bc596c4213ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 197
        },
        {
            "id": "9d9099e1-9e4b-46e4-a89a-9264b5941f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 198
        },
        {
            "id": "7bc8d8f3-63b1-4ef5-bc2a-626eee683487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 231
        },
        {
            "id": "c1ba6d72-1d64-43b5-b7e2-3074983e3e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 232
        },
        {
            "id": "9cbfce80-edb0-4177-a9ce-aa32302c47a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 233
        },
        {
            "id": "68438442-bd4a-4a94-8a6d-72116f76ef43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 234
        },
        {
            "id": "0fd1fedc-8eb9-45e8-a802-c7e6112725e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 235
        },
        {
            "id": "e677bc66-a4e3-48c6-a540-9801f5700ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 242
        },
        {
            "id": "05c963b9-97c3-44ec-af29-d94780426507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 243
        },
        {
            "id": "b9e96254-8114-4451-b242-cf80eca31394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 244
        },
        {
            "id": "b6493634-3679-4616-9ea3-48ec897a5f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 245
        },
        {
            "id": "b5fe2b9f-7251-49e8-b453-725aae59b872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 246
        },
        {
            "id": "340b3f7c-dc61-4f59-91a2-a76c0116f5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 248
        },
        {
            "id": "c081335c-17e9-4c3f-b70f-839e76d7c5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 256
        },
        {
            "id": "86324689-bbf1-40b3-a8cf-a221e89533a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 258
        },
        {
            "id": "9109bb33-d48d-46c0-b9df-ab649a5c1bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 260
        },
        {
            "id": "18a585d3-8515-4612-bc4f-da7e13239736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 263
        },
        {
            "id": "e52d9b23-55ff-4353-a355-10e560148a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 265
        },
        {
            "id": "b80f9e39-488e-4815-9c9f-b6984bcb62de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 267
        },
        {
            "id": "e10e3bd1-fd50-4705-af41-2624e76a6625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 269
        },
        {
            "id": "2c6ff576-c8f3-4e1c-8a6e-fa13abc02682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 271
        },
        {
            "id": "76431daa-f292-49bc-aa59-18af6dc0d64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 273
        },
        {
            "id": "177c42ef-285e-454e-9c5e-d983a433e66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 275
        },
        {
            "id": "23626abf-87c9-4d25-bb79-27c9bb2da675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 277
        },
        {
            "id": "61ad94ff-d2e1-43ff-8aa2-36d88622e97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 279
        },
        {
            "id": "55c74f2c-4458-402a-9bf4-8b92c6170ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 281
        },
        {
            "id": "2e812e41-9f36-4d2e-b193-f38b842f817c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 283
        },
        {
            "id": "91fd044d-5c97-4415-a157-b2ebf18b1bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 285
        },
        {
            "id": "4e9975e4-72d6-4e67-be1a-6f6f69969271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 287
        },
        {
            "id": "2753892e-7b7a-4ce4-9b27-7cd8e63b77b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 289
        },
        {
            "id": "e010c44d-358d-4ec7-a031-cf3bed7ce2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 333
        },
        {
            "id": "0b160c5f-1209-477a-b250-2cee420c3255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 335
        },
        {
            "id": "7f9c2051-a2a8-4554-973c-65b36b27ce94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 337
        },
        {
            "id": "fdac8ed0-3377-4be3-805b-9455e96dcf8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 339
        },
        {
            "id": "270e9f4d-53ef-4a92-89fd-10ffdba597c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 506
        },
        {
            "id": "42f20b62-7061-476a-aab4-0850d449992d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 508
        },
        {
            "id": "13f8f629-bcb9-4237-a7be-ce197db73cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 511
        },
        {
            "id": "59c0b041-5230-4177-ab4c-a94f6da1fae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 913
        },
        {
            "id": "4c77eefd-7db4-4fd4-8908-31af31a7a7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 916
        },
        {
            "id": "fa6f63fd-4b7e-4b8e-9d6d-fd877e0497f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 923
        },
        {
            "id": "8ef415f5-568f-406c-9262-66f3538aa644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 940
        },
        {
            "id": "cacd3e8a-2933-4635-8f54-205e4f7d3128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 941
        },
        {
            "id": "015bec33-64b6-4c19-aa9c-690889d5a1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 945
        },
        {
            "id": "1bd8143b-b532-4c9f-b0d3-32867294d376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 949
        },
        {
            "id": "1f3a2769-dc0c-4e8d-b79a-425106e8cf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 959
        },
        {
            "id": "5c7ea779-81b2-4ca3-aedb-f65e6494cc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 962
        },
        {
            "id": "3decf0b0-d73f-4325-a9c0-7514a0ba1b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 963
        },
        {
            "id": "c86d5ea6-f714-4192-9efd-c8d3a60dc693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 972
        },
        {
            "id": "7960bd96-8f3f-48d6-8d8b-b952db7674cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1040
        },
        {
            "id": "aa3fae96-903f-44a1-9b2d-954714519e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1077
        },
        {
            "id": "b6708523-0d4a-4477-af46-b2cc11461515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1086
        },
        {
            "id": "6776df14-0bc6-4e39-bb75-e8b6781b4e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1089
        },
        {
            "id": "f320177c-956b-4196-8ca2-23309b413247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1092
        },
        {
            "id": "93c61f41-0cfd-4505-a1d0-8d53b4fafeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1095
        },
        {
            "id": "a5ca8767-94fd-4cfe-a904-65151470a309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1104
        },
        {
            "id": "e4baebe3-7af9-4bb8-b284-7fc326719f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1105
        },
        {
            "id": "2866b761-a002-4816-8d8b-3a297e8f3da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1108
        },
        {
            "id": "de5157bf-9e10-4df4-9dcd-121cdce9455e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 1112
        },
        {
            "id": "669dc823-a24a-4611-a3b3-d454f5607f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1139
        },
        {
            "id": "03fc37f6-9b24-4cb3-b760-87dec82117a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1193
        },
        {
            "id": "d193c553-3dd8-485e-aad5-76c75ff2d88a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1195
        },
        {
            "id": "886ae9ad-cf09-4c0e-8c98-c94159b948ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1207
        },
        {
            "id": "be0287d5-4b08-4bf2-a556-6ba245a0866a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1209
        },
        {
            "id": "eed21a93-bf42-4c08-8df2-38bbc3c88c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1228
        },
        {
            "id": "9007104d-3e4b-43e0-b9cf-5889e6a32e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1232
        },
        {
            "id": "86a2201c-7db5-423e-9001-4ed771ce903b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1234
        },
        {
            "id": "b93eeba0-8f91-496a-89ba-7c29f62e987b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1236
        },
        {
            "id": "cd8baabb-d06b-4c74-89b6-4c3d96bc03d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1239
        },
        {
            "id": "10c5b546-c35a-41cf-ba6e-0af72ff75248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1241
        },
        {
            "id": "5843f9ec-bcef-4eb6-87bc-deebc0ce3e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1255
        },
        {
            "id": "307d58cf-5469-4492-b171-70b9730484a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1257
        },
        {
            "id": "588515b6-b31d-4554-a52f-bd5b1cbfe14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 1269
        },
        {
            "id": "226acbad-99dc-46e3-b4dd-0c15b6db4642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 123,
            "second": 7691
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}