{
    "id": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTurretShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5263963-6afe-4dc5-a1a0-3d4d03a922e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "compositeImage": {
                "id": "ef26c19a-a388-410d-818c-93aca7e2d3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5263963-6afe-4dc5-a1a0-3d4d03a922e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9d4fc5b-7f4a-4d0e-b7d7-df5d145847cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5263963-6afe-4dc5-a1a0-3d4d03a922e1",
                    "LayerId": "53a09421-504a-460b-b303-a1d682d643f7"
                },
                {
                    "id": "5199e563-8103-4327-afb6-ec839f2d646f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5263963-6afe-4dc5-a1a0-3d4d03a922e1",
                    "LayerId": "425c9b87-3fae-453b-b07b-ca7e00db1735"
                }
            ]
        },
        {
            "id": "f6098ff3-d846-4a79-8729-e1b77ca70430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "compositeImage": {
                "id": "9522e18c-e26a-4269-9275-0e389c4f672b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6098ff3-d846-4a79-8729-e1b77ca70430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddbdff0c-c2e0-471e-861a-2f25439fdb38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6098ff3-d846-4a79-8729-e1b77ca70430",
                    "LayerId": "53a09421-504a-460b-b303-a1d682d643f7"
                },
                {
                    "id": "75eee38d-6fee-4a73-b9f8-6337d75103a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6098ff3-d846-4a79-8729-e1b77ca70430",
                    "LayerId": "425c9b87-3fae-453b-b07b-ca7e00db1735"
                }
            ]
        },
        {
            "id": "27bd0c74-904f-4de7-b57f-4a84a789b73a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "compositeImage": {
                "id": "cc3bb9a5-705f-4bf5-abf0-cdf2ba9dc031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27bd0c74-904f-4de7-b57f-4a84a789b73a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d95540-73e4-48ce-a54f-4dccfabfdcc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27bd0c74-904f-4de7-b57f-4a84a789b73a",
                    "LayerId": "53a09421-504a-460b-b303-a1d682d643f7"
                },
                {
                    "id": "0aa39104-71d5-414d-b01e-8156ec228f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27bd0c74-904f-4de7-b57f-4a84a789b73a",
                    "LayerId": "425c9b87-3fae-453b-b07b-ca7e00db1735"
                }
            ]
        },
        {
            "id": "4441e4b1-2f95-4586-b382-e08ac3dd7682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "compositeImage": {
                "id": "3b941c9f-41f1-4fe3-b4fc-d73164a40306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4441e4b1-2f95-4586-b382-e08ac3dd7682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5573f0a6-0404-4dc8-8ad8-b5dd3d6088d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4441e4b1-2f95-4586-b382-e08ac3dd7682",
                    "LayerId": "53a09421-504a-460b-b303-a1d682d643f7"
                },
                {
                    "id": "145a8e9f-6f17-4b07-bdcb-0d8cffdb2497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4441e4b1-2f95-4586-b382-e08ac3dd7682",
                    "LayerId": "425c9b87-3fae-453b-b07b-ca7e00db1735"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "53a09421-504a-460b-b303-a1d682d643f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "425c9b87-3fae-453b-b07b-ca7e00db1735",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2890a0a-36e4-43b4-b5c6-c75acf00e4ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}