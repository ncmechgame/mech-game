{
    "id": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCyclopsWave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 61,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10ec0f4f-600a-41ed-8970-3d52453a9c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "0e4e76ab-e776-4806-bd35-73989325be6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ec0f4f-600a-41ed-8970-3d52453a9c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70c9690-fcf0-473c-b781-e213fa2d5da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ec0f4f-600a-41ed-8970-3d52453a9c0b",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "698d653e-63d7-409c-ba66-4ef4eab33385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "a8393b2c-1546-45db-8449-df3feac19d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698d653e-63d7-409c-ba66-4ef4eab33385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae50243-124c-4ecd-bd65-cebc5308f282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698d653e-63d7-409c-ba66-4ef4eab33385",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "f531d1d6-1a8c-49c0-b4ec-bf8ed0a7c6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "c45a54c8-ec6d-41bc-b09f-d01712ccf69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f531d1d6-1a8c-49c0-b4ec-bf8ed0a7c6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15be7c8-b1a4-4c90-80ff-2755154dbf03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f531d1d6-1a8c-49c0-b4ec-bf8ed0a7c6b9",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "4c313e0d-89af-4a03-9109-080b7fbf5bdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "b876cc44-4e66-4607-8f78-57de502e1dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c313e0d-89af-4a03-9109-080b7fbf5bdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad72e8e-b1a1-4475-81c1-e3c05508d5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c313e0d-89af-4a03-9109-080b7fbf5bdc",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "5372af66-e509-4d50-aad2-3cfe3ca9970d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "2812bc66-8d44-4ef9-a46b-7e525f79b085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5372af66-e509-4d50-aad2-3cfe3ca9970d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13ef774-28a9-4e31-9d43-0458ab4f5841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5372af66-e509-4d50-aad2-3cfe3ca9970d",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "9e53ae1e-ed44-4d45-8bd6-77f098f846ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "c348dcea-666a-4d5c-b4a8-409bb59a429c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e53ae1e-ed44-4d45-8bd6-77f098f846ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f9073e5-6c45-4afb-b1ef-17723a985aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e53ae1e-ed44-4d45-8bd6-77f098f846ff",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "51a8c13e-7276-43ba-b372-786e316e91da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "cb4fbd50-4fd6-45b1-ae90-6873a71122af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a8c13e-7276-43ba-b372-786e316e91da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1862461e-55ec-45a4-8184-8424f3a50c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a8c13e-7276-43ba-b372-786e316e91da",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "d21d18d0-3c28-408f-a5c1-d9f8e27b9d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "41e4368b-340b-46d2-a267-410f59f4d0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21d18d0-3c28-408f-a5c1-d9f8e27b9d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f19311-a671-41bf-83a9-146db64d8190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21d18d0-3c28-408f-a5c1-d9f8e27b9d06",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "b8a9909c-fcd4-47db-b44b-c27cb3b04188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "d754d1c8-49fd-4783-9213-668ecd7d6705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a9909c-fcd4-47db-b44b-c27cb3b04188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed4b81d-16ad-4db6-ae16-dc84acacfe0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a9909c-fcd4-47db-b44b-c27cb3b04188",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        },
        {
            "id": "220a8dd5-b870-4f24-9fde-5699ef1d09b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "compositeImage": {
                "id": "0ba12bed-ee30-4954-be61-80c26badbdfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220a8dd5-b870-4f24-9fde-5699ef1d09b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b549ce0-5001-4827-a928-a62ade55c071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220a8dd5-b870-4f24-9fde-5699ef1d09b7",
                    "LayerId": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b58a93e2-0b05-471b-9dbd-c2cfdf9e003f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f1d9be5-9d3d-46b7-930d-8b05839c4748",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 17,
    "yorig": 17
}