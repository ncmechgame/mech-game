{
    "id": "f1424299-66db-4076-ae19-b75a2bd4de0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 16,
    "bbox_right": 295,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c41c3501-0290-4efc-aca5-60065e85880a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1424299-66db-4076-ae19-b75a2bd4de0e",
            "compositeImage": {
                "id": "88e11802-531d-4cb6-b7dc-bce4d0c931ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41c3501-0290-4efc-aca5-60065e85880a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28112ce8-c8a6-4334-8c6b-58328c0695cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41c3501-0290-4efc-aca5-60065e85880a",
                    "LayerId": "482ac5c5-d261-4464-9bd0-f5a2c71aa060"
                }
            ]
        },
        {
            "id": "4e73b008-3594-419b-a5af-5d0f3b6e3b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1424299-66db-4076-ae19-b75a2bd4de0e",
            "compositeImage": {
                "id": "fd17b530-3b23-45e4-850e-c3e26c6f3afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e73b008-3594-419b-a5af-5d0f3b6e3b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1f6647f-fcb1-4245-b5e0-dff2a8d0af96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e73b008-3594-419b-a5af-5d0f3b6e3b35",
                    "LayerId": "482ac5c5-d261-4464-9bd0-f5a2c71aa060"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "482ac5c5-d261-4464-9bd0-f5a2c71aa060",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1424299-66db-4076-ae19-b75a2bd4de0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}