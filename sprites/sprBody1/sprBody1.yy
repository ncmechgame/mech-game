{
    "id": "c84f5647-ee97-42d7-92ba-aeeb3bfe0b78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBody1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 13,
    "bbox_right": 61,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "455dc7a7-ccc3-4df9-9905-9af69a285ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c84f5647-ee97-42d7-92ba-aeeb3bfe0b78",
            "compositeImage": {
                "id": "6515c430-0e3f-4b0f-89a9-20ba824554d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455dc7a7-ccc3-4df9-9905-9af69a285ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186aa787-8cce-451c-a9ab-29d2ee367f77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455dc7a7-ccc3-4df9-9905-9af69a285ba8",
                    "LayerId": "0e5850a8-c584-4c73-bd89-1f183637ce27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "0e5850a8-c584-4c73-bd89-1f183637ce27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c84f5647-ee97-42d7-92ba-aeeb3bfe0b78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 37
}