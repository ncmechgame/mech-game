{
    "id": "1548e888-c674-4635-b8cc-78a68e50b2e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8fb4ef1-31df-4357-9a91-6583be676ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1548e888-c674-4635-b8cc-78a68e50b2e3",
            "compositeImage": {
                "id": "50a1077a-a501-433f-b217-82379cbaf362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8fb4ef1-31df-4357-9a91-6583be676ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481dafea-d8b0-433d-9dc8-96a34d76786f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8fb4ef1-31df-4357-9a91-6583be676ad1",
                    "LayerId": "f6e09d8d-3b64-4def-b166-351be174413a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "f6e09d8d-3b64-4def-b166-351be174413a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1548e888-c674-4635-b8cc-78a68e50b2e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}