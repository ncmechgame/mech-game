{
    "id": "a716c514-4e99-48ab-8b44-fdd2dacc339c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMenuBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f92b07f4-5b38-42b2-830f-ee175fc7fcea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a716c514-4e99-48ab-8b44-fdd2dacc339c",
            "compositeImage": {
                "id": "8e8bb31d-b440-447a-a3e0-e2d6a1325633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f92b07f4-5b38-42b2-830f-ee175fc7fcea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "824fd11f-e678-4a63-8cdf-0964e4007829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f92b07f4-5b38-42b2-830f-ee175fc7fcea",
                    "LayerId": "934edf7e-a731-4480-9c82-35eac789b4a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "934edf7e-a731-4480-9c82-35eac789b4a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a716c514-4e99-48ab-8b44-fdd2dacc339c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}