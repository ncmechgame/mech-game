{
    "id": "d7ad8401-67f2-4a55-aba3-3e3beba2514c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSmoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15c7fc03-ea31-4a09-970b-6f03f4442f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7ad8401-67f2-4a55-aba3-3e3beba2514c",
            "compositeImage": {
                "id": "4bf7c0ce-6f57-4417-8ce7-832035592c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c7fc03-ea31-4a09-970b-6f03f4442f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c179a57e-2dcf-4fbc-8dd7-56d1661f4b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c7fc03-ea31-4a09-970b-6f03f4442f20",
                    "LayerId": "e9889667-606f-41bc-b050-188d3ebfb8b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e9889667-606f-41bc-b050-188d3ebfb8b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7ad8401-67f2-4a55-aba3-3e3beba2514c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 31,
    "yorig": 32
}