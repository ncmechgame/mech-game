{
    "id": "58ece736-8b1e-40b2-a5be-2525ede52870",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFeet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "726d6677-feef-4f92-96c6-c56fc5d29355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "a80fbf13-c52e-46c8-bd8a-475139bbfa27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "726d6677-feef-4f92-96c6-c56fc5d29355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9905431-c236-443e-aa07-8bec6e7bb89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "726d6677-feef-4f92-96c6-c56fc5d29355",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "d099093d-5f87-467e-8d3f-272d8ecbd79d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "b596f126-63e3-4426-ba86-2364f76ab687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d099093d-5f87-467e-8d3f-272d8ecbd79d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb934da-5bb1-4871-b35d-e4bc66fe8102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d099093d-5f87-467e-8d3f-272d8ecbd79d",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "32630fa3-f271-46ad-bd43-5b5b9f54a9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "8c5fb140-7e90-4ae1-84b4-1498dfab597c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32630fa3-f271-46ad-bd43-5b5b9f54a9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3a9543-57a5-4b21-a4a0-245306c3293d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32630fa3-f271-46ad-bd43-5b5b9f54a9d2",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "f9329c7e-2336-4c68-9189-a95d5b43e6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "cc047bd6-e42c-4480-be63-0af3cca11357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9329c7e-2336-4c68-9189-a95d5b43e6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367aab3a-cf81-4987-8f47-4e20a4cdcf16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9329c7e-2336-4c68-9189-a95d5b43e6b9",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "f54d4297-5e18-442a-970f-3a43d56ee3cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "827ccc53-3fb1-443b-93a4-5609c680d809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54d4297-5e18-442a-970f-3a43d56ee3cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef545a21-08ba-4226-9db0-daa1257366bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54d4297-5e18-442a-970f-3a43d56ee3cd",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "a14dd3b6-54ca-4ca9-bd4e-6e4fe7d2eae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "657cc8cf-f63d-4570-9bc7-c1b5a230cffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14dd3b6-54ca-4ca9-bd4e-6e4fe7d2eae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b06abcc-7799-4387-ab02-dc5bf1e994f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14dd3b6-54ca-4ca9-bd4e-6e4fe7d2eae7",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "5950ea67-558d-452f-a4a1-ec786d0819b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "5785ae85-8e19-4c58-b7d8-9de9219a4a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5950ea67-558d-452f-a4a1-ec786d0819b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f1d322-27a4-405b-b1a6-c040f0a0a412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5950ea67-558d-452f-a4a1-ec786d0819b9",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        },
        {
            "id": "9ab44ae7-71af-4cf9-9968-78316723dbb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "compositeImage": {
                "id": "c086cca7-16d7-4feb-8ed9-0ba6a7539d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab44ae7-71af-4cf9-9968-78316723dbb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc374e51-cd41-4ea6-a9f0-9de1cf8be70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab44ae7-71af-4cf9-9968-78316723dbb7",
                    "LayerId": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "013e5b68-b201-4f78-9d7c-3e0d9c3f081f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58ece736-8b1e-40b2-a5be-2525ede52870",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}