{
    "id": "0223a30e-1df5-4098-8e95-c322c4537e49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTurretBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b3c0b4e-81fd-4725-ac71-b399455abb6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0223a30e-1df5-4098-8e95-c322c4537e49",
            "compositeImage": {
                "id": "96b8517e-83f7-4fb4-b1c1-ea8e01f6768f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b3c0b4e-81fd-4725-ac71-b399455abb6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5ef8f0-0756-4cf9-b6fc-e06b357a78b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3c0b4e-81fd-4725-ac71-b399455abb6f",
                    "LayerId": "3abf9783-d134-44a2-ac6f-8bed5d56f2ee"
                },
                {
                    "id": "44d235c4-8b3a-4751-b10a-bd764b04102d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3c0b4e-81fd-4725-ac71-b399455abb6f",
                    "LayerId": "d475fef7-9c12-4103-a8ec-95375695b4c6"
                },
                {
                    "id": "0f267a30-f2e5-49be-bb48-c881b796f482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3c0b4e-81fd-4725-ac71-b399455abb6f",
                    "LayerId": "200ca25e-e3ad-44fd-835d-01a90dad7b38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "200ca25e-e3ad-44fd-835d-01a90dad7b38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0223a30e-1df5-4098-8e95-c322c4537e49",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3abf9783-d134-44a2-ac6f-8bed5d56f2ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0223a30e-1df5-4098-8e95-c322c4537e49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d475fef7-9c12-4103-a8ec-95375695b4c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0223a30e-1df5-4098-8e95-c322c4537e49",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}