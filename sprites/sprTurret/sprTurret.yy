{
    "id": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTurret",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c0c7f91-51d4-4fb9-900b-add34d450346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
            "compositeImage": {
                "id": "6d72a806-4c95-4e52-91bb-ccd337b8c632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0c7f91-51d4-4fb9-900b-add34d450346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e177fa-e48a-4523-abfa-2e59660205b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0c7f91-51d4-4fb9-900b-add34d450346",
                    "LayerId": "948370ee-872b-4c9c-8635-e5ec8785a6f6"
                },
                {
                    "id": "d371558c-6cd7-475d-8ee5-02c7b6ae6f7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0c7f91-51d4-4fb9-900b-add34d450346",
                    "LayerId": "b694bd1d-307a-4af7-a08f-5918040d6cca"
                }
            ]
        },
        {
            "id": "cd347cc0-2ae9-405e-8cff-2bfbbadf23b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
            "compositeImage": {
                "id": "b466943c-b95b-43f2-9807-62771ee075af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd347cc0-2ae9-405e-8cff-2bfbbadf23b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1861db0f-98bc-40ab-9a04-1f411c22aabb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd347cc0-2ae9-405e-8cff-2bfbbadf23b9",
                    "LayerId": "b694bd1d-307a-4af7-a08f-5918040d6cca"
                },
                {
                    "id": "b7cefff2-aea3-486e-8cef-fb590ad66ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd347cc0-2ae9-405e-8cff-2bfbbadf23b9",
                    "LayerId": "948370ee-872b-4c9c-8635-e5ec8785a6f6"
                }
            ]
        },
        {
            "id": "0a5ed76c-024b-42bf-b547-a5b71fb6651a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
            "compositeImage": {
                "id": "215b919c-e13c-48b9-9ed1-42a95c2ed687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5ed76c-024b-42bf-b547-a5b71fb6651a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e447d303-1495-4275-ae28-caf11a395042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5ed76c-024b-42bf-b547-a5b71fb6651a",
                    "LayerId": "b694bd1d-307a-4af7-a08f-5918040d6cca"
                },
                {
                    "id": "c70f91b7-6646-4bcd-9ab3-f8c8275804f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5ed76c-024b-42bf-b547-a5b71fb6651a",
                    "LayerId": "948370ee-872b-4c9c-8635-e5ec8785a6f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b694bd1d-307a-4af7-a08f-5918040d6cca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "948370ee-872b-4c9c-8635-e5ec8785a6f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4dac3f2-68cf-406e-b7aa-1a801d6f25d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}