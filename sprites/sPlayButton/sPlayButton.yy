{
    "id": "515825c4-1c98-41c1-a70b-5ae1283f3d44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86c5ebc9-b95e-4a84-9275-14802d1ce6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515825c4-1c98-41c1-a70b-5ae1283f3d44",
            "compositeImage": {
                "id": "a6c30e4f-f576-431f-94c9-2069af8f3a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86c5ebc9-b95e-4a84-9275-14802d1ce6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53ba00a7-3925-4aae-a0bd-369b9512a03f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86c5ebc9-b95e-4a84-9275-14802d1ce6d2",
                    "LayerId": "7e74f01d-b141-4635-a44f-99bf60456945"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "7e74f01d-b141-4635-a44f-99bf60456945",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "515825c4-1c98-41c1-a70b-5ae1283f3d44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 0,
    "yorig": 0
}