{
    "id": "de8277e3-943b-486f-95d9-e085eb6c9e38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 121,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51d38106-1a16-4140-b553-ed727e6ae7f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de8277e3-943b-486f-95d9-e085eb6c9e38",
            "compositeImage": {
                "id": "f9fd7eb8-ff5a-464f-8d35-e51c9645b7ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d38106-1a16-4140-b553-ed727e6ae7f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16e25a9-10a5-4288-8bf0-2ade0a944d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d38106-1a16-4140-b553-ed727e6ae7f3",
                    "LayerId": "18596b06-bf7f-403a-a45e-2e7e231b94eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "18596b06-bf7f-403a-a45e-2e7e231b94eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de8277e3-943b-486f-95d9-e085eb6c9e38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}