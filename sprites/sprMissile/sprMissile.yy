{
    "id": "b9a8c8d7-7496-43cf-94e1-2ef3ecd2fb2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMissile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35bcf608-13e5-46e1-8072-0c093a1b05c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9a8c8d7-7496-43cf-94e1-2ef3ecd2fb2d",
            "compositeImage": {
                "id": "442d97e5-3ad7-4172-8f5e-15f2886dbafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bcf608-13e5-46e1-8072-0c093a1b05c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474bb2c8-c495-4227-8de4-678d2387e521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bcf608-13e5-46e1-8072-0c093a1b05c9",
                    "LayerId": "9cfef69e-400d-4207-a507-9987bcf29fac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9cfef69e-400d-4207-a507-9987bcf29fac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9a8c8d7-7496-43cf-94e1-2ef3ecd2fb2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 4
}