{
    "id": "55e0e62a-d9d1-4f78-bd4f-08a1209b4ef4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMissile2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2eaa739-81e1-40aa-a401-3645eeec8960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55e0e62a-d9d1-4f78-bd4f-08a1209b4ef4",
            "compositeImage": {
                "id": "28bff50c-fcc7-4cd0-b6cb-ee6de3e02cf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2eaa739-81e1-40aa-a401-3645eeec8960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dde73ee-0ffd-485c-b058-a91b5472bf90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2eaa739-81e1-40aa-a401-3645eeec8960",
                    "LayerId": "3d0b7d40-c80b-40c6-b94b-2fe939d864dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3d0b7d40-c80b-40c6-b94b-2fe939d864dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55e0e62a-d9d1-4f78-bd4f-08a1209b4ef4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 4
}