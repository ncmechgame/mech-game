{
    "id": "59086787-9b60-4597-b9b4-1884e13940cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtRing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb16929-28d8-43ba-8e76-086d1f20a8d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59086787-9b60-4597-b9b4-1884e13940cc",
            "compositeImage": {
                "id": "dbd3ef21-b6bb-407e-a22b-f3a4a0772558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb16929-28d8-43ba-8e76-086d1f20a8d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318eb52b-f82d-47d8-aa12-305c4d1861e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb16929-28d8-43ba-8e76-086d1f20a8d7",
                    "LayerId": "ee6eb168-2c88-4212-8604-f95cd3fc7d89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "ee6eb168-2c88-4212-8604-f95cd3fc7d89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59086787-9b60-4597-b9b4-1884e13940cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}