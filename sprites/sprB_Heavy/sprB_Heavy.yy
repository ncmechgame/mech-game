{
    "id": "559c04e4-976d-4a37-9007-3600f96f16f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Heavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d44e950-e1e3-4906-ac20-4500c3dbdc89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "559c04e4-976d-4a37-9007-3600f96f16f6",
            "compositeImage": {
                "id": "82f3e74f-1edb-477a-91e8-8fbb01d9b127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d44e950-e1e3-4906-ac20-4500c3dbdc89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d8359f-89d4-4794-9bf8-436f137c7048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d44e950-e1e3-4906-ac20-4500c3dbdc89",
                    "LayerId": "bc4d42e3-5f7e-4b9f-a073-f3a30ea388c6"
                }
            ]
        },
        {
            "id": "b29d7e4b-b770-4b1b-86b0-b4b55cb601a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "559c04e4-976d-4a37-9007-3600f96f16f6",
            "compositeImage": {
                "id": "16810e0c-1a8e-48b0-85b3-12f3e767196a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29d7e4b-b770-4b1b-86b0-b4b55cb601a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ea5868-5a37-4595-94b1-8440e2660afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29d7e4b-b770-4b1b-86b0-b4b55cb601a8",
                    "LayerId": "bc4d42e3-5f7e-4b9f-a073-f3a30ea388c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc4d42e3-5f7e-4b9f-a073-f3a30ea388c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "559c04e4-976d-4a37-9007-3600f96f16f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}