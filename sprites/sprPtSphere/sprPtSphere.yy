{
    "id": "b031779d-2aa5-4c33-9f76-fcaabcc9f4a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSphere",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af030375-7f3b-4ecb-b5cd-359f2fa90fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b031779d-2aa5-4c33-9f76-fcaabcc9f4a5",
            "compositeImage": {
                "id": "130b0981-f686-40a0-985d-9c7743d53988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af030375-7f3b-4ecb-b5cd-359f2fa90fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333acffa-9ce7-4f1a-8d57-1ef44550fc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af030375-7f3b-4ecb-b5cd-359f2fa90fc4",
                    "LayerId": "1a1aa335-7fd5-45bf-a9fe-aab22753ae46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "1a1aa335-7fd5-45bf-a9fe-aab22753ae46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b031779d-2aa5-4c33-9f76-fcaabcc9f4a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}