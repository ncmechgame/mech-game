{
    "id": "e73f5602-d702-4c5d-96db-2855c3d81aa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMissileBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74802732-ddc4-4ea2-9da5-9a637d6d899c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73f5602-d702-4c5d-96db-2855c3d81aa4",
            "compositeImage": {
                "id": "efaa1523-9050-4e2f-8faf-f901ee95bfc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74802732-ddc4-4ea2-9da5-9a637d6d899c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a1d4a8-a328-4b2d-8836-b233b84a4928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74802732-ddc4-4ea2-9da5-9a637d6d899c",
                    "LayerId": "9300d241-b1a6-4463-b7c3-6050c2591e57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "9300d241-b1a6-4463-b7c3-6050c2591e57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e73f5602-d702-4c5d-96db-2855c3d81aa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 3
}