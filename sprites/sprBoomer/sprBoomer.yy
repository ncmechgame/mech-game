{
    "id": "1e9fc501-64b9-495d-a569-4f7687474649",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBoomer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25f43c6f-5cea-4d40-ada0-f9c420f1c0b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9fc501-64b9-495d-a569-4f7687474649",
            "compositeImage": {
                "id": "8736402b-059e-4512-967f-808e1c180d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f43c6f-5cea-4d40-ada0-f9c420f1c0b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb94599f-18dd-4ac9-9a40-b98b02e05aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f43c6f-5cea-4d40-ada0-f9c420f1c0b4",
                    "LayerId": "ad98724f-f976-4cb1-9473-4c7920412a4e"
                }
            ]
        },
        {
            "id": "aaee4e54-1e56-44aa-9812-ee457be510ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9fc501-64b9-495d-a569-4f7687474649",
            "compositeImage": {
                "id": "da661517-8ffa-4b0f-b485-66f7ebc87453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaee4e54-1e56-44aa-9812-ee457be510ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "875e0b40-0738-4d17-a229-c0e772a9b5b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaee4e54-1e56-44aa-9812-ee457be510ee",
                    "LayerId": "ad98724f-f976-4cb1-9473-4c7920412a4e"
                }
            ]
        },
        {
            "id": "4169bd5d-867a-404e-bc03-c5017e7e0d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9fc501-64b9-495d-a569-4f7687474649",
            "compositeImage": {
                "id": "d9248651-2104-4424-8209-f374b546cd19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4169bd5d-867a-404e-bc03-c5017e7e0d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9b3e7a-a6be-4f65-b845-12f25a3f4a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4169bd5d-867a-404e-bc03-c5017e7e0d45",
                    "LayerId": "ad98724f-f976-4cb1-9473-4c7920412a4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ad98724f-f976-4cb1-9473-4c7920412a4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e9fc501-64b9-495d-a569-4f7687474649",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 10
}