{
    "id": "95dbef65-cc71-46fc-92b6-a47b28bbf6a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76c775b1-d7f7-44c0-a331-f33ad8743141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95dbef65-cc71-46fc-92b6-a47b28bbf6a3",
            "compositeImage": {
                "id": "5a933727-ac3e-4b39-aae2-3c8fcee12ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c775b1-d7f7-44c0-a331-f33ad8743141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0acd5f7-151a-413d-b263-29c8b1338992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c775b1-d7f7-44c0-a331-f33ad8743141",
                    "LayerId": "52f33634-f02b-4087-ad78-a5f5f7b0f0ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "52f33634-f02b-4087-ad78-a5f5f7b0f0ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95dbef65-cc71-46fc-92b6-a47b28bbf6a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 63,
    "xorig": 31,
    "yorig": 31
}