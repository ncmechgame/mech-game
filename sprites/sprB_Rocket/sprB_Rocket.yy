{
    "id": "b4521b58-df2d-48be-82c4-f4546c221308",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Rocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dbbece5-868c-47be-92c6-44ef4f928c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
            "compositeImage": {
                "id": "03aab189-a04b-42aa-be32-1fbb68ff3cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dbbece5-868c-47be-92c6-44ef4f928c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f2dc8a4-5763-48a9-a18d-0353b4910b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dbbece5-868c-47be-92c6-44ef4f928c5f",
                    "LayerId": "844e828e-112f-46a9-b87b-243471dface9"
                }
            ]
        },
        {
            "id": "c4e4aa37-277b-440e-b93f-0d960950d99c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
            "compositeImage": {
                "id": "6008d1e8-1d77-4ab1-9771-b03fae3c65af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e4aa37-277b-440e-b93f-0d960950d99c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd58633e-8277-4ebb-ad6f-dad32ff482b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e4aa37-277b-440e-b93f-0d960950d99c",
                    "LayerId": "844e828e-112f-46a9-b87b-243471dface9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "844e828e-112f-46a9-b87b-243471dface9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4521b58-df2d-48be-82c4-f4546c221308",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}