{
    "id": "054cfb2f-7b20-4021-addf-f4db220755a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCannon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "026df939-914c-4969-82e3-8b4b7ffb2b01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "054cfb2f-7b20-4021-addf-f4db220755a4",
            "compositeImage": {
                "id": "5dfff17a-f4d0-4174-8921-b7c028e1dfde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026df939-914c-4969-82e3-8b4b7ffb2b01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de73e440-e391-4742-ae1a-a1b0912c461d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026df939-914c-4969-82e3-8b4b7ffb2b01",
                    "LayerId": "cb47027b-2b8e-4c71-96e2-539936a4967d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "cb47027b-2b8e-4c71-96e2-539936a4967d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "054cfb2f-7b20-4021-addf-f4db220755a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 5
}