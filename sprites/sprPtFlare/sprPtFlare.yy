{
    "id": "b5ab6133-9fa8-40d9-975e-163b08c5ea94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtFlare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf0128ed-6c88-482c-86ef-c48375b354ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5ab6133-9fa8-40d9-975e-163b08c5ea94",
            "compositeImage": {
                "id": "c5d717f5-e1bc-4e6d-81fc-30e40bd3f7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0128ed-6c88-482c-86ef-c48375b354ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83874ef-656f-412f-a762-93b2efff7687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0128ed-6c88-482c-86ef-c48375b354ea",
                    "LayerId": "82675a0e-f713-4974-a561-f8c0bf8668c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "82675a0e-f713-4974-a561-f8c0bf8668c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5ab6133-9fa8-40d9-975e-163b08c5ea94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}