{
    "id": "192759f5-0414-438e-acad-59f1150ea2f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSpark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c724bbeb-06c3-4dbf-835c-33661ab428ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "192759f5-0414-438e-acad-59f1150ea2f2",
            "compositeImage": {
                "id": "955e6239-ab2b-4c0f-bbc1-2df57c88a038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c724bbeb-06c3-4dbf-835c-33661ab428ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7364f93-614f-45cb-9589-6797a8a37a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c724bbeb-06c3-4dbf-835c-33661ab428ff",
                    "LayerId": "aaa13278-8d53-4d56-ae14-15ce8cad4a67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "aaa13278-8d53-4d56-ae14-15ce8cad4a67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "192759f5-0414-438e-acad-59f1150ea2f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 27
}