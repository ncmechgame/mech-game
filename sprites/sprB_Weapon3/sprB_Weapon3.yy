{
    "id": "bf2e08b4-73f5-45dd-bc28-354a7723011f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Weapon3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdf8ffd1-9915-44ba-b720-ab75a9b93dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf2e08b4-73f5-45dd-bc28-354a7723011f",
            "compositeImage": {
                "id": "c834bb26-1028-42c0-8b02-e8d578fb9442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf8ffd1-9915-44ba-b720-ab75a9b93dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fbc9c81-2d37-4fa7-84d5-66a11e42585f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf8ffd1-9915-44ba-b720-ab75a9b93dd6",
                    "LayerId": "9927aa40-2a1a-489f-b9a1-1fd8193775df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9927aa40-2a1a-489f-b9a1-1fd8193775df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf2e08b4-73f5-45dd-bc28-354a7723011f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}