{
    "id": "f41a7865-d383-43ec-b3a0-6a07fbdb58b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCustomizationMockUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 707,
    "bbox_left": 32,
    "bbox_right": 990,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ff178e4-2e57-4ef5-b6ff-e0b1c52497eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a7865-d383-43ec-b3a0-6a07fbdb58b2",
            "compositeImage": {
                "id": "b18e4419-2383-4867-bfe4-8eb4d27e4245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff178e4-2e57-4ef5-b6ff-e0b1c52497eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ba384d3-3f74-4d1d-9ce0-bd7b57082c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff178e4-2e57-4ef5-b6ff-e0b1c52497eb",
                    "LayerId": "b67cb7d7-75ee-4647-ab9d-5c5739bd6b33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "b67cb7d7-75ee-4647-ab9d-5c5739bd6b33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f41a7865-d383-43ec-b3a0-6a07fbdb58b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}