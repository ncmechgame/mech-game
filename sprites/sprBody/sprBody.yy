{
    "id": "8609c69c-9e79-4be6-8438-1289a9a7bdf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90892c5f-cee1-4cfd-b80c-62b8b9919d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8609c69c-9e79-4be6-8438-1289a9a7bdf0",
            "compositeImage": {
                "id": "53e25a1d-942a-461f-b947-2a1f816d32f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90892c5f-cee1-4cfd-b80c-62b8b9919d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06a6082-1885-4a34-8eed-5efdc37deb53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90892c5f-cee1-4cfd-b80c-62b8b9919d31",
                    "LayerId": "5a7c1621-bfff-4b4d-8bc7-0722f980c5e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "5a7c1621-bfff-4b4d-8bc7-0722f980c5e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8609c69c-9e79-4be6-8438-1289a9a7bdf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 16
}