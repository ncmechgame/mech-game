{
    "id": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Lazor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "605e8ad4-eb85-4fa3-9e3c-b5534f5914e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
            "compositeImage": {
                "id": "da5d2459-6836-4e1d-8b71-25e6941e489c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605e8ad4-eb85-4fa3-9e3c-b5534f5914e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69702dc8-ecb7-4b00-900b-7f531e4ff083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605e8ad4-eb85-4fa3-9e3c-b5534f5914e1",
                    "LayerId": "dbd23aa9-34cd-45f6-b343-7a91388c37cc"
                }
            ]
        },
        {
            "id": "f22d91db-4e77-4c74-848f-1d3b7ea562d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
            "compositeImage": {
                "id": "e8814d46-e04a-462a-83a9-9cce5ff3c665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22d91db-4e77-4c74-848f-1d3b7ea562d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f7412f-ceb5-4a7c-a75b-ed918702b128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22d91db-4e77-4c74-848f-1d3b7ea562d5",
                    "LayerId": "dbd23aa9-34cd-45f6-b343-7a91388c37cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dbd23aa9-34cd-45f6-b343-7a91388c37cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14732f98-f178-4f9b-bd14-5e2f723e52f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}