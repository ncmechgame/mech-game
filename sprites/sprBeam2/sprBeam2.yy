{
    "id": "b1b54cc9-2ecf-43ef-baab-6b58b03b4abd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBeam2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52014be2-e254-4660-bbd7-c4c412eaae5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b54cc9-2ecf-43ef-baab-6b58b03b4abd",
            "compositeImage": {
                "id": "6efcdf5a-9e57-494d-8b3c-199b580d2a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52014be2-e254-4660-bbd7-c4c412eaae5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d648c1e7-5641-4939-9006-ab09281a2c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52014be2-e254-4660-bbd7-c4c412eaae5a",
                    "LayerId": "4a0ba180-3622-4bd3-a3f6-defc1c585a51"
                }
            ]
        },
        {
            "id": "8d04bdbd-6af0-4dc2-8d57-d1486b243c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b54cc9-2ecf-43ef-baab-6b58b03b4abd",
            "compositeImage": {
                "id": "da690168-9f50-4b52-a7a0-2dc72da33fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d04bdbd-6af0-4dc2-8d57-d1486b243c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25b4764-db84-4a1a-9aed-cd238f0e34e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d04bdbd-6af0-4dc2-8d57-d1486b243c6e",
                    "LayerId": "4a0ba180-3622-4bd3-a3f6-defc1c585a51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "4a0ba180-3622-4bd3-a3f6-defc1c585a51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1b54cc9-2ecf-43ef-baab-6b58b03b4abd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}