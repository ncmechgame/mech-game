{
    "id": "cd2751a6-589f-4edb-a318-dc1a943f25b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2db1c534-7685-4066-8c67-310269deefc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd2751a6-589f-4edb-a318-dc1a943f25b1",
            "compositeImage": {
                "id": "5d546015-2ddb-4ea9-a032-236560f2cf61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2db1c534-7685-4066-8c67-310269deefc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc84c72-9fae-4822-82d6-293662be51a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2db1c534-7685-4066-8c67-310269deefc9",
                    "LayerId": "4d3913db-37d8-43da-8b21-9e7bf34cc084"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "4d3913db-37d8-43da-8b21-9e7bf34cc084",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd2751a6-589f-4edb-a318-dc1a943f25b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}