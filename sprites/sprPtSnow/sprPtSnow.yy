{
    "id": "79c16152-bd66-4110-992a-476957256b8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSnow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d1c2450-b91e-4f1d-b53a-f65c85d6d7e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c16152-bd66-4110-992a-476957256b8f",
            "compositeImage": {
                "id": "82e2ca95-bfae-40d8-9f65-3820c0eed3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1c2450-b91e-4f1d-b53a-f65c85d6d7e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d384006f-4fd5-4b6a-b798-638325fb941d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1c2450-b91e-4f1d-b53a-f65c85d6d7e9",
                    "LayerId": "45f642a4-a95a-4e7c-8bb9-9a5a08903271"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "45f642a4-a95a-4e7c-8bb9-9a5a08903271",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79c16152-bd66-4110-992a-476957256b8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 29
}