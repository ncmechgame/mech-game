{
    "id": "e6a106e2-020e-41c9-a8ff-fdc4b09ca529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSmoke_v2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 13,
    "bbox_right": 114,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c259a9d8-dc02-420d-897d-bfc176502cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6a106e2-020e-41c9-a8ff-fdc4b09ca529",
            "compositeImage": {
                "id": "2fe299ba-801d-4ed0-b609-cb94755c7f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c259a9d8-dc02-420d-897d-bfc176502cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47fd09e-052f-411c-a392-e354951bf68c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c259a9d8-dc02-420d-897d-bfc176502cea",
                    "LayerId": "52857ba6-22ed-4cf6-aa2e-cb7d85167bbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "52857ba6-22ed-4cf6-aa2e-cb7d85167bbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6a106e2-020e-41c9-a8ff-fdc4b09ca529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}