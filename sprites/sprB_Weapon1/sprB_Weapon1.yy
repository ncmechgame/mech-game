{
    "id": "18ec04cb-dd29-4261-ad85-dead6d564758",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Weapon1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "261f8949-8be3-4ef4-ba93-89608e9ba7f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18ec04cb-dd29-4261-ad85-dead6d564758",
            "compositeImage": {
                "id": "d0805b4c-aa6b-4cec-9783-660ae59f6c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "261f8949-8be3-4ef4-ba93-89608e9ba7f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4f6fab-de66-434c-b1df-ea8c8f8f1578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "261f8949-8be3-4ef4-ba93-89608e9ba7f6",
                    "LayerId": "88bc443e-b4a4-4906-8321-0d89e9568ffb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "88bc443e-b4a4-4906-8321-0d89e9568ffb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18ec04cb-dd29-4261-ad85-dead6d564758",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}