{
    "id": "cdea2a13-c331-42b6-9a04-a89ed84fa91c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a852a080-b6a5-4ead-9d43-6656f7d93ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdea2a13-c331-42b6-9a04-a89ed84fa91c",
            "compositeImage": {
                "id": "7186884d-edd0-429d-84bb-affc3e048b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a852a080-b6a5-4ead-9d43-6656f7d93ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03464f1c-c91a-483c-9736-7f86f95fcfcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a852a080-b6a5-4ead-9d43-6656f7d93ab8",
                    "LayerId": "3919d695-2192-4689-922e-efadde966e37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "3919d695-2192-4689-922e-efadde966e37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdea2a13-c331-42b6-9a04-a89ed84fa91c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 29,
    "yorig": 29
}