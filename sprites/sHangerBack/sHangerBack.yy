{
    "id": "e4160bcf-e1d3-4e43-86bc-8a726c910ded",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHangerBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a8e868c-a2a2-44c6-a6cf-258b59be4d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4160bcf-e1d3-4e43-86bc-8a726c910ded",
            "compositeImage": {
                "id": "85ba37a7-0d71-4bdf-88df-e8697fd2a9e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a8e868c-a2a2-44c6-a6cf-258b59be4d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf948ba-466f-4bb2-8ca7-1a43a8bcaa73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8e868c-a2a2-44c6-a6cf-258b59be4d32",
                    "LayerId": "6891ca20-ea21-4574-ab8e-f0a0a2ced03a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "6891ca20-ea21-4574-ab8e-f0a0a2ced03a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4160bcf-e1d3-4e43-86bc-8a726c910ded",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}