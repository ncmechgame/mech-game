{
    "id": "933fd513-8828-4299-a382-a70c5382c467",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSheetCrossHairs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21ce5237-73ee-45e0-866c-31742e534500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "e91d49a5-1784-4822-9b02-c2e81b8e5e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ce5237-73ee-45e0-866c-31742e534500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be7b0f9e-7ef2-4e48-bd04-d7f27452e7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ce5237-73ee-45e0-866c-31742e534500",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "6225e75d-d912-409c-80c7-ae61fbda10fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "02257a65-f6c7-4717-9a5f-0978269f6a1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6225e75d-d912-409c-80c7-ae61fbda10fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c4471f-d6dd-4061-8695-3ce4f7ef7c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6225e75d-d912-409c-80c7-ae61fbda10fc",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "06f7b613-3efd-4be4-83d6-0f450b67e52d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "bbb1005c-7572-4948-8ada-2a714cf68e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06f7b613-3efd-4be4-83d6-0f450b67e52d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b672bfe4-52b0-4035-8bbe-663733ea082a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06f7b613-3efd-4be4-83d6-0f450b67e52d",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "c86c7258-e6a7-40fe-afbd-737372558d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "6d3bdb88-69a6-4950-afdd-94e60037e480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86c7258-e6a7-40fe-afbd-737372558d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3945e42f-481f-4fa3-be21-7f4483cbb0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86c7258-e6a7-40fe-afbd-737372558d9a",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "e8e3ee42-d11c-4c18-bc53-0e734cbac6f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "d3017a3d-e2f8-4f2a-bbae-555ae0efb9cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e3ee42-d11c-4c18-bc53-0e734cbac6f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e71f694-dbed-4701-ba70-94c4755de34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e3ee42-d11c-4c18-bc53-0e734cbac6f4",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "18bfbf5c-fa2e-41f7-99bf-bf10acb88f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "bbfa627d-4a41-4962-a637-947ea0e81e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18bfbf5c-fa2e-41f7-99bf-bf10acb88f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd18c87f-ffad-4d64-9ffa-7f1a7ccd3a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18bfbf5c-fa2e-41f7-99bf-bf10acb88f71",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "c425ebe9-6b3f-4dba-8c71-879b0fcb975a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "7f22cf48-32bd-426e-bf1f-f2b8c923c4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c425ebe9-6b3f-4dba-8c71-879b0fcb975a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9858187-793f-42a0-87db-fe029c04ac67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c425ebe9-6b3f-4dba-8c71-879b0fcb975a",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "969d1211-1c23-46ee-b10b-b8c55050ac25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "49514e5f-495a-4d38-9a80-6ff1e59faea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "969d1211-1c23-46ee-b10b-b8c55050ac25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffae0d7e-d5e3-4e73-b49b-7877eb6d0d9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "969d1211-1c23-46ee-b10b-b8c55050ac25",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "023b5db5-0589-4882-91f5-2986884921fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "86ce4619-7b4c-4461-b574-41f714867d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "023b5db5-0589-4882-91f5-2986884921fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e1a30e-4727-456c-8b5d-388bd545f5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "023b5db5-0589-4882-91f5-2986884921fd",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "4edd7671-c8fc-4935-92ea-5c312994f8bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "55a43ca2-f053-43dd-8029-31cd6d403c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4edd7671-c8fc-4935-92ea-5c312994f8bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932afb0c-7aa4-4b02-99d8-1973ed40f0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4edd7671-c8fc-4935-92ea-5c312994f8bf",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "f956b11c-4524-4743-bc55-e37d9d91c68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "64142d40-231d-4b54-ba45-9ba770e8f07e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f956b11c-4524-4743-bc55-e37d9d91c68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "befca234-546a-4713-966d-14b8b99bf636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f956b11c-4524-4743-bc55-e37d9d91c68d",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "c7b57cef-0ae4-41e4-af95-02e327460acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "93ef5d46-ebbc-425b-ab31-433219766abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7b57cef-0ae4-41e4-af95-02e327460acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbaf59ff-1839-40e2-8887-da3686c68068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b57cef-0ae4-41e4-af95-02e327460acf",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "b8bd81f4-7b21-4eb4-9739-c0011e51743e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "a9111c85-7322-4e15-8322-704deaad8150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bd81f4-7b21-4eb4-9739-c0011e51743e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce78177b-385a-4dd0-beee-980b9954f0ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bd81f4-7b21-4eb4-9739-c0011e51743e",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "a9f1c1c2-79e7-4d21-ae4e-9a0b7b0e2a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "95de7bb5-251a-41cc-bada-19cc3313ce46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9f1c1c2-79e7-4d21-ae4e-9a0b7b0e2a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d988670e-b035-4f1c-9faf-d3251ff2f898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9f1c1c2-79e7-4d21-ae4e-9a0b7b0e2a37",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "edcfa89a-c234-42fd-a92e-82d91baa05fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "1421398b-d6e6-4bb3-9481-f41ce2934d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edcfa89a-c234-42fd-a92e-82d91baa05fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23ff6cd-62cf-45b6-acd2-c822c441bbde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edcfa89a-c234-42fd-a92e-82d91baa05fd",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "7125ebd0-b6ce-429b-ada0-6796d3f5ffe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "054dc093-abd1-4a34-bbb8-27f25ce72336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7125ebd0-b6ce-429b-ada0-6796d3f5ffe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd15466e-f474-4653-83b4-ca1745d73b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7125ebd0-b6ce-429b-ada0-6796d3f5ffe7",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "1bc12a81-4a2b-4906-9978-a311ee7fce2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "90960896-b832-4d83-beb1-308d071b687f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc12a81-4a2b-4906-9978-a311ee7fce2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06e70073-232a-4eda-afd1-3bb1ef9aae2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc12a81-4a2b-4906-9978-a311ee7fce2d",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "79704ac8-3981-4c13-98e0-c4e721e20bbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "44e26d05-d5b4-44d7-89c7-0a3413661629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79704ac8-3981-4c13-98e0-c4e721e20bbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbdd560a-b159-4f77-a105-1f24f43e11a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79704ac8-3981-4c13-98e0-c4e721e20bbd",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "93a6899e-07f2-4280-8d5e-ddd32253646c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "7c2ccfad-c3b5-4ac8-88ce-94c282fe5da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a6899e-07f2-4280-8d5e-ddd32253646c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c91b3c-4666-4b5a-88c3-1381c18ba606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a6899e-07f2-4280-8d5e-ddd32253646c",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "3d1b773d-ab4d-4c4a-8db5-d95ffd519f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "1b5229b8-b257-4bcf-a528-8bfb54611a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1b773d-ab4d-4c4a-8db5-d95ffd519f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca871e32-f284-419b-8601-1bf69cd0d785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1b773d-ab4d-4c4a-8db5-d95ffd519f86",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "f3d24217-f9ae-43f9-9ef2-d4f2325875d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "a57ff416-a553-4e4b-b2ff-3511fc22402d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d24217-f9ae-43f9-9ef2-d4f2325875d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dbbc58-cddc-41d0-ae7c-cbe5e2114f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d24217-f9ae-43f9-9ef2-d4f2325875d3",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "e697209b-938b-4da1-9232-b776d9aced38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "ce547807-2e4b-495b-9e80-7347066eacb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e697209b-938b-4da1-9232-b776d9aced38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c9db86-82ae-441a-a8c7-94c59c15186a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e697209b-938b-4da1-9232-b776d9aced38",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "3794c122-5a19-4438-bce0-584911a9bab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "3677a840-6dc1-47d7-acad-2710b3664e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3794c122-5a19-4438-bce0-584911a9bab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7f93f0-2b21-4bcd-8e26-ee162c9e1dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3794c122-5a19-4438-bce0-584911a9bab9",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "943a46a7-a98b-4bf6-9787-680c6922f251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "9391004b-0701-402e-bb6a-e1f416d9cc13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943a46a7-a98b-4bf6-9787-680c6922f251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82911fa3-121a-4017-b974-64593cf77b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943a46a7-a98b-4bf6-9787-680c6922f251",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "3d14adf9-e53a-4e66-a5cc-d29667769ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "46907329-014a-4f34-ab52-3b17db145349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d14adf9-e53a-4e66-a5cc-d29667769ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a4ffd5-512c-4df9-8c66-2a90ce45fd8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d14adf9-e53a-4e66-a5cc-d29667769ae9",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "db872c1e-8f5a-4b64-a695-91fe6e50f884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "39ef121c-4950-4dec-aab0-c4e32b5d8875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db872c1e-8f5a-4b64-a695-91fe6e50f884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db157a9-7230-40c8-a785-de4c3e5f849a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db872c1e-8f5a-4b64-a695-91fe6e50f884",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "3cb5d3d2-4cd7-4eab-98ae-fe0e9d62e5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "6e2fa046-364c-41bf-99a5-2cd7d23a62f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb5d3d2-4cd7-4eab-98ae-fe0e9d62e5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8328b7-f9f2-4109-9de5-b466aa88564a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb5d3d2-4cd7-4eab-98ae-fe0e9d62e5db",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "26e6ba80-443c-4d29-a301-e3752d21fa46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "80ec18dc-db73-438e-a016-3db5515d4ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e6ba80-443c-4d29-a301-e3752d21fa46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9faced1-e4a5-4265-900b-88a1b7e1b1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e6ba80-443c-4d29-a301-e3752d21fa46",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "fe1d3f90-d540-43d7-a099-ba0accec3fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "b308ebb2-a5d7-41d3-9bf4-c7295af4dd12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe1d3f90-d540-43d7-a099-ba0accec3fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db6d83d-ea70-4fa9-848f-716b463fa969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe1d3f90-d540-43d7-a099-ba0accec3fcd",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "21b02c98-3ed6-40c0-bd9c-7d35b4334b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "ae0e0956-d11a-489c-910a-efa9f5aa1283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b02c98-3ed6-40c0-bd9c-7d35b4334b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd4a822-10c5-429d-a916-10581881f53b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b02c98-3ed6-40c0-bd9c-7d35b4334b46",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "cc55769b-353c-487c-8ca6-f472a26319be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "2c289e81-a163-46b2-bfc0-a0670efada88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc55769b-353c-487c-8ca6-f472a26319be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c522cc62-1d76-4f59-a700-a2f35c0814d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc55769b-353c-487c-8ca6-f472a26319be",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "20e1c67d-41e6-47bc-b75c-ad3cd2f4e54d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "6f774bc9-4171-4fcf-963a-24ec626922e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e1c67d-41e6-47bc-b75c-ad3cd2f4e54d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3041eb-6e6c-47ca-9335-fbb23535fd3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e1c67d-41e6-47bc-b75c-ad3cd2f4e54d",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "31453a2e-8866-4c52-8bda-df358ab9bf4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "7b4cc4c3-8731-4759-bd8d-2d122a311792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31453a2e-8866-4c52-8bda-df358ab9bf4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f60870-238c-42b2-b09d-13abef6be96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31453a2e-8866-4c52-8bda-df358ab9bf4c",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "135e6cd6-7398-40c2-9541-62b10a73b436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "29f3f287-cad6-40ca-add4-ea770fc73f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "135e6cd6-7398-40c2-9541-62b10a73b436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4824987f-733c-470c-818f-3e4dbf8fcb20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "135e6cd6-7398-40c2-9541-62b10a73b436",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "080a9244-834e-4799-bc9e-19d688464852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "78f8ecce-46b8-4a25-a9ca-556fbc75981b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080a9244-834e-4799-bc9e-19d688464852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268bffe3-a890-48e0-9c44-40e115041915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080a9244-834e-4799-bc9e-19d688464852",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "d0de61d8-8e3e-4b1e-b767-4ec16eb0756c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "893b7131-8d29-48a3-b54d-444c4a325595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0de61d8-8e3e-4b1e-b767-4ec16eb0756c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f5d031-97bc-41b0-87b5-ba36d50ff691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0de61d8-8e3e-4b1e-b767-4ec16eb0756c",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "612aeea6-d9b6-4a84-8974-1cbcdab19164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "c7ed0d43-65f9-4200-ba56-c40e9459781c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612aeea6-d9b6-4a84-8974-1cbcdab19164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747f9b21-0d04-477d-9a00-4e53a083a807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612aeea6-d9b6-4a84-8974-1cbcdab19164",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "fe83a19e-d555-4b4c-9e73-4ee40e0ee5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "a05670f2-7ab2-421f-8e62-a7b369bdc1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe83a19e-d555-4b4c-9e73-4ee40e0ee5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52124da8-3ef7-462e-a9e7-6f84322faf75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe83a19e-d555-4b4c-9e73-4ee40e0ee5a4",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "660b3aad-04c7-478c-8a14-6a679bcd38d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "d74ce920-9e45-4327-8625-cd72e8b8256e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660b3aad-04c7-478c-8a14-6a679bcd38d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b6e179-ac1c-4ecb-b642-ef65b5877825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660b3aad-04c7-478c-8a14-6a679bcd38d0",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "60cadc4c-3419-4073-ba2e-ae364f3564e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "f8b46795-13b6-45c8-9c00-35393eb91360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60cadc4c-3419-4073-ba2e-ae364f3564e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5d56b9-7726-4813-a8b0-3b023eb07acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60cadc4c-3419-4073-ba2e-ae364f3564e8",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "1acbbf6d-f1a1-4a1f-b7e0-c74061c3611b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "1e8ed21d-c6c7-4f20-ae31-0db180d1d703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1acbbf6d-f1a1-4a1f-b7e0-c74061c3611b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d037007-7b4d-44cf-8662-1aeb0146b596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1acbbf6d-f1a1-4a1f-b7e0-c74061c3611b",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "9e09928f-cf67-496a-bb27-e1e47b40e10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "ebff39d3-d71e-40bf-96e2-9f0d11b9539a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e09928f-cf67-496a-bb27-e1e47b40e10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3037d66-c91d-4df9-84d2-c0c5d3a26fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e09928f-cf67-496a-bb27-e1e47b40e10f",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "b8bb6614-edae-42ed-9b7e-e4a7ce681ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "c8d0bb50-cdc6-44aa-a06b-ee4b79f767d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bb6614-edae-42ed-9b7e-e4a7ce681ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e460b2f-3192-4960-a303-4f7bd9e97dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bb6614-edae-42ed-9b7e-e4a7ce681ac2",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "c4757c6d-0cd5-4bbd-b2af-fc400349c732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "37309521-b5dc-42ef-92ea-3518b2f397ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4757c6d-0cd5-4bbd-b2af-fc400349c732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692724c8-e841-40d3-b1bb-a9d30fcd7a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4757c6d-0cd5-4bbd-b2af-fc400349c732",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "39f4f0eb-b09e-41c0-8ce3-513552c5db9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "b86e2585-21e7-4ef2-ac7f-6dd2bc88bfd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f4f0eb-b09e-41c0-8ce3-513552c5db9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04de1484-3c5c-4345-8e55-b264c2bfa268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f4f0eb-b09e-41c0-8ce3-513552c5db9f",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "697bfafa-9a54-4406-b261-ff96ed5c92f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "8911b594-ee39-4a39-95b2-ccf434a65de9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "697bfafa-9a54-4406-b261-ff96ed5c92f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63fe6ef9-a1d4-4c5f-aef9-dc3e2379eadc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "697bfafa-9a54-4406-b261-ff96ed5c92f8",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "5666b835-1bd0-4349-9330-a37845249b51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "0eef5972-6ed0-4a72-99db-18cf096c6d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5666b835-1bd0-4349-9330-a37845249b51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839a9e10-643e-40a5-99f2-f95e05024f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5666b835-1bd0-4349-9330-a37845249b51",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "478f2dc7-1811-46e7-a820-b71ed073f361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "19362250-1468-42b8-8a81-16ac1b9ba691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "478f2dc7-1811-46e7-a820-b71ed073f361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c67c06-6249-465f-b44c-fd998c22c325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "478f2dc7-1811-46e7-a820-b71ed073f361",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "595422dc-51c7-443a-a7bd-5e4a46880001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "32972fff-9c88-412f-8926-e31ec2afe851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595422dc-51c7-443a-a7bd-5e4a46880001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc88552a-1c9a-4e2b-8ee9-5c159f4498b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595422dc-51c7-443a-a7bd-5e4a46880001",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "b84fa3bc-647e-4fa9-80f0-55835458eabb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "8f4effe4-1e16-4771-b27f-05da5a461422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84fa3bc-647e-4fa9-80f0-55835458eabb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd6d8f4-5e45-4207-9614-35d208ffcb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84fa3bc-647e-4fa9-80f0-55835458eabb",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "5537307a-c6c0-47d7-b487-1f4c15e7043d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "1e1c5715-2c21-4156-be3c-864d242b9290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5537307a-c6c0-47d7-b487-1f4c15e7043d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3df0675-f685-48b9-bc19-79acc1c750b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5537307a-c6c0-47d7-b487-1f4c15e7043d",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "f91154ec-bfbb-4464-b276-a5e8ed87e899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "41dd2c1e-e9c4-43e6-8e6a-a0e2566bc779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f91154ec-bfbb-4464-b276-a5e8ed87e899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4339b50b-17f8-4a2d-be8e-5de3c1d471a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f91154ec-bfbb-4464-b276-a5e8ed87e899",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "d4203ae4-5fe9-46f2-a3b4-c3517f8d3943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "6fdee43e-0b81-4f02-af71-990ae14decc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4203ae4-5fe9-46f2-a3b4-c3517f8d3943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca4ed13-4f4a-43da-8a49-48d54ae93f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4203ae4-5fe9-46f2-a3b4-c3517f8d3943",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "794489d5-33c1-4260-9650-603c48619409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "f277ccdb-5e5a-4d1b-a656-5878828c138e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794489d5-33c1-4260-9650-603c48619409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b074d75-c17c-48a9-a23a-1908d1f087c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794489d5-33c1-4260-9650-603c48619409",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "ff87fe1f-cb72-4868-bbd4-e8a61071032a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "8233b90e-341e-4427-a1f0-5a27b48ddb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff87fe1f-cb72-4868-bbd4-e8a61071032a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442a4a2a-2459-42e9-8959-b84d06da0070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff87fe1f-cb72-4868-bbd4-e8a61071032a",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "8f2580c8-e0fe-4d19-ba46-4364a1b74b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "59f59527-99c3-45ca-9a64-eea8cd0e52e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2580c8-e0fe-4d19-ba46-4364a1b74b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1d561f2-bad1-4068-ac54-30cba36ec43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2580c8-e0fe-4d19-ba46-4364a1b74b23",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "bf2f32dd-c28a-47d6-87a5-2b217ec4285f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "0580a43d-0275-4938-8f6e-026558894fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf2f32dd-c28a-47d6-87a5-2b217ec4285f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b196a02a-ad23-47e0-8233-d66805f3cab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2f32dd-c28a-47d6-87a5-2b217ec4285f",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "aaae3129-038c-45d3-9bca-d9753b569d98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "efb62650-2b59-4946-8c36-997fa0ca2af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaae3129-038c-45d3-9bca-d9753b569d98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4158773-dab9-40e0-b815-d3087fcb410d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaae3129-038c-45d3-9bca-d9753b569d98",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "07ca1a48-dfb3-42f8-93f4-daad46de9ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "169de121-1119-4547-9df2-ee6a9bb78308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ca1a48-dfb3-42f8-93f4-daad46de9ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eab45e99-cf8e-4546-8f5c-ad49316ae3f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ca1a48-dfb3-42f8-93f4-daad46de9ba2",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "629b0d1b-d22e-4a49-8d9f-db7775c5a581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "82f080ea-5c4e-41ba-8fc0-093156988da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "629b0d1b-d22e-4a49-8d9f-db7775c5a581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15cb6d8b-eedd-4643-a09b-792be66e4a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "629b0d1b-d22e-4a49-8d9f-db7775c5a581",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "647ba834-bacd-40d1-a472-71088d9dac45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "23523c72-a7a9-428d-aee0-ccbc3824640d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647ba834-bacd-40d1-a472-71088d9dac45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ccd555-4d4a-449c-9a7a-65a12e027151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647ba834-bacd-40d1-a472-71088d9dac45",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "751e95f8-3fa1-4b4e-8060-8379b54612e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "17a6d855-e324-4d66-ada2-148aa2bf4735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751e95f8-3fa1-4b4e-8060-8379b54612e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d52365-c3d1-4d12-a668-b426585d6e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751e95f8-3fa1-4b4e-8060-8379b54612e9",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "b630441b-92b3-48bb-857a-69f13d29d264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "21233ef1-5306-44fc-9dfe-980dd77d636f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b630441b-92b3-48bb-857a-69f13d29d264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328fa8bb-cacc-489b-a8a3-090f6e5636d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b630441b-92b3-48bb-857a-69f13d29d264",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "8a6c0dd6-d0d5-4f44-9dc7-8e41c35c7a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "a6020578-ced7-4e61-a874-c12545613b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6c0dd6-d0d5-4f44-9dc7-8e41c35c7a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571fde56-3a0c-4d84-8a4d-749fb0985c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6c0dd6-d0d5-4f44-9dc7-8e41c35c7a07",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        },
        {
            "id": "be6e4c77-b6c0-4624-81a0-13e2893a0bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "compositeImage": {
                "id": "f19782a3-13a4-41a2-a470-af4224acc534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be6e4c77-b6c0-4624-81a0-13e2893a0bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef66f9c9-4d81-4456-a3a5-9032c238ab16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be6e4c77-b6c0-4624-81a0-13e2893a0bc2",
                    "LayerId": "6a7fa417-69b1-4a2c-815d-7bda898bc061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6a7fa417-69b1-4a2c-815d-7bda898bc061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "933fd513-8828-4299-a382-a70c5382c467",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}