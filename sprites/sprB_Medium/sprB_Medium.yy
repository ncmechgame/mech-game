{
    "id": "acb53a44-d572-4212-aa4c-8d9073e33b26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08867cda-63d9-4a9d-8cd7-bf4c5ba079cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb53a44-d572-4212-aa4c-8d9073e33b26",
            "compositeImage": {
                "id": "8edb8b8e-d215-4d91-b3f6-c3c253d04f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08867cda-63d9-4a9d-8cd7-bf4c5ba079cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef37509-1e7b-43e6-bf0a-e3bdb61f1822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08867cda-63d9-4a9d-8cd7-bf4c5ba079cc",
                    "LayerId": "3c534743-a39e-41b7-b592-ad946d440d08"
                }
            ]
        },
        {
            "id": "8ce181b6-980b-4345-b017-6fe78b00fb37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb53a44-d572-4212-aa4c-8d9073e33b26",
            "compositeImage": {
                "id": "1ed3b431-b196-465f-b09d-408ad99346c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce181b6-980b-4345-b017-6fe78b00fb37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dd3767-b497-46f4-bc3a-f8759e699a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce181b6-980b-4345-b017-6fe78b00fb37",
                    "LayerId": "3c534743-a39e-41b7-b592-ad946d440d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3c534743-a39e-41b7-b592-ad946d440d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acb53a44-d572-4212-aa4c-8d9073e33b26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}