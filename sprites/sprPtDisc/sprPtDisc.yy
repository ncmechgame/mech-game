{
    "id": "bf1eca47-008f-40df-a4ad-84510ff07b5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtDisc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7f26bcc-b678-4d52-a946-77d359497a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf1eca47-008f-40df-a4ad-84510ff07b5b",
            "compositeImage": {
                "id": "6a4dccc9-de42-465b-ad8f-3c7bdac6cafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f26bcc-b678-4d52-a946-77d359497a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afcb575-5c3d-4d11-82ac-22e6294b2e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f26bcc-b678-4d52-a946-77d359497a0a",
                    "LayerId": "ef7e47c9-3cc9-4d47-8a25-5af907dc9e88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 57,
    "layers": [
        {
            "id": "ef7e47c9-3cc9-4d47-8a25-5af907dc9e88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf1eca47-008f-40df-a4ad-84510ff07b5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 58,
    "xorig": 29,
    "yorig": 28
}