{
    "id": "8342d0f7-2378-4c54-a3e1-cec3333d2007",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtLine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c0a89a6-2192-4797-853f-805a171d986d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8342d0f7-2378-4c54-a3e1-cec3333d2007",
            "compositeImage": {
                "id": "9d121350-6f4c-42a5-86e7-5ae5b6e56659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0a89a6-2192-4797-853f-805a171d986d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334d6781-6d23-4ea6-95e5-88492c81d30c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0a89a6-2192-4797-853f-805a171d986d",
                    "LayerId": "2c0107ce-846b-443b-ac16-92c7d4178f83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "2c0107ce-846b-443b-ac16-92c7d4178f83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8342d0f7-2378-4c54-a3e1-cec3333d2007",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 5
}