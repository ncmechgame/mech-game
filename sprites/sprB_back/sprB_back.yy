{
    "id": "e2d8fbbc-2925-47e2-82f1-dec03093fc7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9275d85e-85b8-45d3-b7a3-fdbe9ba4baf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d8fbbc-2925-47e2-82f1-dec03093fc7e",
            "compositeImage": {
                "id": "9ede9e7d-038a-43c6-aa0e-8f5a29915ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9275d85e-85b8-45d3-b7a3-fdbe9ba4baf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8365f851-0222-43cc-91f6-a25eace20553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9275d85e-85b8-45d3-b7a3-fdbe9ba4baf6",
                    "LayerId": "02c83ee6-a48a-442e-9516-4e40e55fde16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "02c83ee6-a48a-442e-9516-4e40e55fde16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2d8fbbc-2925-47e2-82f1-dec03093fc7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}