{
    "id": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCyclopsWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eebc4f23-013a-4a42-b273-75793734342c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "compositeImage": {
                "id": "585d5953-1380-4711-8afb-4ce4e0cbef98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eebc4f23-013a-4a42-b273-75793734342c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f1ac48-9967-4ff1-9c3c-4e2a04a67527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eebc4f23-013a-4a42-b273-75793734342c",
                    "LayerId": "7e4e3c11-1f5c-4462-9645-4e68443a58ee"
                },
                {
                    "id": "da60b1fd-6608-4b50-a41c-4ef5bc3130b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eebc4f23-013a-4a42-b273-75793734342c",
                    "LayerId": "f315a1b3-9068-4b8d-b0bd-136bea0641e6"
                }
            ]
        },
        {
            "id": "d533b8ab-2dd8-4a14-989d-bc76a407a3ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "compositeImage": {
                "id": "1c39c716-1c83-4581-9b36-65de32e3cb99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d533b8ab-2dd8-4a14-989d-bc76a407a3ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e15bd4c-9583-466d-94ce-c742b6079bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d533b8ab-2dd8-4a14-989d-bc76a407a3ea",
                    "LayerId": "f315a1b3-9068-4b8d-b0bd-136bea0641e6"
                },
                {
                    "id": "7730652e-7f5e-48b5-9864-dddbdf428bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d533b8ab-2dd8-4a14-989d-bc76a407a3ea",
                    "LayerId": "7e4e3c11-1f5c-4462-9645-4e68443a58ee"
                }
            ]
        },
        {
            "id": "802db921-974e-4216-bf28-bf1316b34554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "compositeImage": {
                "id": "55c2a49f-671c-4b50-9c58-0df1a76c31a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802db921-974e-4216-bf28-bf1316b34554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69cf0d51-aba4-41f2-81e2-138bc6d205af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802db921-974e-4216-bf28-bf1316b34554",
                    "LayerId": "f315a1b3-9068-4b8d-b0bd-136bea0641e6"
                },
                {
                    "id": "b0ed36f4-0b52-495e-9b9d-0b9278857000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802db921-974e-4216-bf28-bf1316b34554",
                    "LayerId": "7e4e3c11-1f5c-4462-9645-4e68443a58ee"
                }
            ]
        },
        {
            "id": "15b11774-94da-4596-9511-cb2bf029d725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "compositeImage": {
                "id": "24c0f12d-8564-44cc-a5a2-a6ef8e9bc06a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b11774-94da-4596-9511-cb2bf029d725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ba433a9-e150-4ac5-8ea0-19ab4221c69b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b11774-94da-4596-9511-cb2bf029d725",
                    "LayerId": "f315a1b3-9068-4b8d-b0bd-136bea0641e6"
                },
                {
                    "id": "272326c7-f438-4f09-8ab9-d483f2874ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b11774-94da-4596-9511-cb2bf029d725",
                    "LayerId": "7e4e3c11-1f5c-4462-9645-4e68443a58ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f315a1b3-9068-4b8d-b0bd-136bea0641e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "blendMode": 0,
            "isLocked": false,
            "name": "face",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7e4e3c11-1f5c-4462-9645-4e68443a58ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b01acb88-2b19-42b2-a8bc-4b1e0e663221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}