{
    "id": "bed91c98-a9ed-43e8-805b-1436a4a370cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprB_Light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e5edcdd-4197-4c17-be0e-a47da0e86a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bed91c98-a9ed-43e8-805b-1436a4a370cb",
            "compositeImage": {
                "id": "10d9e83c-0dcd-4576-b7c6-5af1ca09b311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5edcdd-4197-4c17-be0e-a47da0e86a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d0ec69-4d93-4c6c-a451-e9243ec6f4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5edcdd-4197-4c17-be0e-a47da0e86a58",
                    "LayerId": "881e4dbc-00d2-4c91-b8d2-20fcd1d1186c"
                }
            ]
        },
        {
            "id": "1cbd1f9e-f08e-460c-be2a-e639545fd317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bed91c98-a9ed-43e8-805b-1436a4a370cb",
            "compositeImage": {
                "id": "b1d153f1-4f71-4e61-90e3-fb0487be05e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cbd1f9e-f08e-460c-be2a-e639545fd317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20e598e-1c6a-4286-a5eb-39346eeaab67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cbd1f9e-f08e-460c-be2a-e639545fd317",
                    "LayerId": "881e4dbc-00d2-4c91-b8d2-20fcd1d1186c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "881e4dbc-00d2-4c91-b8d2-20fcd1d1186c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bed91c98-a9ed-43e8-805b-1436a4a370cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}