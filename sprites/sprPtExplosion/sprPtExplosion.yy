{
    "id": "c8ad2f57-c293-47c5-9e2d-9caed12e73ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtExplosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0ec34fc-85ae-4cb0-996f-3c552a13c870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8ad2f57-c293-47c5-9e2d-9caed12e73ae",
            "compositeImage": {
                "id": "b5c257ca-ad25-4e2a-b84e-ceac17090df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ec34fc-85ae-4cb0-996f-3c552a13c870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e8202eb-6d74-47e8-b89d-29fbf82c73c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ec34fc-85ae-4cb0-996f-3c552a13c870",
                    "LayerId": "d54d1041-65d5-40d7-b175-f197e4821ced"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "d54d1041-65d5-40d7-b175-f197e4821ced",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8ad2f57-c293-47c5-9e2d-9caed12e73ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 31,
    "yorig": 31
}