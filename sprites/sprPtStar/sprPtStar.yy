{
    "id": "48af3562-67b9-46d1-8d95-0e5f43cd025b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPtStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72538a8c-21ee-41c7-bcc9-b1ddf2766d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48af3562-67b9-46d1-8d95-0e5f43cd025b",
            "compositeImage": {
                "id": "569bdc70-c2c2-482d-8dfb-2b483fb5f519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72538a8c-21ee-41c7-bcc9-b1ddf2766d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a799152b-4a0f-4885-a866-ef7b166cbc04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72538a8c-21ee-41c7-bcc9-b1ddf2766d87",
                    "LayerId": "14d61b64-f8f8-4531-a3cb-1c31c6e1e3fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "14d61b64-f8f8-4531-a3cb-1c31c6e1e3fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48af3562-67b9-46d1-8d95-0e5f43cd025b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 25,
    "yorig": 26
}