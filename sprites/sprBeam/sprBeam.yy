{
    "id": "26d857e7-b4e1-40e2-a7f0-ba101d9bf8d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBeam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f90ed31-8d14-49a2-9ed9-e9c3b57d84f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d857e7-b4e1-40e2-a7f0-ba101d9bf8d1",
            "compositeImage": {
                "id": "d977d217-21e0-47d4-9c2d-9cd1578e84d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f90ed31-8d14-49a2-9ed9-e9c3b57d84f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b759441-01f9-4a47-8cee-665df0b5fc48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f90ed31-8d14-49a2-9ed9-e9c3b57d84f7",
                    "LayerId": "27d9bdb3-9718-43fb-a130-6794f78a4681"
                }
            ]
        },
        {
            "id": "190f02e4-1300-44c3-a350-5a52ad932f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d857e7-b4e1-40e2-a7f0-ba101d9bf8d1",
            "compositeImage": {
                "id": "75f8063a-a140-4fe1-b46d-ebfd579ae10d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190f02e4-1300-44c3-a350-5a52ad932f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77024fcb-adae-4a50-923d-18818ef3cf99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190f02e4-1300-44c3-a350-5a52ad932f96",
                    "LayerId": "27d9bdb3-9718-43fb-a130-6794f78a4681"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "27d9bdb3-9718-43fb-a130-6794f78a4681",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26d857e7-b4e1-40e2-a7f0-ba101d9bf8d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}