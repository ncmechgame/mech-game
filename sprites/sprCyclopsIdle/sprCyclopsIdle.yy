{
    "id": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCyclopsIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45307a6e-ee40-4da1-b77b-ed84a9ea9997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "compositeImage": {
                "id": "bf3b0b0a-ede6-4cf2-97d0-b1fdf8a2991d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45307a6e-ee40-4da1-b77b-ed84a9ea9997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4509afd3-2e85-4921-9868-86f3f1c65f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45307a6e-ee40-4da1-b77b-ed84a9ea9997",
                    "LayerId": "07fcbfa1-d532-4a98-9f73-061f17bdf843"
                },
                {
                    "id": "201eb3da-2ec2-4e27-9d20-91c17d05db89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45307a6e-ee40-4da1-b77b-ed84a9ea9997",
                    "LayerId": "b2b28a64-ae04-43d8-9786-27892952d3d6"
                }
            ]
        },
        {
            "id": "d7b8e0b3-3873-4f9a-b68c-1be534ac74c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "compositeImage": {
                "id": "b4ef3c4d-51a3-4236-aac0-6f38fe8c4cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7b8e0b3-3873-4f9a-b68c-1be534ac74c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f36d4c-bdff-4947-89bc-3f892f780431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b8e0b3-3873-4f9a-b68c-1be534ac74c2",
                    "LayerId": "07fcbfa1-d532-4a98-9f73-061f17bdf843"
                },
                {
                    "id": "e1dc218e-1c56-4180-845a-35383d863d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b8e0b3-3873-4f9a-b68c-1be534ac74c2",
                    "LayerId": "b2b28a64-ae04-43d8-9786-27892952d3d6"
                }
            ]
        },
        {
            "id": "b29168b0-247c-4816-8a33-5fa019e9734a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "compositeImage": {
                "id": "6d5d5033-dc44-4f1e-b698-4efbbbd7cd27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29168b0-247c-4816-8a33-5fa019e9734a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413f1636-2b77-4503-bc8b-fbc5bd692b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29168b0-247c-4816-8a33-5fa019e9734a",
                    "LayerId": "07fcbfa1-d532-4a98-9f73-061f17bdf843"
                },
                {
                    "id": "3c6611e9-51f4-4c48-a299-8aa47009ba69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29168b0-247c-4816-8a33-5fa019e9734a",
                    "LayerId": "b2b28a64-ae04-43d8-9786-27892952d3d6"
                }
            ]
        },
        {
            "id": "34dea868-5618-4832-ac63-76d19a374b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "compositeImage": {
                "id": "0b32d767-c97a-442d-be20-504a8d4f05ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34dea868-5618-4832-ac63-76d19a374b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0884fd71-3cb5-4abf-b075-ded03fe1672b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34dea868-5618-4832-ac63-76d19a374b30",
                    "LayerId": "07fcbfa1-d532-4a98-9f73-061f17bdf843"
                },
                {
                    "id": "89d8fd57-ea0a-4104-a8d6-fc8246e026d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34dea868-5618-4832-ac63-76d19a374b30",
                    "LayerId": "b2b28a64-ae04-43d8-9786-27892952d3d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "07fcbfa1-d532-4a98-9f73-061f17bdf843",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "face",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b2b28a64-ae04-43d8-9786-27892952d3d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6720a6a-c62c-438a-b7e8-8017408f9cd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}