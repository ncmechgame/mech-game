{
    "id": "b8bbd2ef-112c-4c9d-b238-c7b5b0ec0dfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCustomButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77dfe7dd-1c04-4010-a52d-b686847b495b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8bbd2ef-112c-4c9d-b238-c7b5b0ec0dfa",
            "compositeImage": {
                "id": "fad24c53-573b-4893-8661-42b43dccd37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77dfe7dd-1c04-4010-a52d-b686847b495b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0581dc97-ff3b-4c9f-a33a-797c4e58cec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77dfe7dd-1c04-4010-a52d-b686847b495b",
                    "LayerId": "b12430c4-66fa-40a4-aff0-52ba7a428da2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "b12430c4-66fa-40a4-aff0-52ba7a428da2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8bbd2ef-112c-4c9d-b238-c7b5b0ec0dfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 0,
    "yorig": 0
}